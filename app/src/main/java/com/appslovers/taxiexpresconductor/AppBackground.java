package com.appslovers.taxiexpresconductor;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.body.Frag_2_Tarifario;
import com.appslovers.taxiexpresconductor.data.ISetActivityResult;
import com.appslovers.taxiexpresconductor.data.ISocket;
import com.appslovers.taxiexpresconductor.data.InnerActions;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.util.MyGps;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.data.Notificaciones;
import com.appslovers.taxiexpresconductor.util.SocketIo;
import com.appslovers.taxiexpresconductor.data.Urls;
import com.appslovers.taxiexpresconductor.util.BroadCastReserva;
import com.appslovers.taxiexpresconductor.util.SocketIoBridge;
import com.appslovers.taxiexpresconductor.util.Tools;
import com.github.nkzawa.socketio.client.Ack;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AppBackground extends Service {


    public static AppBinder AppConexion = new AppBinder();
    NotificationManager NM;

    public static final int ZOOM_GOOGLEMAPS = 16;

    @Override
    public IBinder onBind(Intent intent) {
        AppConexion.setApp(this);
        Log.d("AppBackground", "onBind");
        return AppConexion;
    }

    @Override
    public void onRebind(Intent intent) {

        AppConexion.setApp(this);
        Log.d("AppBackground", "onReBind");
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {

        //AppConexion.setApp(null);
        AppConexion.setRoot(null);
        Log.d("AppBackground", "onUnBind");
        return true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            if (intent.getAction() != null) {
                doIntentInside(intent);
            }
        }
        return START_NOT_STICKY;
    }

    public void doIntentInside(Intent i) {
        switch (InnerActions.valueOf(i.getAction())) {

            case notact_updateestado:

                JsonPack.RespEstadoCondcutor rec = (JsonPack.RespEstadoCondcutor) i.getSerializableExtra("rec");
                if (rec != null) {

                    Log.d("notact_updateestado", "REC OK");
                    ServicioActual = rec.Servicio;
                    ClienteActual = rec.Cliente;
                    if (ServicioActual != null && ServicioActual.getEstado() > 0) {
                        Log.d("Servicio actual", ">" + ServicioActual.getEstado());
                        if (!isNodeJsConnected()) {
                            NodeJsTurnOn(null);
                        }
                    } else {
                        Log.e("Serbvicio actual", "NULL");
                    }

                } else {
                    Log.e("notact_updateestado", "REC null");
                }

                break;
            case notact_salirservicio:

                NM.cancelAll();
                stopForeground(true);
                stopSelf();
                android.os.Process.killProcess(android.os.Process.myPid());
                break;

            case notact_detenernode:
                NodeJsTurnOff();
                break;

            case notact_encendernode:
                NodeJsTurnOn(null);
                break;

            case notact_cancelar_encendernode:
                NodeJsCancelTurnOn();
                break;
            case notact_cancelar_detenernode:
                NodeJsCancelTurnOff();
                break;

            case notact_aceptarSolicitud:
                int ida = i.getIntExtra("notiid", -1);
                if (ida > -1)
                    NM.cancel(ida);

                JsonPack.NuevaSolicitud ns = (JsonPack.NuevaSolicitud) i.getSerializableExtra("solicitud");
                AceptarServicioConductor(ns);
                break;

            case notact_rechazarSolicitud:
                int idr = i.getIntExtra("notiid", -1);
                if (idr > -1)
                    NM.cancel(idr);
                break;

            case notact_iniciarReserva:
                ReservaIniciarServicio();
                NM.cancel(11111111);
                break;

            case notact_rechazarReserva:
                NM.cancel(11111111);
                break;

            case noact_aceptarReserva:
                NM.cancel(56467);
                JsonPack.NuevaReserva nr = (JsonPack.NuevaReserva) i.getSerializableExtra("reserva");
                AceptarNuevaReserva(nr);

                break;

            case noact_noaceptarReserva:
                NM.cancel(56467);
                break;
        }
    }

    public void detenerServicio() {
        NM.cancelAll();
        stopForeground(true);
        stopSelf();
    }

    public boolean isNodeJsConnected() {
        if (SI != null) {
            if (SI.SOCKET != null) {
                if (SI.SOCKET.connected()) {
                    return true;
                }
            }
        }
        return false;

    }


    //SPUser Session;
    JsonPack.ResponseLogin Session;

    public void NodeJsTurnOn(ISetActivityResult iar) {

        Session = new SPUser(getApplicationContext()).getResponseLogin();

        //startForeground(1, Notificaciones.getNotificacionDetener(getApplicationContext()));
        startForeground(1, Notificaciones.getNotificacionEncendiendoDeteniendo(getApplicationContext(), true));
        SI.Conectar();
    }

    public void NodeJsCancelTurnOn() {
        Log.i("appbg", ">>>>>>>>>> cancel turn on");
        SI.Desconectar();
    }

    public void NodeJsCancelTurnOff() {
        Log.i("appbg", ">>>>>>>>>> cancel turn off");
    }

    public void NodeJsTurnOff() {
        //SI.SOCKET.disconnect();
        //SI.SOCKET.close();
        SI.Desconectar();
        startForeground(1, Notificaciones.getNotificacionEncendiendoDeteniendo(getApplicationContext(), false));
        //startForeground(1, Notificaciones.getNotificacionEncender(getApplicationContext()));

    }


    public void AceptarServicioConductor(JsonPack.NuevaSolicitud ns) {

        JsonPack.SolicitudRespuesta aceptaTaxi = new JsonPack.SolicitudRespuesta();
        aceptaTaxi.conductor_id = Session.conductor_id;
        aceptaTaxi.servicio_id = ns.ServicioId;
        Log.d("Envio nueva sol", ">" + ns.Envio);
        if (ns.Envio == 0) {
            aceptaTaxi.socketCli = null;
        } else {
            aceptaTaxi.socketCli = ns.socketCliente;
        }

        SI.emitData("AceptarSolicitudConductor", new Gson().toJson(aceptaTaxi));
    }

    public void EnviarLlegadaRecogo(SocketIo.CallBack a) {

        JsonPack.LlegoRecojo recojo = new JsonPack.LlegoRecojo();
        recojo.IdConductor = Session.conductor_id;
        recojo.IdServicio = ServicioActual.IdServicio;

        SI.emitDataCallback("LLegadaConductor", new Gson().toJson(recojo), a);
    }


    public void EnviarLlegadaRecogo(Ack a) {

        JsonPack.LlegoRecojo recojo = new JsonPack.LlegoRecojo();
        recojo.IdConductor = Session.conductor_id;
        recojo.IdServicio = ServicioActual.IdServicio;

        SI.emitDataCallback("LLegadaConductor", new Gson().toJson(recojo), a);
    }

    public void EnviarIniciarServicio() {

        JsonPack.LlegoRecojo recojo = new JsonPack.LlegoRecojo();
        recojo.IdConductor = Session.conductor_id;
        recojo.IdServicio = ServicioActual.IdServicio;

        SI.emitDataCallback("IniciarServicio", new Gson().toJson(recojo), new Ack() {


            @Override
            public void call(Object... args) {
                Log.i("EnviarLlegadaRecogo", ">" + SocketIo.getOneString(args));
            }


        });
    }


    public void EnviarAlerta() {
        JsonPack.AlertaWeb A = new JsonPack.AlertaWeb();
        if (MYGPS != null && MYGPS.lastKnownLocation != null) {
            A.latitud = MYGPS.lastKnownLocation.getLatitude();
            A.longitud = MYGPS.lastKnownLocation.getLongitude();

            if (getServicioActual() != null) {
                A.tipo = "3";
                A.codigo = getServicioActual().IdServicio;
                A.flag = 2;
            } else {
                A.codigo = new SPUser(getApplicationContext()).getResponseLogin().conductor_id;
                A.tipo = "2";
                //A.flag=2;
            }
            SI.emitData("nuevaAlertaWeb", new Gson().toJson(A));
        } else {
            Log.e("EviarAlerta", "Sin coordaenadas");
        }

    }

    public void EnviarIniciarServicio(SocketIo.CallBack a) {

        JsonPack.LlegoRecojo recojo = new JsonPack.LlegoRecojo();
        recojo.IdConductor = Session.conductor_id;
        recojo.IdServicio = ServicioActual.IdServicio;


        SI.emitDataCallback("IniciarServicio", new Gson().toJson(recojo), a);
    }

    public void enviarSolicitudPago(String idCliente){
        SI.emitData("enviarSolicitudPago",idCliente);
    }

    public void EnviarFinalizarServicio(JsonPack.CostosAdicionales ca, final SocketIo.CallBack a) {

        final JsonPack.LlegoRecojo recojo = new JsonPack.LlegoRecojo();
        recojo.IdConductor = Session.conductor_id;
        recojo.IdServicio = ServicioActual.IdServicio;

        recojo.flagexpreso = 0;
        recojo.Paradas = ca.Paradas;
        recojo.Espera = ca.Espera;
        recojo.Parqueos = ca.Parqueos;
        recojo.Peajes = ca.Peajes;
        recojo.tarifa = getServicioActual().Tarifa;
        recojo.DestinoDireccion = "";
        recojo.distancia = Double.parseDouble(String.format("%.4f", distancia_acumulada / 1000).replace(",", "."));

        getServicioActual().costosadicionales = ca;

        if ((getServicioActual().DestinoId != null && getServicioActual().DestinoId.equals("0"))) {
            Log.i(">", "si es expreso");
            recojo.flagexpreso = 1;
            MyIFinalizarServicio = new IFinalizarServicio() {
                @Override
                public void onResult(JsonPack.Zona z, LatLng coords, String dir) {
                    recojo.DestinoDireccion = dir;
                    recojo.DestinoLat = coords.latitude;
                    recojo.DestinoLng = coords.longitude;
                    recojo.DestinoId = z != null ? z.id : "0";
                    recojo.tarifa = "0";

                    if (PolyUtil.containsLocation(new LatLng(lastLatitude, lastLongitude), Tools.generarArray(), false)) {
                        recojo.activo = "1";
                    } else {
                        recojo.activo = "0";
                    }

                    MRT.putParams("tiposervicio_id", "" + "1")
                            .putParams("origen_id", getServicioActual().OrigenId)
                            .putParams("destino_id", recojo.DestinoId)
                            .putParams("empresa_id", getClienteActual().empresa_id)
                            .send();

                }

                @Override
                public void onResult2(LatLng ll, String dir) {//para el tipo 2 que es por  kilometraje
                    recojo.DestinoDireccion = dir;
                    recojo.DestinoLat = ll.latitude;
                    recojo.DestinoLng = ll.longitude;
                    recojo.DestinoId = "0";
                    recojo.tarifa = "0";
                    if (PolyUtil.containsLocation(new LatLng(lastLatitude, lastLongitude), Tools.generarArray(), false)) {
                        recojo.activo = "1";
                    } else {
                        recojo.activo = "2";
                    }
                    SI.emitDataCallback("FinalizarServicio", new Gson().toJson(recojo), a);

                }

                @Override
                public void onEmiting(JsonPack.ResponseTrarifa t) {


                    getServicioActual().Tarifa = "" + t.tarifa;
                    recojo.tarifa = "" + t.tarifa;
                    if (PolyUtil.containsLocation(new LatLng(lastLatitude, lastLongitude), Tools.generarArray(), false)) {
                        recojo.activo = "1";
                    } else {
                        recojo.activo = "2";
                    }
                    Log.e("FINSERV", "onEmiting: "+ new Gson().toJson(recojo));
                    SI.emitDataCallback("FinalizarServicio", new Gson().toJson(recojo), a);
                }
            };

            MyRequestJsonPackGoogleGeoCode.CancelarRequest();
            MyRequestJsonPackGoogleGeoCode.putParams("latlng", lastLatitude + "," + lastLongitude)
                    .putParams("sensor", "false")
                    .putParams("key", Urls.GOOGLE_KEY)
                    .setShowLogs(false)
                    .send();
        } else if ((getServicioActual().DestinoId != null && !getServicioActual().DestinoId.equals("0"))) {
            if (PolyUtil.containsLocation(new LatLng(lastLatitude, lastLongitude), Tools.generarArray(), false)) {
                recojo.activo = "1";
            } else {
                recojo.activo = "2";
            }

            SI.emitDataCallback("FinalizarServicio", new Gson().toJson(recojo), a);
            Log.i(">", "no es expreso");

        }

    }






    IFinalizarServicio MyIFinalizarServicio;

    public interface IFinalizarServicio {
        void onResult(JsonPack.Zona z, LatLng coords, String dir);

        void onResult2(LatLng zonaactual, String dir);

        void onEmiting(JsonPack.ResponseTrarifa t);
    }


    MyRequest MyRequestJsonPackGoogleGeoCode = new MyRequest<JsonPack.GoogleGeoCode>(null, Urls.google_inversegeocode, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.GoogleGeoCode object) {

            if (object.results.size() > 0) {
                Log.d("caiman", "object.results.size(): " + object.results.size());
                //object.results.get(0).formatted_address;

                localizarZona(new LatLng(object.results.get(0).geometry.location.lat, object.results.get(0).geometry.location.lng), object.results.get(0).formatted_address);
            } else {
                localizarZona(new LatLng(0, 0), "Dirección no localizada");
            }
        }

        @Override
        public void onFailedRequest(ResponseWork rw, boolean isActive) {
            localizarZona(new LatLng(0, 0), "Dirección no localizada");
        }
    };


    AsyncTask<LatLng, Integer, JsonPack.Zona> TareaLocalizar;

    public synchronized void localizarZona(final LatLng zonaactual, final String direccion) {
        if (((AppForeGround) getApplication()).flag_tipotarifa == 1) {
            if (TareaLocalizar != null) {
                TareaLocalizar.cancel(true);
            }
            TareaLocalizar = new AsyncTask<LatLng, Integer, JsonPack.Zona>() {
                @Override
                protected JsonPack.Zona doInBackground(LatLng... params) {
                    JsonPack.Zona zona = null;
                    if (params[0].latitude != 0.0 && params[0].longitude != 0.0) {
                        for (JsonPack.Zona Z : ((AppForeGround) getApplication()).getZonas()) {
                            if (PolyUtil.containsLocation(params[0], Z.getLtns(), false)) {
                                zona = Z;
                                break;
                            }
                        }
                    }
                    return zona;
                }

                @Override
                protected void onPostExecute(JsonPack.Zona zon) {

                    //validar zona null
                    if (MyIFinalizarServicio != null)
                        MyIFinalizarServicio.onResult(zon, zonaactual, direccion);
                    if (zon != null) {
                        Log.d("localizarZonas", "Localizada!!");
                    } else {
                        Log.e("localizarZonas", "No Localizada");
                    }
                }
            };
            TareaLocalizar.execute(zonaactual);
        } else if (((AppForeGround) getApplication()).flag_tipotarifa == 2) {
            if (MyIFinalizarServicio != null)
                MyIFinalizarServicio.onResult2(zonaactual, direccion);

        }


    }


    MyRequest<JsonPack.ResponseTrarifa> MRT = new MyRequest<JsonPack.ResponseTrarifa>(Urls.ws_consultar_tarifa, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.ResponseTrarifa object) {

            if (object.status) {
                //""+object.tarifa;
                //String.format( "%.2f", object.tarifa);

                if (MyIFinalizarServicio != null)
                    MyIFinalizarServicio.onEmiting(object);

            } else {
                JsonPack.ResponseTrarifa tar = new JsonPack.ResponseTrarifa();
                tar.status = false;
                tar.tarifa = 0.0f;
                if (MyIFinalizarServicio != null)
                    MyIFinalizarServicio.onEmiting(tar);
            }

        }
    };

    public void CalificarCliente(float calificacion, String mensaje) {
        new MyRequest<JsonPack.ResponseLogin>(Urls.ws_calificacion, MyRequest.HttpRequestType.GET) {

            @Override
            public void onSucces(ResponseWork rw, boolean isActive) {

            }
        }
                .putParams("tipo", "2")
                .putParams("servicio_id", ServicioActual.IdServicio)
                .putParams("cliente_id", ServicioActual.IdCliente)
                .putParams("conductor_id", ServicioActual.IdConductor)
                .putParams("calificacion", "" + calificacion)
                .putParams("mensaje", mensaje)
                .send();
    }

    MyGps MYGPS;
    public SocketIo SI;
    ISocket isoc;

    SocketIoBridge SOIB;
    SocketIoBridge.AuxiliarBridge AXB;

    public void setAuxiliarBridge(SocketIoBridge.AuxiliarBridge axb) {
        AXB = axb;
    }

    public void setSocketIoBridge(SocketIoBridge s) {
        SOIB = s;
        if (SOIB != null) {

            Log.i("setSocketIoBridge", "SocketIoBridge OK");
            /*
            if(TEMP_NS!=null)
            {
                SOIB.nuevaSolicitudConductor(TEMP_NS);
                TEMP_NS=null;
            }*/
            if (Solicitudes != null && Solicitudes.size() > 0) {
                Log.i("setSocketIoBridge", "Hay Solicitudes");
                enviarNotificaciones();
            } else {
                NM.cancel(notidpp);
                Log.i("setSocketIoBridge", "NO hay solicitudes");
            }
        } else {
            Log.i("setSocketIoBridge", "Null SocketIoBridge");
        }
    }


    ArrayList<JsonPack.NuevaSolicitud> Solicitudes;


    public void enviarNotificaciones() {
        for (JsonPack.NuevaSolicitud ns : Solicitudes) {
            if (SOIB != null) {
                SOIB.nuevaSolicitudConductor(ns);
            }

        }
        Solicitudes.clear();
        NM.cancel(notidpp);
    }


    JsonPack.UpdateGeoConductor geoConductor;

    public JsonPack.UpdateGeoConductor getGeoConductor() {
        return geoConductor;
    }

    public JsonPack.Cliente getClienteActual() {
        if (ClienteActual == null) {
            Log.e("getClienteActual", "es null");
        } else {
            Log.d("getClienteActual", "es OK");
        }
        return ClienteActual;
    }

    public void EnviarCancelarServicio() {

        if (ServicioActual != null) {
            JsonPack.RespCancelarServicio cs = new JsonPack.RespCancelarServicio();
            cs.conductor_id = new SPUser(getApplication()).getResponseLogin().conductor_id;
            cs.servicio_id = ServicioActual.IdServicio;
            cs.usuario = "2";//CONDUCTOR
            cs.tipo = "2";//ANDROID

            SI.emitData("CancelarService", new Gson().toJson(cs));
        }

    }

    int notidpp = 10;

    public JsonPack.Servicio getServicioActual() {
        if (ServicioActual == null) {
            Log.e("getServicioActual", "es null");
        } else {
            Log.d("getServicioActual", "es OK");
        }
        return ServicioActual;
    }


    public void AceptarNuevaReserva(JsonPack.NuevaReserva Reserva) {
        SI.emitData("SolicitudAceptarReservaMovil", new Gson().toJson(Reserva));

        if (Reserva.aceptado != 2) { //SI EL CONDUCTOR ACEPTA= 1 / NO ACEPTA= 2
            prepararaReserva(Reserva);
        }
    }

    public void ReservaIniciarServicio() {
        Log.i("ReservaIniciarServicio", "llega al appbackground....");


        SPUser sptax = new SPUser(getApplication());

        JsonPack.InicarReserva ir = new JsonPack.InicarReserva();
        ir.conductor_id = sptax.getResponseLogin().conductor_id;
        ir.reserva_id = sptax.getReservaPendiente().reserva_id;


        SI.emitData("ReservaIniciada", new Gson().toJson(ir));

        Intent i = new Intent(this, RootSplash.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }


    public void playAudio(int i) {
        switch (i) {
            case 1:
                try {
                    audio1.prepare();
                } catch (IllegalStateException e) {
                    Log.e(AppBackground.class.getSimpleName(), "audio1>" + e.getMessage());
                } catch (IOException e) {
                    Log.e(AppBackground.class.getSimpleName(), "audio1>" + e.getMessage());
                }
                audio1.start();
                break;
            case 2:
                try {
                    audio2.prepare();
                } catch (IllegalStateException e) {
                    Log.e(AppBackground.class.getSimpleName(), "audio1>" + e.getMessage());
                } catch (IOException e) {
                    Log.e(AppBackground.class.getSimpleName(), "audio1>" + e.getMessage());
                }
                audio2.start();
                break;
        }

    }

    public double lastLatitude = 0;
    public double lastLongitude = 0;

    public JsonPack.Servicio ServicioActual;
    public JsonPack.Cliente ClienteActual;
    MediaPlayer audio1, audio2;

    public double latitudConductor, longitudConductor;

    public double distancia_acumulada = 0.0d;
    SPUser SPTAX;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(AppBackground.class.getSimpleName(), "onCreste Servicio");
        audio1 = MediaPlayer.create(getApplicationContext(), R.raw.audio1);
        audio2 = MediaPlayer.create(getApplicationContext(), R.raw.audio2);

        startForeground(1, Notificaciones.getNotificacionEncender(getApplicationContext()));
        NM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        SOIB = new SocketIoBridge();
        isoc = new ISocket() {

            @Override
            public void ReservaPendienteConductor(JsonPack.ReservaPendiente r) {

                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.ReservaPendienteConductor(r);
            }



            @Override
            public void finalizarConTarjeta(String json) {
                Log.e("AppBackground", "finalizarConTarjeta:" + json);
                SOIB.finalizarConTarjeta(json);
            }

            @Override
            public void refreshCoordinates(double lat, double lng) {
                System.out.println("refreshCoordinates(Location location) en APPBACKGROUND {");
                Log.d("LATITUD CONDUCTOR =", "" + lat);
                Log.d("LONGITUD CONDUCTOR =", "" + lng);
                System.out.println("}");
                if (SOIB != null) {
                    SOIB.refreshCoordinates(lat, lng);
                }
            }

            @Override
            public void ReservaProxima(JsonPack.ReservaAsignada s) {
                //ReservaIniciarServicio(s.Reserva);//esto no va, segun la hora de la reserva programar un notificacion
                //new SPUser(getApplication()).setReservas(s.Reserva);
                Log.e("reserva", "Reserva: " + s);
            }

            @Override
            public void respUpdateSocketCliente(final String s) {
            }

            //@Override public void refreshCoordinates(final JsonPack.respUpdateGeoConductor ru) {}
            @Override
            public void respUpdateSocketConductor(final String s) {
                System.out.println("UPDATING DRIVERS SOCKET!!!!!!!!!!***********************");
            }

            @Override
            public void solicitudListaTaxi(final JsonPack.AceptaTaxi a) {
            }


            @Override
            public void alertaTaxiWeb(final JsonPack.Alerta a) {
            }

            @Override
            public void NotificacionReserva(JsonPack.NodeNuevaReserva s) {//llega y muestra el dialogo de reserva aceptar/rechazar

                if (s.Reserva.reserva_id != null) {
                    if (SPTAX == null)
                        SPTAX = new SPUser(getApplication());

                    SPTAX.setReservaPendiente(s.Reserva);
                    //prepararaReserva(s.Reserva);
                }

                playAudio(1);
                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null)) {

                    if (AXB != null) {

                        AXB.onNodeNuevaReserva(s.Reserva);
                        //new SPUser(getApplication()).setReservas(s.Reserva);
                    }
                } else {
                    //NM.cancel(notidpp);
                    Log.d(">nueva notificacion", "MOSTRANDO NOTIFICACION");
                    NM.notify(56467, Notificaciones.getNotificacionSolicitudReserva(s.Reserva, getApplicationContext()));
                }
            }

            @Override
            public void NotificacionReservaAceptada(JsonPack.NodeNuevaReserva s) {

                if (s.Reserva.reserva_id != null) {
                    if (SPTAX == null)
                        SPTAX = new SPUser(getApplication());
                    SPTAX.setReservaPendiente(s.Reserva);
                    prepararaReserva(s.Reserva);
                }

            }

            @Override
            public void connect(String s) {
                //Log.d("id", ">" + SI.SOCKET.id());
                //Message message = mHandler.obtainMessage(1, "");
                //message.sendToTarget();

                SPTAX = new SPUser(getApplication());
                MYGPS.turnOn(true);
                JsonPack.UpdateConductorSocket ucs = new JsonPack.UpdateConductorSocket();
                ucs.idConductor = Session.conductor_id;
                ucs.socketConductor = SI.SOCKET.id();

                SI.emitData("setUpdateSocketConductor", new Gson().toJson(ucs));

                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.connect(s);
                else
                    Log.e(SocketIo.class.getSimpleName(), "connect null bridge ");

                startForeground(1, Notificaciones.getNotificacionDetener(getApplicationContext()));

            }

            @Override
            public void FinalizarServicioTaxista(JsonPack.RespFinalizarServ s) {
                Log.d("FinalizarServicioClie", ">" + s);

                ServicioActual.Estado = "6";
                ((AppForeGround) getApplication()).flag_califico = 1;
                playAudio(2);

                guardarVale(new SPUser(getApplicationContext()).getCodigoVale(),ServicioActual.IdServicio,s);

                /*
                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.FinalizarServicioTaxista(s);
                */
            }

            @Override
            public void nuevaSolicitudConductor(JsonPack.NuevaSolicitud ns) {

                playAudio(1);
                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null)) {
                    NM.cancel(notidpp);
                    SOIB.nuevaSolicitudConductor(ns);
                    new SPUser(getApplication().getApplicationContext()).setCodigoVale(ns.vale);


                } else {

                    if (Solicitudes == null)
                        Solicitudes = new ArrayList<>();

                    Solicitudes.add(ns);

                    if (AXB != null && (AppConexion != null && AppConexion.getRoot() != null)) {
                        AXB.onNuevasSolicitudes(Solicitudes.size());
                    } else {
                        //NM.cancel(notidpp);
                        NM.notify(notidpp, Notificaciones.getNotificacionSolicitud(getApplicationContext(), Solicitudes.size()));
                    }

                }

            }

            @Override
            public void ServicioCaducado(JsonPack.ServicioCad servicio) {

                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.ServicioCaducado(servicio);
                else
                    Log.e(SocketIo.class.getSimpleName(), "ServicioCaducadonull bridge ");


            }

            @Override
            public void ConductorSeleccionado(JsonPack.RespEstadoCondcutor s) {

                distancia_acumulada = 0.0d;
                new SPUser(getApplicationContext()).setAdiconales(null).setDistanciaRecorrida(null);

                playAudio(2);
                ServicioActual = s.Servicio;
                ClienteActual = s.Cliente;
                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.ConductorSeleccionado(s);
                else
                    Log.e(SocketIo.class.getSimpleName(), "ConductorSeleccionado null bridge ");

            }


            //Danny 11-08-2016
            //Inicio listerner
            @Override
            public void ServicioCancelado(JsonPack.CancelarServicio s) {
                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null)) {
                    Log.d("entro?", "ServicioCancelado: ");
                    SOIB.ServicioCancelado(s);
                } else {
                    NM.notify(notidpp, Notificaciones.getNotificationCancelarServicio(getApplicationContext()));
                    Log.e(SocketIo.class.getSimpleName(), "ConductorSeleccionado null bridge ");
                }
            }
            //Fin


            @Override
            public void reconnect(String s) {
                Log.d("socket", ">reconnect");
            }

            @Override
            public void disconnect(String s) {
                Log.d("socket", ">disconnect :p");


                if (MYGPS != null)
                    MYGPS.turnOff();

                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.disconnect(s);
                else
                    Log.e(SocketIo.class.getSimpleName(), "disconnect null bridge ");

                Log.i("disconnect", "-----------------before noti");
                startForeground(1, Notificaciones.getNotificacionEncender(getApplicationContext()));
                Log.i("disconnect", "-----------------after noti");
                //SI.SOCKET.close();
            }

            @Override
            public void connect_error(String s) {
                Log.d("socket", ">connect_error");
            }

            @Override
            public void connect_tiemout(String s) {
                Log.d("socket", ">connect_tiemout");
            }

            @Override
            public void contestar(final JsonPack.Testnode t) {
                Log.d("HECHO ", ">" + t.nombre);
            }
        };

        SI = new SocketIo(isoc, Urls.servidornodejssocket);

        MYGPS = new MyGps(getApplicationContext(), new MyGps.IMyGps() {


            @Override
            public void onGpsTurnOn() {

            }

            @Override
            public void onGpsTurnOff(Status status) {


            }

            @Override
            public void onLocation(Location l) {

                System.out.println("CALLING METHOD onLocation(Location l)***************************************");


                if (Session != null) {
                    JsonPack.UpdateGeoConductor ucg = new JsonPack.UpdateGeoConductor();

                    ucg.idConductor = Session.conductor_id;
                    ucg.latitud = l.getLatitude();
                    ucg.longitud = l.getLongitude();


                    if (getServicioActual() != null) {
                        if (getServicioActual().Estado.equals("3")) {
                            if (lastLatitude != 0.0d && lastLongitude != 0.0d) {

                                float[] result = new float[4];
                                Location.distanceBetween(lastLatitude, lastLongitude, l.getLatitude(), l.getLongitude(), result);
                                if (result != null) {
                                    if (result.length > 0) {
                                        //validar distancia minima para registro del DELTA
                                        Log.i("distancia delta", ">" + result[0] + " |total>" + distancia_acumulada);
                                        distancia_acumulada += result[0];
                                        if (SPTAX != null) {
                                            JsonPack.DistanciaRecorrida dr = new JsonPack.DistanciaRecorrida();
                                            dr.idServicio = getServicioActual().IdServicio;
                                            dr.distancia = distancia_acumulada;
                                            SPTAX.setDistanciaRecorrida(dr);
                                        }
                                    }
                                }

                            }
                        }
                    }

                    lastLatitude = l.getLatitude();
                    lastLongitude = l.getLongitude();

                    isoc.refreshCoordinates(lastLatitude, lastLongitude);

                    SI.emitData("setUpdateGeoConductor", new Gson().toJson(ucg));
                }
            }

            @Override
            public void onOldLocation(Location l) {

            }

            @Override
            public void onEstaApagado() {

            }

            @Override
            public void onFueApagado() {

            }
        });


    }

    public void guardarVale(String vale,String idServicio,final JsonPack.RespFinalizarServ s)
    {

        new MyRequest<JsonPack.ResponseGeneric>(Urls.save_vale, MyRequest.HttpRequestType.POST) {

            @Override
            public void onParseSuccesForeground(ResponseWork rw, JsonPack.ResponseGeneric object) {


                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.FinalizarServicioTaxista(s);


            }

        }
                .putParams("vale",vale)
                .putParams("idservicio",idServicio)
                .send();
    }

    private void updateDriversMarker(Location l) {
    }

    public void prepararaReserva(JsonPack.NuevaReserva Reserva) {
        //2016-08-05T23:21:00.000Z
        //2016-08-05 23:21:00.000
        Log.v("Reserva : ", ">" + Reserva.reserva_fecha);
        //SimpleDateFormat DiaMesAnio = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        //2016-08-22T22:14:00.000Z
        SimpleDateFormat DiaMesAnio = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");

        Date tempDate = null;
        try {
            String reserva_fecha = null;

            if (Reserva.reserva_fecha != null) {
                reserva_fecha = Reserva.reserva_fecha;
            } else {
                reserva_fecha = Reserva.cliente_fecha;
            }

            tempDate = DiaMesAnio.parse(reserva_fecha.replace("Z", "").replace("T", " "));
            //tempDate = DiaMesAnio.parse(reserva_fecha);
            //yyyy-MM-dd HH:mm:sss
        } catch (Exception ex) {
            Log.e("transformarFecha", ">" + ex.getMessage());
        }

        int restarsegundos = ((AppForeGround) getApplication()).Alerta * 1000 * 60;

        if (tempDate != null) {
            Calendar CAL = Calendar.getInstance();
            CAL.setTime(tempDate);
            CAL.add(Calendar.MILLISECOND, -restarsegundos);
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            Intent notificationIntent = new Intent(getApplicationContext(), BroadCastReserva.class);
            /*notificationIntent.putExtra("ori",Reserva.getDirecOri());
            notificationIntent.putExtra("des",Reserva.getDirecDes());
            notificationIntent.putExtra("fec",Reserva.getFechaRes());
            notificationIntent.putExtra("tar",Reserva.getTarifa());*/
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.set(AlarmManager.RTC_WAKEUP, CAL.getTimeInMillis(), pendingIntent);
            Log.i("reserva", "para ser notificado a la hora " + tempDate);

        } else {
            Log.e("tempDate", "Es null");
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_se_registro_reserva), Toast.LENGTH_SHORT).show();
        }
    }





}
