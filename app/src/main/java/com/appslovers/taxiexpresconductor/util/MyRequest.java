package com.appslovers.taxiexpresconductor.util;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.appslovers.taxiexpresconductor.data.IFrgamentStates;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.ParameterizedType;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by javierquiroz on 2/06/16.
 */
@SuppressWarnings("unchecked")
public class MyRequest<T> implements IFrgamentStates {

    public boolean bActive = true;

    HttpRequestType TYPE;
    Bundle bundle;
    String httpurl;

    public boolean showlogs = true;

    public MyRequest(IMethodFragmentStates mf, String url, HttpRequestType type) {
        TYPE = type;

        httpurl = url;

        if (mf != null)
            mf.setIfs(MyRequest.this);

        bActive = true;
    }

    public MyRequest setShowLogs(boolean b) {
        showlogs = b;
        return this;
    }

    public MyRequest(String url, HttpRequestType type) {
        TYPE = type;
        httpurl = url;

        bActive = true;
    }

    public String DECOD = new String();

    public MyRequest(String url, HttpRequestType type, String decodeingformat) {
        TYPE = type;
        httpurl = url;
        DECOD = decodeingformat;
        bActive = true;
    }


    @SuppressWarnings("unchecked")
    private Class<T> ClaseParseo;

    public enum HttpRequestType {

        MULTIPART, DELETE, PUT, POST, GET, RAW;
    }

    public enum RequestReturnType {
        OK, FAIL
    }

    String urlparams;

    public MyRequest setParametros(Bundle params) {

        bundle = params;
        buildUrlParams();

        return this;
    }


    public void buildUrlParams() {

        Uri.Builder builder = new Uri.Builder();
                /*
                .appendQueryParameter("firstParam", paramValue1)
                .appendQueryParameter("secondParam", paramValue2)
                .appendQueryParameter("thirdParam", paramValue3);*/


        StringBuffer sb = new StringBuffer();
        for (String key : bundle.keySet()) {
            Object value = bundle.get(key);

            if (value != null) {

                builder.appendQueryParameter(key, value.toString());
                //sb.append(key + "=" + value + "&");


            }

        }
        /*
        urlparams = sb.toString();
        urlparams = urlparams.substring(0, urlparams.length() - 1).replace(" ","%20");*/

        urlparams = builder.build().getEncodedQuery();

        Log.i("urlparams", ">" + urlparams);


    }

    ArrayList<MyRequestValidaror> Validators = new ArrayList<>();

    public MyRequest putParams(String key, String value, MyRequestValidaror.Validator mrv) {
        if (bundle == null)
            bundle = new Bundle();


        Validators.add(new MyRequestValidaror(value).setValidator(mrv));
        bundle.putSerializable(key, value);
        return this;
    }

    public MyRequest putParams(String key, String value, MyRequestValidaror.Validator mrv, int indexvalition) {
        if (bundle == null)
            bundle = new Bundle();


        Validators.add(new MyRequestValidaror(value, indexvalition).setValidator(mrv));
        bundle.putSerializable(key, value);
        return this;
    }


    public MyRequest putParams(String key, String value) {
        if (bundle == null)
            bundle = new Bundle();

        bundle.putSerializable(key, value);
        return this;
    }

    String RAW;

    public MyRequest putRawContent(String raw) {
        RAW = raw;
        return this;
    }


    public void onFailedRequest(ResponseWork rw, boolean isActive) {
    }

    public void onFailedRequestForeground(ResponseWork rw) {
    }

    public void onFailedRequestBackground(ResponseWork rw) {
    }

    public void onSucces(ResponseWork rw, boolean isActive) {

    }

    public void onSuccesForeground(ResponseWork rw) {

    }

    public void onSuccesBackground(ResponseWork rw) {

    }

    @SuppressWarnings("unchecked")
    public void onParseSucces(ResponseWork rw, boolean isActive, T object) {

    }

    @SuppressWarnings("unchecked")
    public void onParseSuccesForeground(ResponseWork rw, T object) {

    }

    @SuppressWarnings("unchecked")
    public void onParseSuccesBackground(ResponseWork rw, T object) {

    }

    @SuppressWarnings("unchecked")
    public void onValidateEx(String v, int index) {


    }


    public void CancelarRequest() {
        //Task
        if (Task != null) {
            Log.v("REQUEST FROM " + Task.hashCode(), "CANCELADO MANUALMENTE");
            if (Task.getStatus() == AsyncTask.Status.RUNNING || Task.getStatus() == AsyncTask.Status.PENDING)
                Task.cancel(true);

        } else {
            Log.v("REQUEST FROM NULL", "CANCELADO MANUALMENTE");
        }
    }

    @Override
    public void onFragResume() {

        bActive = true;
    }

    @Override
    public void onFragStop() {

        bActive = false;
    }

    public MyRequest send() {
        int i = 0;
        if (Validators.size() > 0) {
            for (MyRequestValidaror mrv : Validators) {
                i++;
                if (mrv != null && !mrv.Validador.validate(mrv.Value)) {
                    Log.e(MyHttpRequest.MyRequest.class.getSimpleName(), "> Validacion Exception " + mrv.Value);
                    onValidateEx(mrv.Value, mrv.checkValue > 0 ? mrv.checkValue : i);
                    return this;
                }
            }

        }
        prepare();
        Task.execute();
        return this;
    }


    int readTimeOut = 10000;
    int connectTimeOut = 15000;
    AsyncTask<Bundle, Integer, ResponseWork> Task;

    public void prepare() {
        try {
            this.ClaseParseo = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            Log.d("Parseo", ">" + this.ClaseParseo.getSimpleName());
        } catch (ClassCastException e) {
            Log.e(ClassCastException.class.getSimpleName(), ">" + e.getMessage());
        }


        Task = new AsyncTask<Bundle, Integer, ResponseWork>() {

            @Override
            protected ResponseWork doInBackground(Bundle... params) {


                ResponseWork rw = new ResponseWork();
                try {


                    //if(urlparams==null)
                    buildUrlParams();

                    String buildurl = httpurl;
                    if (TYPE.equals(HttpRequestType.GET)) {
                        buildurl = httpurl + "?" + urlparams;//(urlparams.replace(" ", "%20"));

                    }


                    //URL url = new URL(URLEncoder.encode(buildurl.toString(),"utf-8"));
                    URL url = new URL(buildurl);

                    Log.v(MyRequest.class.getSimpleName(), ">" + url.toString());

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(readTimeOut);
                    conn.setConnectTimeout(connectTimeOut);
                    conn.setDoInput(true);


                    DataOutputStream wr;
                    switch (TYPE) {
                        case GET:

                            conn.setRequestMethod("GET");

                            break;

                        case POST:
                            conn.setDoOutput(true);
                            conn.setRequestMethod("POST");

                            wr = new DataOutputStream(conn.getOutputStream());
                            //if(!(urlparams.length()>0))
                            buildUrlParams();

                            wr.writeBytes(urlparams);
                            wr.flush();
                            wr.close();

                            break;

                        case RAW:

                            conn.setRequestMethod("POST");

                            wr = new DataOutputStream(conn.getOutputStream());
                            wr.writeBytes(RAW);
                            wr.flush();
                            wr.close();
                            break;
                    }

                    BufferedReader in;
                    if (DECOD.length() > 0)
                        in = new BufferedReader(new InputStreamReader(conn.getInputStream(), DECOD));
                    else
                        in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    String inputLine;
                    StringBuffer response = new StringBuffer();


                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine + "\n");
                    }
                    rw.ResponseBody = response.toString();
                    rw.Type = RequestReturnType.OK;

                    if (conn.getResponseCode() != 200) {
                        rw.Type = RequestReturnType.FAIL;
                        rw.ResponseBody = "Response Code : " + rw.ResponseBody + " | ResponseMessage : ";
                        Log.e("RespCodeException", ">" + rw.ResponseBody);
                    }


                    in.close();
                    conn.disconnect();


                }
                /*
                catch (URISyntaxException e)
                {
                    rw.Type=RequestReturnType.FAIL;
                    rw.ResponseBody=e.getMessage();
                    Log.e(URISyntaxException.class.getSimpleName(),">"+e.getMessage());
                }*/ catch (MalformedURLException e) {
                    rw.Type = RequestReturnType.FAIL;
                    rw.ResponseBody = e.getMessage();
                    Log.e(MalformedURLException.class.getSimpleName(), ">" + e.getMessage());
                } catch (ProtocolException e) {
                    rw.Type = RequestReturnType.FAIL;
                    rw.ResponseBody = e.getMessage();
                    Log.e(ProtocolException.class.getSimpleName(), ">" + e.getMessage());
                } catch (IOException e) {
                    rw.Type = RequestReturnType.FAIL;
                    rw.ResponseBody = e.getMessage();
                    Log.e(IOException.class.getSimpleName(), ">" + e.getMessage());
                }

                return rw;
            }

            @Override
            protected void onPostExecute(ResponseWork RW) {
                if (showlogs)
                    Log.d("response", ">" + RW.ResponseBody);

                if (RW.Type.equals(RequestReturnType.OK)) {
                    if (ClaseParseo != null) {
                        Object rp = null;
                        try {
                            rp = new Gson().fromJson(RW.ResponseBody, ClaseParseo);
                        } catch (JsonParseException e) {
                            Log.e(JsonParseException.class.getSimpleName(), ">" + e.getMessage());
                        }

                        if (rp != null) {
                            onParseSucces(RW, bActive, (T) rp); //onParseSucces(RW, bActive, ClaseParseo.cast(rp));
                            if (bActive) {
                                onParseSuccesForeground(RW, (T) rp);
                            } else {
                                onParseSuccesBackground(RW, (T) rp);
                            }

                        } else {
                            onSucces(RW, bActive);
                            if (bActive) {
                                onSuccesForeground(RW);
                            } else {
                                onSuccesBackground(RW);
                            }
                        }
                    } else {

                        onSucces(RW, bActive);
                        if (bActive) {
                            onSuccesForeground(RW);
                        } else {
                            onSuccesBackground(RW);
                        }

                    }
                } else if (RW.Type.equals(RequestReturnType.FAIL)) {
                    onFailedRequest(RW, bActive);
                    if (bActive) {
                        onFailedRequestForeground(RW);
                    } else {
                        onFailedRequestBackground(RW);
                    }
                }

            }
        };
    }


    public class ResponseWork {
        public int ResponseCode;
        public RequestReturnType Type;
        public String ResponseBody;
    }






    /*
    public  String postRequest( String mainUrl,HashMap<String,String> parameterList)
    {
        String response="";
        try {
            URL url = new URL(mainUrl);

            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String, String> param : parameterList.entrySet())
            {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }

            byte[] postDataBytes = postData.toString().getBytes("UTF-8");




            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);

            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            StringBuilder sb = new StringBuilder();
            for (int c; (c = in.read()) >= 0; )
                sb.append((char) c);
            response = sb.toString();


        return  response;
        }catch (Exception excep){
            excep.printStackTrace();}
        return response;
    }
     */
}
