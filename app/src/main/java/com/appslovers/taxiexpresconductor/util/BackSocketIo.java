package com.appslovers.taxiexpresconductor.util;

import android.util.Log;


import com.appslovers.taxiexpresconductor.data.ISocket;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.ArrayList;


/**
 * Created by javierquiroz on 3/06/16.
 */
public class BackSocketIo {

    public Socket SOCKET;
    //ISocket iSocket;



    ArrayList<String> Eventos;
    ArrayList<Class> Clases;
    ISocket ISOK;
    Gson GSON;
    public BackSocketIo(ISocket iSocket, ArrayList<String> eventos, ArrayList<Class> clases)
    {
        ISOK=iSocket;
        GSON=new Gson();

        Eventos=new ArrayList<>();
        Eventos.add(Socket.EVENT_CONNECT);
        Eventos.add(Socket.EVENT_DISCONNECT);
        Eventos.add(Socket.EVENT_CONNECT_ERROR);
        Eventos.add(Socket.EVENT_CONNECT_TIMEOUT);
        Eventos.addAll(eventos);

        Clases=new ArrayList<Class>();
        Clases.add(String.class);
        Clases.add(String.class);
        Clases.add(String.class);
        Clases.add(String.class);
        Clases.addAll(clases);




    }
    public static   class MyListerner implements Emitter.Listener
    {
        public String myevent="";



        @Override
        public void call(Object... args) {

        }
    }

    public void Conectar()
    {

        try {
            SOCKET =  com.github.nkzawa.socketio.client.IO.socket("http://safytaxi.tutaxidispatch.com:8081");


        } catch (URISyntaxException e) {

            Log.e(e.getClass().getSimpleName(),">"+e.getMessage());

        }





        if(SOCKET!=null)
        {
            for(int i=0;i<Clases.size();i++)
            {
                final int iii=i;
                Log.d("seteando", ">" + Eventos.get(i));





                MyListerner LEMITER= new MyListerner() {


                    @Override
                    public void call(Object... args) {

                        String s = getOneString(args);

                        /*

                        try {
                            Field privateField = Emitter.class.getDeclaredField("callbacks");
                            privateField.setAccessible(true);
                            ConcurrentMap<String, ConcurrentLinkedQueue<Emitter.Listener>> dl=
                                    (ConcurrentMap<String, ConcurrentLinkedQueue<Emitter.Listener>>)privateField.get(SOCKET);

                            for(String k:dl.keySet())
                            {
                                Log.v("key>", ">" + k);


                            }
                        }
                        catch (NoSuchFieldException e)
                        {
                            Log.d("nosusch",">"+e.getMessage());
                        }
                        catch (IllegalAccessException e)
                        {
                            Log.d("illegalac",">"+e.getMessage());
                        }*/




                        Log.v("call>>", ">" + s + " | "+myevent );
                        try {


                            Method[] METODOS = ISOK.getClass().getMethods();

                            Log.d("metodos size", ">" + METODOS.length);
                            for (int k = 0; k < METODOS.length; k++) {
                                //if(METODOS[k].getTypeParameters()[0].equals(Clases.get(iii)))
                                //if(METODOS[k].getReturnType().equals(Clases.get(iii)))
                                if (METODOS[k].getName().equals(this.myevent) ){
                                    Log.d("metodo name", " match >" + METODOS[k].getName());


                                    Log.v("getting met type",">");
                                    for(Class c:METODOS[k].getParameterTypes())
                                    {
                                        Log.v("getting met type",">"+c.getSimpleName());
                                    }

                                    Log.e("debug", "method name >" + METODOS[k].getName());
                                    //Method method = ISOK.getClass().getDeclaredMethod(METODOS[k].getName(), METODOS[k].getParameterTypes()[0].getClass());
                                    //Method method = ISocket.class.getDeclaredMethod(METODOS[k].getName(), METODOS[k].getParameterTypes()[0].getClass());
                                    //Method method = ISOK.getClass().getDeclaredMethod(METODOS[k].getName(), METODOS[k].getReturnType());

                                    //Object o = GSON.fromJson(s,METODOS[k].getReturnType());
                                    Object o=null;
                                    if(s!=null)
                                     o= GSON.fromJson(s,METODOS[k].getParameterTypes()[0].getClass());
                                    else
                                        Log.e("debug","Objec es null>");



                                    METODOS[k].invoke(ISOK, METODOS[k].getParameterTypes()[0].getClass().cast(o));

                                    //method.invoke(ISOK, METODOS[k].getReturnType().cast(o));
                                    break;
                                } else {
                                    Log.d("metodo name", "no match >" + METODOS[k].getName());
                                }
                            }

                        } catch (JsonParseException e) {
                            Log.e(e.getClass().getSimpleName(), Eventos.get(iii) + ">" + e.getMessage());

                        }/* catch (NoSuchMethodException e) {
                            Log.e(e.getClass().getSimpleName(), Eventos.get(iii) + ">" + e.getMessage());
                        }*/
                        catch (IllegalStateException e) {
                            Log.e(e.getClass().getSimpleName(), Eventos.get(iii) + ">" + e.getMessage());
                        } catch (IllegalAccessException e) {
                            Log.e(e.getClass().getSimpleName(), Eventos.get(iii) + ">" + e.getMessage());
                        } catch (InvocationTargetException e) {
                            Log.e(e.getClass().getSimpleName(), Eventos.get(iii) + ">" + e.getMessage());
                        }

                    }

                };
                LEMITER.myevent=Eventos.get(i);

                SOCKET.on(Eventos.get(i), LEMITER);
            }

            SOCKET.connect();
            Log.d("socke","Conectando");
        }
        else
        {
            Log.e("SOCKE", "null");
        }

  /*
                Emitter EM=new Emitter();
                EM
                .on("", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {

                    }
                })
                .on("", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {

                    }
                });*/
    }

    public String getOneString(Object... args)
    {
        String r=null;
        if (args != null) {
            for (Object o : args) {
                if (o != null)
                {
                    Log.v("getString parse ", "> " + o.toString());
                    r=o.toString();
                    break;
                }
                else
                {
                    //Log.v("objecto ", "> null");
                }

            }
        }
        return r;
    }


    /*
    public void implmentame()
    {

    }
    private Class<A> ClaseA;
    private Class<B> ClaseB;
    public void  hacer()
    {



        try {


            Type[] clases=((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments();
            if(clases.length>0)
            this.ClaseA= (Class<A>) clases[0];
            if(ClaseA!=null)Log.d("claseA",ClaseA.getSimpleName());

            if(clases.length>1)
            this.ClaseB = (Class<B>) clases[1];
            if(ClaseA!=null)Log.d("claseB",ClaseB.getSimpleName());




        }
        catch (ClassCastException e)
        {
            Log.e(ClassCastException.class.getSimpleName(), ">" + e.getMessage());
        }
    }*/






}
