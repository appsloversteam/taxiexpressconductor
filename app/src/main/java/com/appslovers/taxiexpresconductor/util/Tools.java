package com.appslovers.taxiexpresconductor.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by javierquirozgalindo on 19/11/15.
 */
public class Tools {


    public static String formatFloat(float f) {return String.format(Locale.US,"%.2f",f);}
    public static String formatDouble(double d) {return String.format(Locale.US,"%.2f",d);}

    public static int getInteger(String s)
    {
        if(s.contains(",")) {s=s.replace(",",".");}
        int f=0;
        try {f=Integer.parseInt(s);} catch (NumberFormatException e) {Log.e(e.getClass().getSimpleName(),">"+e.getMessage());}
        return f;
    }
    public static float getFloat(String s)
    {
        if(s.contains(",")) {s=s.replace(",",".");}
        float f=0.0f;
        try {f=Float.parseFloat(s);} catch (NumberFormatException e) {Log.e(e.getClass().getSimpleName(),">"+e.getMessage());}
        return f;
    }

    public static double getDouble(String s)
    {
        if(s.contains(",")) {s=s.replace(",",".");}
        double f=0.0d;
        try {f=Double.parseDouble(s);} catch (NumberFormatException e) {Log.e(e.getClass().getSimpleName(),">"+e.getMessage());}
        return f;
    }

    public static void LogMe(Object c,Object m,int i)
    {
        String hc="Hashcode:null";
        if(c!=null)
        {
            hc=""+c.hashCode();
        }


        String classname="Classname:null";
        if(c!=null)
        {
            classname=c.getClass().getSimpleName();
        }
        switch (i)
        {
            case 1:

                if(m==null)
                    Log.i(classname, hc + ">>>>>NULL");
                else
                    Log.i(classname, hc + ">>>>>" + m);
                break;

            case 2:
                if(m==null)
                    Log.e(classname, hc + ">>>>>NULL");
                else
                    Log.e(classname, hc + ">>>>>" + m);
                break;

            case 3:

                if(m==null)
                    Log.v(classname, hc + ">>>>>NULL");
                else
                    Log.v(classname, hc + ">>>>>" + m);

                break;
        }
    }


    public static String getCertificateSHA1Fingerprint( Context mContext) {
        PackageManager pm = mContext.getPackageManager();
        String packageName = mContext.getPackageName();
        int flags = PackageManager.GET_SIGNATURES;
        PackageInfo packageInfo = null;
        try {
            packageInfo = pm.getPackageInfo(packageName, flags);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Signature[] signatures = packageInfo.signatures;
        byte[] cert = signatures[0].toByteArray();
        InputStream input = new ByteArrayInputStream(cert);
        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X509");
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        X509Certificate c = null;
        try {
            c = (X509Certificate) cf.generateCertificate(input);
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        String hexString = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] publicKey = md.digest(c.getEncoded());
            hexString = byte2HexFormatted(publicKey);
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (CertificateEncodingException e) {
            e.printStackTrace();
        }
        return hexString;
    }

    public static String byte2HexFormatted(byte[] arr) {
        StringBuilder str = new StringBuilder(arr.length * 2);
        for (int i = 0; i < arr.length; i++) {
            String h = Integer.toHexString(arr[i]);
            int l = h.length();
            if (l == 1) h = "0" + h;
            if (l > 2) h = h.substring(l - 2, l);
            str.append(h.toUpperCase());
            if (i < (arr.length - 1)) str.append(':');
        }
        return str.toString();
    }

    public static String keyhashfacebook(Context a)
    {
        String kh="";
        try {
            PackageInfo info = a.getPackageManager().getPackageInfo(a.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                kh= Base64.encodeToString(md.digest(), Base64.DEFAULT);
            }
        } catch (PackageManager.NameNotFoundException e) {

            kh=e.getMessage();

        } catch (NoSuchAlgorithmException e) {
            kh=e.getMessage();
        }

        return kh;
    }

    public static ArrayList<LatLng> generarArray()
    {
        ArrayList<LatLng> aeropuerto=new ArrayList<>();
        aeropuerto.add(new LatLng(-12.03652,-77.12728));
        aeropuerto.add(new LatLng(-12.0385,-77.1225));
        aeropuerto.add(new LatLng(-12.03923,-77.11917));
        aeropuerto.add(new LatLng(-12.03946,-77.11651));
        aeropuerto.add(new LatLng(-12.04015,-77.11355));
        aeropuerto.add(new LatLng(-12.0412,-77.10911));
        aeropuerto.add(new LatLng(-12.04133,-77.1077));
        aeropuerto.add(new LatLng(-12.04074,-77.10759));
        aeropuerto.add(new LatLng(-12.04015,-77.10782));
        aeropuerto.add(new LatLng(-12.03945,-77.10764));
        aeropuerto.add(new LatLng(-12.03925,-77.10738));

        aeropuerto.add(new LatLng(-12.0393,-77.10689));
        aeropuerto.add(new LatLng(-12.03904,-77.10597));
        aeropuerto.add(new LatLng(-12.03879,-77.10566));
        aeropuerto.add(new LatLng(-12.03836,-77.10358));
        aeropuerto.add(new LatLng(-12.03686,-77.10332));
        aeropuerto.add(new LatLng(-12.03553,-77.10157));
        aeropuerto.add(new LatLng(-12.03358,-77.10253));
        aeropuerto.add(new LatLng(-12.03007,-77.10426));

        aeropuerto.add(new LatLng(-12.02857,-77.10504));
        aeropuerto.add(new LatLng(-12.0266,-77.10608));
        aeropuerto.add(new LatLng(-12.02433,-77.10709));
        aeropuerto.add(new LatLng(-12.02345,-77.10529));
        aeropuerto.add(new LatLng(-12.01995,-77.10706));
        aeropuerto.add(new LatLng(-12.02083,-77.10887));
        aeropuerto.add(new LatLng(-12.01624, -77.11102));
        aeropuerto.add(new LatLng(-12.01685,-77.11239));
        aeropuerto.add(new LatLng(-12.01362,-77.11401));
        aeropuerto.add(new LatLng(-12.01211,-77.11092));
        aeropuerto.add(new LatLng(-12.00284,-77.11548));
        aeropuerto.add(new LatLng(-12.00135,-77.11629));
        aeropuerto.add(new LatLng( -12.00066,-77.11702));
        aeropuerto.add(new LatLng(-11.99986,-77.11859));
        aeropuerto.add(new LatLng(-11.99795,-77.12376));
        aeropuerto.add(new LatLng( -11.99671,-77.12631));
        aeropuerto.add(new LatLng(-11.99904, -77.12657));
        aeropuerto.add(new LatLng(-12.00311,-77.12707));
        aeropuerto.add(new LatLng(-12.00443,-77.12721));
        aeropuerto.add(new LatLng(-12.00643,-77.12758));
        aeropuerto.add(new LatLng(-12.00926,-77.12811));
        aeropuerto.add(new LatLng(-12.01631,-77.12955));
        aeropuerto.add(new LatLng(-12.01841,-77.12996));
        aeropuerto.add(new LatLng(-12.01938,-77.12998));
        aeropuerto.add(new LatLng(-12.02179,-77.12936));
        aeropuerto.add(new LatLng(-12.02603,-77.12848));
        aeropuerto.add(new LatLng(-12.03038,-77.12756));
        aeropuerto.add(new LatLng(-12.03086,-77.12762));
        aeropuerto.add(new LatLng(-12.03197,-77.12807));
        aeropuerto.add(new LatLng(-12.03275,-77.12805));
        aeropuerto.add(new LatLng(-12.03552,-77.12741));
        return aeropuerto;
    }
}

