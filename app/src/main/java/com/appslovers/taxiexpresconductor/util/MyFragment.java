package com.appslovers.taxiexpresconductor.util;

import android.support.v4.app.Fragment;
import android.view.View;

import com.appslovers.taxiexpresconductor.AppForeGround;
import com.appslovers.taxiexpresconductor.data.IFrgamentStates;

/**
 * Created by javierquiroz on 2/06/16.
 */
public class MyFragment extends Fragment implements IMethodFragmentStates {



    public View ViewRoot;

    IFrgamentStates IFS;

    @Override
    public void setIfs(IFrgamentStates ifs) {
        IFS=ifs;
    }

    public AppForeGround getApp()
    {
        return (AppForeGround)getActivity().getApplication();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(IFS!=null)
        IFS.onFragResume();
    }

    @Override
    public void onStop() {

        if(IFS!=null)
            IFS.onFragStop();
        super.onStop();

    }
}
