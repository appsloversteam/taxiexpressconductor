package com.appslovers.taxiexpresconductor.util;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.appslovers.taxiexpresconductor.data.CustomEmitterListener;
import com.appslovers.taxiexpresconductor.data.ISocket;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;


/**
 * Created by javierquiroz on 3/06/16.
 */
public class SocketIo {

    public Socket SOCKET;
    ISocket ISOK;


    //ArrayList<String> Eventos;
    // ArrayList<Class> Clases;

    String urlmain;
    Gson GSON;


    public SocketIo(ISocket iSocket, String url)//, ArrayList<String> eventos, ArrayList<Class> clases)
    {
        urlmain = url;
        ISOK = iSocket;
        GSON = new Gson();
        /*
        Eventos=new ArrayList<>();
        Eventos.add(Socket.EVENT_CONNECT);
        Eventos.add(Socket.EVENT_DISCONNECT);
        Eventos.add(Socket.EVENT_CONNECT_ERROR);
        Eventos.add(Socket.EVENT_CONNECT_TIMEOUT);
        Eventos.addAll(eventos);


        Clases=new ArrayList<Class>();
        Clases.add(String.class);
        Clases.add(String.class);
        Clases.add(String.class);
        Clases.add(String.class);
        Clases.addAll(clases);*/


        try {
            IO.Options options = new IO.Options();
            options.timeout = 5 * 1000;
            SOCKET = IO.socket(urlmain, options);


        } catch (URISyntaxException e) {
            Log.e(e.getClass().getSimpleName(), ">" + e.getMessage());
        }


        if (SOCKET != null) {

            for (Method IterMethd : ISocket.class.getDeclaredMethods()) {

                Log.i(Socket.class.getSimpleName(), "setEmitterOn>" + IterMethd.getName());
                if (true)//if(SOCKET.hasListeners(IterMethd.getName()))
                {

                    SOCKET.on(IterMethd.getName(), new CustomEmitterListener(IterMethd.getName()) {

                                @Override
                                public void call(Object... args) {

                                    String s = getOneString(args);

                                    Log.i(Socket.class.getSimpleName(), "call on(" + this.myEvent + ")");
                                    Log.i(Socket.class.getSimpleName(), "respoonse >" + s);

                                    try {

                                        boolean noencontrado = true;

                                        Method[] METODOS = ISOK.getClass().getDeclaredMethods();
                                        for (int k = 0; k < METODOS.length; k++) {

                                            //Log.i(Socket.class.getSimpleName(), "Scan method >"+METODOS[k].getName());

                                            if (METODOS[k].getName().equals(this.myEvent)) {

                                                noencontrado = false;

                                                //Log.i(Socket.class.getSimpleName(), "Match method >"+METODOS[k].getName());
                                                //for(Class c:METODOS[k].getParameterTypes()) {Log.v("getting met type for",">"+c.getSimpleName());}

                                                if (METODOS[k].getParameterTypes()[0].getSimpleName().equals("String")) {
                                                    // METODOS[k].invoke(ISOK, s);

                                                    MethodJson mj = new MethodJson();
                                                    mj.M = METODOS[k];
                                                    mj.J = s;
                                                    mj.B = true;
                                                    Message message = mHandler.obtainMessage(1, mj);
                                                    message.sendToTarget();

                                                } else {
                                                    Object o = null;
                                                    if (s != null) {
                                                        try {
                                                            Class c = METODOS[k].getParameterTypes()[0];

                                                             o = GSON.fromJson(s, c);
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else
                                                        Log.e(Socket.class.getSimpleName(), "response > null");


                                                    //Log.d("MetodoFirst Param Type", " NAME >" + METODOS[k].getParameterTypes()[0].getSimpleName());

                                                    MethodJson mj = new MethodJson();
                                                    mj.M = METODOS[k];
                                                    mj.J = o;
                                                    mj.B = false;
                                                    Message message = mHandler.obtainMessage(1, mj);
                                                    message.sendToTarget();
                                                    //METODOS[k].invoke(ISOK, METODOS[k].getParameterTypes()[0].cast(o));
                                                }

                                                break;

                                            }


                                        }

                                        if (noencontrado) {
                                            Log.e(Socket.class.getSimpleName(), "No matched method >" + this.myEvent);
                                        }

                                    } catch (UnsupportedOperationException e) {
                                        Log.e(e.getClass().getSimpleName(), this.myEvent + ">" + e.getMessage() + ">" + e.getCause().getMessage());
                                    }
                                    /*
                                    catch (IllegalAccessException e) {
                                        Log.e(e.getClass().getSimpleName(),  this.myEvent + ">" + e.getMessage()+">"+e.getCause().getMessage());
                                    }

                                    catch (InvocationTargetException e) {
                                        Log.e(e.getClass().getSimpleName(),  this.myEvent + "1>" + e.getMessage()+">"+e.getCause().getMessage());
                                    }*/ catch (JsonParseException e) {
                                        Log.e(e.getClass().getSimpleName(), this.myEvent + ">" + e.getMessage());

                                    }
                                    /*catch (NoSuchMethodException e) {
                                        Log.e(e.getClass().getSimpleName(), this.myEvent + ">" + e.getMessage());
                                    }*/ catch (IllegalStateException e) {
                                        Log.e(e.getClass().getSimpleName(), this.myEvent + ">" + e.getMessage());
                                    }


                                }

                            }
                    );

                }

            }
        }
    }

    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message message) {

            Method M = ((MethodJson) message.obj).M;
            Object O = ((MethodJson) message.obj).J;
            boolean b = ((MethodJson) message.obj).B;

            try {

                Log.i("what", ">" + message.what);
                if (message.what == 2) {

                    ((CallBack) O).onCallBack(((CallBack) O).res);
                } else {
                    if (b) {
                        M.invoke(ISOK, String.valueOf(O));
                    /*
                    if(O!=null) {
                        M.invoke(ISOK, O.toString());
                    else{
                     M.invoke(ISOK, "");
                    }*/
                    } else {
                        Object o = M.getParameterTypes()[0].cast(O);

                        if (o != null) {
                            M.invoke(ISOK, o);
                        }
                    }
                }

            } catch (InvocationTargetException e) {
                Log.e(e.getClass().getSimpleName(), M.getName() + ">handler>" + e.getMessage() + ">" + e.getCause().getMessage());
            } catch (IllegalAccessException e) {
                Log.e(e.getClass().getSimpleName(), M.getName() + ">handler>" + e.getMessage());
            }

        }
    };

    public void Desconectar() {
        if (SOCKET.connected())
            SOCKET.disconnect();
        else
            Log.d(this.getClass().getSimpleName(), ">socket desconectado");
    }

    public static class MethodJson {
        /*
        public static Method M;
        public static Object J;
        public static boolean B;*/
        public Method M;
        public Object J;
        public boolean B;
        public int T;
    }


    public void Conectar() {
        if (SOCKET != null) {
            SOCKET.connect();
        } else {
            Log.d(this.getClass().getSimpleName(), ">socket null");
        }


    }

    public static String getOneString(Object... args) {
        String r = null;
        if (args != null) {
            for (Object o : args) {
                if (o != null) {
                    Log.i(SocketIo.class.getSimpleName(), "getOneString>" + o.toString());
                    r = o.toString().trim();
                    break;
                } else {
                    Log.i(SocketIo.class.getSimpleName(), ">null");
                }

            }
        }
        return r;
    }

    public void emitData(String operation, String data) {
        if (SOCKET != null && SOCKET.connected()) {
            Log.i(this.getClass().getSimpleName(), operation + ">" + data);
            SOCKET.emit(operation, data);
        } else {
            Log.e(this.getClass().getSimpleName(), operation + ">" + data);
        }
    }

    public static class CallBack {
        Handler H;
        public String res;

        public Ack CallBack = new Ack() {
            @Override
            public void call(Object... args) {

                Log.i("callback", ">");
                res = SocketIo.getOneString(args);
                MethodJson mj = new MethodJson();
                mj.J = SocketIo.CallBack.this;
                mj.T = 1;

                Message message = H.obtainMessage(2, mj);
                message.sendToTarget();
            }
        };

        public CallBack setHandler(Handler h) {
            H = h;
            return this;
        }

        public void onCallBack(String s) {

        }
    }

    public void emitDataCallback(String operation, String data, Ack callback) {
        if (SOCKET != null && SOCKET.connected()) {
            Log.i(this.getClass().getSimpleName(), operation + ">" + data);
            SOCKET.emit(operation, data, callback);
        } else {
            Log.e(this.getClass().getSimpleName(), operation + ">" + data);
        }
    }

    public void emitDataCallback(String operation, String data, CallBack callback) {
        if (SOCKET != null && SOCKET.connected()) {
            Log.i(this.getClass().getSimpleName(), operation + ">" + data);
            //SOCKET.emit(operation, data, callback);

            SOCKET.emit(operation, data, callback.setHandler(mHandler).CallBack);
        } else {
            Log.e(this.getClass().getSimpleName(), operation + ">" + data);
        }
    }
}