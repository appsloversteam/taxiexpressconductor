package com.appslovers.taxiexpresconductor.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by javierquirozgalindo on 1/12/15.
 */
public class ConfirmDialog implements DialogInterface.OnClickListener {

    AlertDialog dialog;

    /*
    new ConfirmDialog(getWindow().getContext(),"","")
    {
        @Override
        public void onPositive() {

    }

        @Override
        public void onNegative() {

    }
    };*/
    public void dismisss() {
        dialog.dismiss();
    }

    public ConfirmDialog(Context c, String title) {
        constructor(c, "", title, "Sí", "No");
    }

    public ConfirmDialog(Context c, String message, String title) {
        constructor(c, message, title, "Sí", "No");
    }

    public ConfirmDialog(Context c, String message, String title, String yestext) {
        constructor(c, message, title, yestext, "No");
    }

    public ConfirmDialog(Context c, String message, String title, String yestext, String nottext) {
        constructor(c, message, title, yestext, nottext);
    }

    private void constructor(Context cc, String m, String t, String yt, String nt) {
        dialog = new AlertDialog.Builder(cc, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                .setTitle(t)
                .setMessage(m)
                .setNegativeButton(nt, this)
                .setPositiveButton(yt, this)
                .create();
    }

    public void show() {
        dialog.show();
    }

    public void showQuestion(String message) {
        dialog.setMessage(message);
        dialog.show();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            //positive
            case -1:
                onPositive();
                break;
            //negative
            case -2:
                onNegative();
                break;
            //neutral
            case -3:
                break;
        }

    }

    public void onPositive() {

    }

    public void onNegative() {

    }
}
