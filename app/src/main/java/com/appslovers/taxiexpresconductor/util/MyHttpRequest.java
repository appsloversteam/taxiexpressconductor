package com.appslovers.taxiexpresconductor.util;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;


import com.appslovers.taxiexpresconductor.data.IFrgamentStates;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by javierquiroz on 2/06/16.
 */
public class MyHttpRequest implements Application.ActivityLifecycleCallbacks{


    public MyHttpRequest() {


    }

    Activity A;
    MyRequest MR;
    Map<String,MyRequest> Requests;

    boolean flagSttoped=false;

    public void prepare(MyRequest mr) {
        MR = mr;
        A=MR.aa;
    }

    public void prepare(String tag,MyRequest mr)
    {
        if(Requests==null)
            Requests=new HashMap<String, MyRequest>();
        Requests.put(tag,mr);
    }


    public void  changeFlag(boolean b)
    {
        Log.v("changeFlag","--------------"+String.valueOf(b));
        flagSttoped=b;

    }

    public static class MyRequest implements IFrgamentStates {

        public  boolean bActive;
        public Activity aa;
        public MyRequest(MyFragment mf) {
             aa= mf.getActivity();
             mf.setIfs(MyRequest.this);
             bActive=true;

        }


        public void onResponseSucces(boolean isActive) {


        }

        @Override
        public void onFragResume() {

            bActive=true;
        }

        @Override
        public void onFragStop() {

            bActive=false;
        }

        public void send()
        {

        }
    }

    public void send()
    {
        new CountDownTimer(5000,1000)
        {
            @Override
            public void onTick(long millisUntilFinished) {

                Log.v("onTick",">>>>>>>");
            }

            @Override
            public void onFinish() {

                Log.v("onFinish",">>>>>>>");

                if(!flagSttoped) {

                    Log.d(">>>","esta activo");
                    if (MR != null) {

                        Log.d(">>>", " es OK");
                        //if(MR.bActive)//si se quiere poner onSucces y onFailedStop
                        MR.onResponseSucces(MR.bActive);

                    } else {
                        Log.e(">>>", " es null");
                    }
                }
                else
                {
                    Log.e(">>>","esta detenido");
                }

            }
        }.start();
    }


    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Log.d("onActivityCreated","||||||||||||||||||||||");
    }

    @Override
    public void onActivityStarted(Activity activity) {


        if(A!=null) {
            if (A.equals(activity)) {
                Log.d("onActivityStarted", "coincide");
                changeFlag(false);
            } else {
                Log.d("onActivityStarted", "no coicnide");
            }
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
        if(A!=null)
        {
            if (A.equals(activity)) {
                Log.d("onActivityResumed", "coincide");
               changeFlag(false);
            } else {
                Log.d("onActivityResumed", "no coicnide");
            }
        }


    }

    @Override public void onActivityPaused(Activity activity) {}

    @Override
    public void onActivityStopped(Activity activity) {
        if(A!=null) {
            if (A.equals(activity)) {

                Log.e("onActivityStopped", "coincide");
                changeFlag(true);
            } else {
                Log.e("onActivityStopped", "no coicnide");
            }
        }

    }

    @Override public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}

    @Override
    public void onActivityDestroyed(Activity activity) {

        changeFlag(false);

    }
}
