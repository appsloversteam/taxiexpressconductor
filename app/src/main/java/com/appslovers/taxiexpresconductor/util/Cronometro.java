package com.appslovers.taxiexpresconductor.util;

import android.app.Activity;
import android.os.CountDownTimer;
import android.util.Log;

import com.appslovers.taxiexpresconductor.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by javierquiroz on 19/07/16.
 */
public class Cronometro {



    Activity A;
    public Cronometro(Activity a)
    {
        A=a;
    }
    public interface ICronometro {
        void onChange(String texto, boolean color);
    }

    SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat HHmmss = new SimpleDateFormat("HH:mm:ss");

    long tiempoavanzado = 0l;

    long undia = 24 * 60 * 60 * 1000;
    long cadasegundo = 1000;
    CountDownTimer CDT = new CountDownTimer(undia, cadasegundo) {
        @Override
        public void onTick(long millisUntilFinished) {
            getDiferenciaTiempo();
        }

        @Override
        public void onFinish() {
        }
    };

    public String TInicio;
    public String TFinal;

    public ICronometro ICRONO;

    public float tiempo_paraemepzar_a_cobrar=0f;
    public void setICrono(ICronometro icrono, String tinicio, String tfinal) {
        this.ICRONO=icrono;

        if(tinicio!=null && !tinicio.contains("1969"))
        {
            this.TInicio = tinicio;

        }
        else {
            Calendar cal=Calendar.getInstance();
            this.TInicio=yyyyMMddHHmmss.format(cal.getTime());
        }

        if(tfinal!=null && !tfinal.contains("1969")) {
            Log.i("asignando","tfinal >>>>"+tfinal);
            this.TFinal = tfinal;
        }
        else {
            Log.i("asignando","tfinal >>>>es null!!!");
            this.TFinal = null;
        }

    }


    public void encender(boolean b) {
        if (b) {
            CDT.start();
        } else {

            CDT.cancel();
            if (TFinal == null) {
                TFinal = yyyyMMddHHmmss.format(new Date());
            }
            getDiferenciaTiempo();

        }
    }

    public void getDiferenciaTiempo() {
        try {
            Date DInicio = yyyyMMddHHmmss.parse(TInicio);

            Date DFinal = null;
            if (TFinal != null) {
                DFinal = yyyyMMddHHmmss.parse(TFinal);
            } else {
                DFinal = new Date();
            }

            tiempoavanzado=Math.abs(DFinal.getTime()-DInicio.getTime());

        } catch (ParseException e) {
            Log.e("parse", ">" + e.getMessage());
        }

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(tiempoavanzado);
        cal.add(Calendar.HOUR_OF_DAY, -1 * (int) TimeUnit.HOURS.convert(TimeZone.getDefault().getRawOffset(), TimeUnit.MILLISECONDS));

        if (ICRONO != null)
            ICRONO.onChange(HHmmss.format(cal.getTime()), (tiempoavanzado > (tiempo_cortesia)));

    }


    public float getCostoEspera() {
        float total = 0f;


        //Log.i("avaz - empe",">"+tiempoavanzado+" - "+tiempo_cortesia);
        //Log.i("X / espera",">"+((int)(tiempoavanzado-tiempo_cortesia))+" / "+tiempo_extra);

        total = (
                ((int) (
                        ((tiempoavanzado - tiempo_cortesia) > 0 ? tiempoavanzado - tiempo_cortesia : 0)
                                /
                                tiempo_extra
                )
                )
                        +
                        ((tiempoavanzado - tiempo_cortesia) >= 1 ? 1 : 0)

        ) * precio_extra;

        //Log.i("Costo espera",">"+total);
        return total;
    }


    /*TaxiExpress*/
    float tiempo_extra=5*60*1000f;//tiempo bloque de espera
    float precio_extra=1.0f;//precio por bloque de espera
    float tiempo_cortesia=5*60*1000f;//tiempo que de tolerancia sin costot




}
