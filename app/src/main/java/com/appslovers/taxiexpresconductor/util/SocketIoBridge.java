package com.appslovers.taxiexpresconductor.util;

import com.appslovers.taxiexpresconductor.data.ISocket;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by javierquiroz on 30/06/16.
 */
public class SocketIoBridge implements ISocket {


    @Override
    public void ReservaPendienteConductor(JsonPack.ReservaPendiente r) {

    }


    @Override
    public void finalizarConTarjeta(String json){

    }

    @Override
    public void refreshCoordinates(double lat, double lng) {

    }

    public static class AuxiliarBridge
    {
        public void onNuevasSolicitudes(int cantidad)
        {

        }

        public void onNodeNuevaReserva(JsonPack.NuevaReserva nr)
        {

        }

    }

    @Override
    public void ReservaProxima(JsonPack.ReservaAsignada s) {



    }

    @Override
    public void ServicioCaducado(JsonPack.ServicioCad ser) {

    }

    public void onGpsLocation(LatLng ll)
    {

    }

    @Override
    public void nuevaSolicitudConductor(JsonPack.NuevaSolicitud ns) {

    }

    @Override
    public void connect(String s) {

    }

    @Override
    public void reconnect(String s) {

    }

    @Override
    public void disconnect(String s) {

    }

    @Override
    public void connect_error(String s) {

    }

    @Override
    public void connect_tiemout(String s) {

    }

    @Override
    public void solicitudListaTaxi(JsonPack.AceptaTaxi ace) {

    }

    @Override
    public void ConductorSeleccionado(JsonPack.RespEstadoCondcutor s) {

    }

    //Danny 11-08-2016
    //Inicio
    @Override
    public void ServicioCancelado(JsonPack.CancelarServicio s) {

    }

    @Override
    public void FinalizarServicioTaxista(JsonPack.RespFinalizarServ s) {

    }

    //Fin


    @Override
    public void contestar(JsonPack.Testnode t) {

    }

    @Override
    public void alertaTaxiWeb(JsonPack.Alerta a) {

    }

    @Override
    public void respUpdateSocketConductor(String s) {

    }

    @Override
    public void respUpdateSocketCliente(String s) {

    }

    @Override
    public void NotificacionReserva(JsonPack.NodeNuevaReserva s) {

    }

    @Override
    public void NotificacionReservaAceptada(JsonPack.NodeNuevaReserva s) {

    }
}
