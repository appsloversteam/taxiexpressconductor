package com.appslovers.taxiexpresconductor.body;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.data.IMyAdapter;
import com.appslovers.taxiexpresconductor.data.IMyViewHolder;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.util.MyAdapter;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.data.Urls;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class FragRecargasHistorial extends Fragment implements IMyViewHolder{



    View ViewRoot;
    private RecyclerView rvLista;
    ArrayList<JsonPack.RecargaHist> recargas;
    LinearLayoutManager llm;
    private  MyAdapter<FragDisponible.SolicitudVh,JsonPack.RecargaHist> mAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot =inflater.inflate(R.layout.fragrecargas_historial,null);

        rvLista=(RecyclerView) ViewRoot.findViewById(R.id.lista);
        //rvLista.setHasFixedSize(true);
        llm=new LinearLayoutManager(getActivity());
        rvLista.setLayoutManager(llm);


        return ViewRoot;
    }

    MyRequest<JsonPack.RespRecargaHistorial> MR=new MyRequest<JsonPack.RespRecargaHistorial>(Urls.ws_historial_recarga, MyRequest.HttpRequestType.GET)
    {

        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.RespRecargaHistorial object) {

            if(object!=null)
            {
                recargas=object.historial;
                if(recargas!=null && recargas.size()>0)
                {

                    mAdapter=new MyAdapter<>(recargas,FragRecargasHistorial.this,R.layout.fragrecargahist_item);
                    rvLista.setAdapter(mAdapter);
                }
            }
        }
    };
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody)getActivity()).mostarTopBarSolicitud();
        ((RootBody)getActivity()).tvTitulo.setText("Recargas");

        MR.putParams("conductor_id",new SPUser(getActivity()).getResponseLogin().conductor_id).send();

    }

    @Override
    public RecyclerView.ViewHolder getInstancia(View v) {
        return new RecargaVh(v);
    }

    public class RecargaVh extends RecyclerView.ViewHolder implements IMyAdapter<RecargaVh>{

        public TextView tvNroOp,tvMonto,tvFecha,tvEntidad,tvTransaccion;


        public RecargaVh(View v) {
            super(v);

            tvNroOp=(TextView)v.findViewById(R.id.tvNroOp);
            tvMonto=(TextView)v.findViewById(R.id.tvMonto);
            tvFecha=(TextView)v.findViewById(R.id.tvFecha);
            tvEntidad=(TextView)v.findViewById(R.id.tvEntidad);
            tvTransaccion=(TextView)v.findViewById(R.id.tvTransaccion);
        }

        @Override
        public void bindView(RecargaVh holder, int position) {

            holder.tvNroOp.setText(recargas.get(position).recarga_num_oper);
            holder.tvMonto.setText(recargas.get(position).recarga_monto);
            holder.tvFecha.setText(recargas.get(position).fecha);
            holder.tvEntidad.setText(recargas.get(position).banco_descripcion);
            holder.tvTransaccion.setText(recargas.get(position).recarga_tipo);

        }

    }

}
