package com.appslovers.taxiexpresconductor.body;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.R;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class Frag_7_ServicioEmergencia extends Fragment {


    RadioGroup Rg;
    View ViewRoot;
    String numero="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot =inflater.inflate(R.layout.fragservicosemergencia,null);

        ViewRoot.findViewById(R.id.btnLlamar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!numero.isEmpty())
                {
                    try
                    {
                        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numero)));
                    }
                    catch (ActivityNotFoundException e)
                    {
                        //Toast.makeText(getActivity(), "No se puede realizar la llamada.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        Rg=(RadioGroup)ViewRoot.findViewById(R.id.rgServicio);
        Rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {



                switch (i)
                {
                    case R.id.rbPolicia:

                        numero="105";
                        break;

                    case R.id.rbCruzRoja:
                        numero="115";
                        break;

                    case R.id.rbDefensaCivil:
                        numero="110";
                        break;

                    case R.id.rbBomberos:
                        numero="116";
                        break;

                    case R.id.rbSamu:
                        numero="106";
                        break;

                }



            }
        });


        return ViewRoot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody)getActivity()).mostarTopBarSolicitud();
        ((RootBody)getActivity()).tvTitulo.setText("Servicios de Emergencias");
    }
}
