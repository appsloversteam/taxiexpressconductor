package com.appslovers.taxiexpresconductor.body;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appslovers.taxiexpresconductor.data.IMyAdapter;
import com.appslovers.taxiexpresconductor.data.IMyViewHolder;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.util.MyAdapter;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.data.Urls;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by javierquiroz on 1/06/16.
 */

public class Frag_1_Reservas extends Fragment implements IMyViewHolder {


    View ViewRoot;
    RecyclerView rvLista;
    ArrayList<JsonPack.Reserva> reservas;
    LinearLayoutManager llm;
    private MyAdapter<FragDisponible.SolicitudVh, JsonPack.Reserva> mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot = inflater.inflate(R.layout.fragreservas, null);

        rvLista = (RecyclerView) ViewRoot.findViewById(R.id.lista);


        llm = new LinearLayoutManager(getActivity());
        rvLista.setLayoutManager(llm);

        return ViewRoot;
    }

    JsonPack.ResponseLogin rl;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((RootBody) getActivity()).mostarTopBarSolicitud();
        ((RootBody) getActivity()).tvTitulo.setText("Reservas Pendientes");

        rl = new SPUser(getActivity()).getResponseLogin();
        MHR.putParams("conductor_id", rl.conductor_id);
        MHR.send();
    }

    MyRequest MHR = new MyRequest(Urls.ws_lista_reservas, MyRequest.HttpRequestType.GET, "ISO-8859-1") {
        @Override
        public void onSuccesForeground(ResponseWork rw) {

            reservas = null;
            try {
                //reservas=new Gson().fromJson(rw.ResponseBody,  new TypeToken<ArrayList<JsonPack.Reserva>>() {}.getType());
                JsonPack.RespReservas rr = new Gson().fromJson(rw.ResponseBody, JsonPack.RespReservas.class);
                if (rr != null) {
                    if (rr.result != null) {
                        reservas = rr.result;


                        mAdapter = new MyAdapter<>(reservas, Frag_1_Reservas.this, R.layout.fragreservas_item);
                        rvLista.setAdapter(mAdapter);
                    }

                }


            } catch (JsonParseException e) {
                Log.e("json ex", ">" + e.getMessage());
            }

        }
    };

    @Override
    public RecyclerView.ViewHolder getInstancia(View v) {
        return new ReservaViewHolder(v);
    }


    public class ReservaViewHolder extends RecyclerView.ViewHolder implements IMyAdapter<ReservaViewHolder> {


        TextView tvFecha, tvHora, tvOrigen, tvDestino, tvCliente, tvTarifa;
        NumberFormat format;

        public ReservaViewHolder(View v) {
            super(v);
            tvFecha = (TextView) v.findViewById(R.id.tvFecha);
            tvHora = (TextView) v.findViewById(R.id.tvHora);
            tvOrigen = (TextView) v.findViewById(R.id.tvOrigen);
            tvDestino = (TextView) v.findViewById(R.id.tvDestino);
            tvCliente = (TextView) v.findViewById(R.id.tvCliente);
            tvTarifa = (TextView) v.findViewById(R.id.tvTarifa);
            format = new DecimalFormat("#0.00");
        }


        @Override
        public void bindView(ReservaViewHolder holder, int position) {

            holder.tvCliente.setText(reservas.get(position).cliente_nombre);
            holder.tvHora.setText(reservas.get(position).Hora);
            holder.tvFecha.setText(reservas.get(position).Fecha);
            holder.tvOrigen.setText(reservas.get(position).dir_origen);
            holder.tvDestino.setText(reservas.get(position).dir_destino);
            try {
                holder.tvTarifa.setText("S/. " + format.format(Float.parseFloat(reservas.get(position).tarifa)));
            } catch (Exception e) {
                holder.tvTarifa.setText("S/. " + reservas.get(position).tarifa);
            }
        }
    }
}
