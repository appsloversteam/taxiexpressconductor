package com.appslovers.taxiexpresconductor.body;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appslovers.taxiexpresconductor.data.IMyAdapter;
import com.appslovers.taxiexpresconductor.data.IMyViewHolder;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.util.MyAdapter;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.data.Urls;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 1/06/16.
 */

public class Frag_3_ServiciosHistorial extends Fragment implements IMyViewHolder{


    View ViewRoot;
    RecyclerView rvLista;
    ArrayList<JsonPack.ServicioHist> Servicios;
    LinearLayoutManager llm;
    MyAdapter<FragDisponible.SolicitudVh,JsonPack.ServicioHist> mAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot=inflater.inflate(R.layout.fragservicioshistorial,null);

        rvLista=(RecyclerView)ViewRoot.findViewById(R.id.lista);

        /*
        Servicios=new ArrayList<>();
        Servicios.add(new JsonPack.ServicioHist());
        Servicios.add(new JsonPack.ServicioHist());
        Servicios.add(new JsonPack.ServicioHist());*/

        llm=new LinearLayoutManager(getActivity());
        rvLista.setLayoutManager(llm);
        //



        return ViewRoot;
    }

    JsonPack.ResponseLogin rl;
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody)getActivity()).mostarTopBarSolicitud();

        ((RootBody)getActivity()).tvTitulo.setText("Historial de Servicios");

        rl=new SPUser(getActivity()).getResponseLogin();
        MHR.putParams("conductor_id", rl.conductor_id);
        MHR.send();
    }

    MyRequest MHR=new MyRequest(Urls.ws_historial_servicios, MyRequest.HttpRequestType.GET,"ISO-8859-1")
    {
        @Override
        public void onSuccesForeground(ResponseWork rw) {

            Servicios=null;
            try {

                JsonPack.ResponseHistorial RR=new Gson().fromJson(rw.ResponseBody, JsonPack.ResponseHistorial.class);
                        //Servicios=new Gson().fromJson(rw.ResponseBody,  new TypeToken<ArrayList<JsonPack.Reserva>>() {}.getType());
                if(RR.result!=null && RR.result.size()>0)
                {
                    Servicios=RR.result;
                    mAdapter=new MyAdapter<>(Servicios,Frag_3_ServiciosHistorial.this,R.layout.fraghistorial_item);
                    rvLista.setAdapter(mAdapter);
                }

            }
            catch (JsonParseException e)
            {
                Log.e("json ex",">"+e.getMessage());
            }

        }
    };


    @Override
    public RecyclerView.ViewHolder getInstancia(View v) {
        return new HistorialViewHolder(v);
    }


    public class HistorialViewHolder  extends RecyclerView.ViewHolder implements IMyAdapter<HistorialViewHolder>
    {


        TextView tvFecha,tvHora,tvOrigen,tvDestino,tvCosto;
        ImageView paymentType;

        public HistorialViewHolder(View v)
        {
            super(v);
            tvFecha=(TextView)v.findViewById(R.id.tvFecha);
            tvHora=(TextView)v.findViewById(R.id.tvHora);
            tvOrigen=(TextView)v.findViewById(R.id.tvOrigen);
            tvDestino=(TextView)v.findViewById(R.id.tvDestino);
            tvCosto=(TextView)v.findViewById(R.id.tvCosto);
            paymentType=(ImageView)v.findViewById(R.id.imageView8);


        }


        @Override
        public void bindView(HistorialViewHolder holder, int position) {


            holder.tvDestino.setText(Servicios.get(position).direccion_destino);
            holder.tvOrigen.setText(Servicios.get(position).direccion_origen);
            holder.tvCosto.setText("S/. "+Servicios.get(position).tarifa);
            holder.tvFecha.setText(Servicios.get(position).fecha);
            holder.tvHora.setText(Servicios.get(position).hora);
            switch(Servicios.get(position).tipo_pago){
                case 1:
                    paymentType.setImageResource(R.drawable.vehiculo_e);
                    break;
                case 2:
                    paymentType.setImageResource(R.drawable.vehiculo_v);
                    break;
                case 3:
                    paymentType.setImageResource(R.drawable.vehiculo_t);
                    break;
            }



        }
    }
}
