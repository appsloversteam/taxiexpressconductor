package com.appslovers.taxiexpresconductor.body;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.data.Urls;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class Frag_5_Beneficios extends Fragment implements View.OnClickListener{


    View ViewRoot;
    TextView tvPuntos;
    SPUser spcon;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot=inflater.inflate(R.layout.fragbeneficios,null);
        ViewRoot.findViewById(R.id.btnCanjear).setOnClickListener(this);
        tvPuntos=(TextView)ViewRoot.findViewById(R.id.tvPuntos);
        spcon=new SPUser(getActivity());
        return ViewRoot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody)getActivity()).mostarTopBarSolicitud();
        ((RootBody)getActivity()).tvTitulo.setText("Beneficios");
        tvPuntos.setText("");
    }


    @Override
    public void onResume() {
        super.onResume();

        MHR.putParams("conductor_id",spcon.getResponseLogin().conductor_id).send();
    }

    MyRequest<JsonPack.RespPuntos> MHR=new MyRequest<JsonPack.RespPuntos>(Urls.ws_obtener_puntos, MyRequest.HttpRequestType.GET)
    {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.RespPuntos object) {
            if(object!=null) {
                pasar=true;
                puntos=object.puntos;
                tvPuntos.setText("" + object.puntos);
            }
        }
    };

    int puntos=0;
    boolean pasar=false;
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnCanjear:
                if(pasar){
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fl_root_inside, FragCanjes.nuevo(puntos), FragCanjes.class.getSimpleName())
                            .addToBackStack("bsdf")
                            .commit();

                }
                break;
        }
    }
}
