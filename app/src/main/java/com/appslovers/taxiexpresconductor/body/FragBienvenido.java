package com.appslovers.taxiexpresconductor.body;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.util.ConfirmDialog;
import com.appslovers.taxiexpresconductor.util.MyAppBackground;
import com.appslovers.taxiexpresconductor.AppBackground;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.util.MyFragment;
import com.appslovers.taxiexpresconductor.util.MyGps;
import com.appslovers.taxiexpresconductor.util.SocketIoBridge;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsStatusCodes;

/**
 * Created by javierquiroz on 30/05/16.
 */
public class FragBienvenido extends MyFragment implements View.OnClickListener{

    TextView tvTipoTaxista;
    Button btnDisponible,btnCancelar;

    boolean isTryCancelConnect=false;
    ProgressBar pbCambiando;
    RatingBar rbPromedio;
    TextView tvSaludo;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.i("bienvenido","-------------->>>>>>>.------------- onCreatedView");
        ViewRoot = inflater.inflate(R.layout.fragbienvenido,null);
        rbPromedio=(RatingBar)ViewRoot.findViewById(R.id.rbPromedio);
        tvSaludo=(TextView)ViewRoot.findViewById(R.id.tvSaludo);
        pbCambiando=(ProgressBar)ViewRoot.findViewById(R.id.pbCambiando);
        btnCancelar=(Button) ViewRoot.findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(this);
        btnDisponible=(Button) ViewRoot.findViewById(R.id.btnDisponible);
        btnDisponible.setOnClickListener(this);
        ViewRoot.findViewById(R.id.imageView4).setOnClickListener(this);
        tvTipoTaxista=(TextView)ViewRoot.findViewById(R.id.tvTipoTaxista);

        return ViewRoot;
    }





    MyAppBackground MAB;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody)getActivity()).flSolicitudes.setVisibility(View.GONE);
        ((RootBody)getActivity()).Emergencia.setBackgroundResource(R.drawable.bg_touch_grisconborde4r);
        ((RootBody)getActivity()).Emergencia.setEnabled(false);

        Log.i("bienvenido", "-------------->>>>>>>.------------- onViewCreated");
        JsonPack.ResponseLogin rl=new SPUser(getActivity()).getResponseLogin();

        if(rl!=null) {
            tvSaludo.setText("Bienvenido, " + rl.nombre);
            rbPromedio.setRating(rl.calificacion);
            tvTipoTaxista.setText("Servicio : " + rl.tipo_nombre);
        }

        String versionName="1.0";
        try
        {
            versionName = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
        }
        catch (PackageManager.NameNotFoundException nnfe)
        {Log.e(nnfe.getClass().getSimpleName(),">"+nnfe.getMessage());}
        ((TextView)ViewRoot.findViewById(R.id.tvVersion)).setText("Version : "+versionName);

        if(getActivity()!=null)
        ((RootBody)getActivity()).tvTitulo.setText("Bienvenido");
        /*if(new SPUser(getActivity()).getResponseLogin().conductor_status==0)
        {
            new DialogRegistroEnProceso().show(getFragmentManager(), "regproc");

        }*/


        MAB=new MyAppBackground(getActivity())
        {   @Override
            public void todo(AppBackground app,int idop,Object o) {
                 app.NodeJsTurnOn(null);
            }
        };


    }


    @Override
    public void onStart() {
        super.onStart();
        MAB.addListner(new SocketIoBridge() {
            @Override
            public void connect(String s) {

                if (isTryCancelConnect) {
                    Log.i("FragBienvenido", " llamando a cancel turnon");
                    MAB.ABG.NodeJsCancelTurnOn();
                } else {
                    //getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fl_root_inside, new FragDisponible(), FragDisponible.class.getSimpleName() + System.currentTimeMillis())
                            .commit();
                }

            }

            @Override
            public void disconnect(String s) {

                isTryCancelConnect=false;
                pbCambiando.setVisibility(View.GONE);
                btnCancelar.setVisibility(View.GONE);
                btnDisponible.setVisibility(View.VISIBLE);
                //Toast.makeText(getContext(),"NODE DESCONECTADO !!!!!",Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onStop() {

        Log.e("MyAppBackground","removeListener Frag Bienvenido");
        MAB.removeListener();

        super.onStop();
    }

    MyGps mgps;
    @Override
    public void onClick(View v) {


        switch (v.getId())
        {

            case R.id.imageView4:

                //int t=7/0;



                break;


            case R.id.btnDisponible:



                mgps=new MyGps(getActivity(), new MyGps.IMyGps() {
                    @Override public void onLocation(Location l) {}
                    @Override public void onOldLocation(Location l) {}
                    @Override public void onEstaApagado() {}
                    @Override public void onFueApagado() {}

                    @Override
                    public void onGpsTurnOn() {

                        btnDisponible.setVisibility(View.GONE);
                        btnCancelar.setText("Conectando...(toque para cancelar)");
                        btnCancelar.setVisibility(View.VISIBLE);
                        pbCambiando.setVisibility(View.VISIBLE);
                        MAB.doit(getActivity(),0);
                    }

                    @Override public void onGpsTurnOff(Status status) {

                        switch (status.getStatusCode()) {

                            case LocationSettingsStatusCodes.SUCCESS:
                                mgps.turnOff();

                                btnDisponible.setVisibility(View.GONE);
                                btnCancelar.setText("Conectando...(toque para cancelar)");
                                btnCancelar.setVisibility(View.VISIBLE);
                                pbCambiando.setVisibility(View.VISIBLE);
                                MAB.doit(getActivity(),0);

                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {status.startResolutionForResult(getActivity(), MyGps.REQUEST_CHECK_SETTINGS_GPS);}
                                catch (IntentSender.SendIntentException e) {Log.e(e.getClass().getSimpleName(), ">" + e.getMessage());}
                             break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                                Toast.makeText(getActivity(),"NO SE PUEDE",Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                });

                ((RootBody)getActivity()).mygps=mgps;
                mgps.turnOn(false);
                /*

                new ConfirmDialog(getActivity().getWindow().getContext(), "Por favor verifique que su GPS esta activado.", getString(R.string.app_empresa), "Continuar","Cancelar") {
                    @Override
                    public void onPositive() {
                        btnDisponible.setVisibility(View.GONE);
                        btnCancelar.setText("Conectando...(toque para cancelar)");
                        btnCancelar.setVisibility(View.VISIBLE);
                        pbCambiando.setVisibility(View.VISIBLE);
                        MAB.doit(getActivity(),0);

                    }


                }.show();*/



                break;

            case R.id.btnCancelar:
                //btnCancelar.setVisibility(View.GONE);
                //btnDisponible.setVisibility(View.VISIBLE);
                btnCancelar.setText("Cancelando...");
                isTryCancelConnect=true;


                break;



        }
    }




}
