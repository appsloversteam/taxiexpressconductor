package com.appslovers.taxiexpresconductor.body;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.util.DatePickerFragment;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.util.TimePickerFragment;
import com.appslovers.taxiexpresconductor.data.Urls;
import com.appslovers.taxiexpresconductor.util.Tools;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class FragRecargasRegistro extends Fragment implements View.OnClickListener {

    View ViewRoot;
    TextView tvSaldo;
    EditText etMonto, etNumeorVoucher;
    Button btnEntidad, btnFecha, btnHora;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot = inflater.inflate(R.layout.fragrecargas_registro, null);

        etNumeorVoucher = (EditText) ViewRoot.findViewById(R.id.etNroOperacion);
        etMonto = (EditText) ViewRoot.findViewById(R.id.etMonto);
        tvSaldo = (TextView) ViewRoot.findViewById(R.id.tvSaldo);
        btnHora = (Button) ViewRoot.findViewById(R.id.btnHora);
        btnHora.setOnClickListener(this);
        btnFecha = (Button) ViewRoot.findViewById(R.id.btnFecha);
        btnFecha.setOnClickListener(this);

        btnEntidad = (Button) ViewRoot.findViewById(R.id.btnEntidad);
        btnEntidad.setOnClickListener(this);

        ViewRoot.findViewById(R.id.btnEnviar).setOnClickListener(this);


        return ViewRoot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody) getActivity()).mostarTopBarSolicitud();
        ((RootBody) getActivity()).tvTitulo.setText("Recargas");

        MR.putParams("conductor_id", new SPUser(getActivity()).getResponseLogin().conductor_id).send();

    }

    MyRequest<JsonPack.RespRecargaHistorial> MR = new MyRequest<JsonPack.RespRecargaHistorial>(Urls.ws_historial_recarga, MyRequest.HttpRequestType.GET) {

        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.RespRecargaHistorial object) {

            if (object != null) {
                //Toast.makeText(getContext(),">"+object.saldo,Toast.LENGTH_SHORT).show();
                if (object.saldo <= 0) {
                    tvSaldo.setBackgroundColor(getResources().getColor(R.color.bgrojo));
                    tvSaldo.setTextColor(Color.WHITE);
                } else {
                    tvSaldo.setBackgroundColor(getResources().getColor(R.color.bgverde));
                    tvSaldo.setTextColor(Color.BLACK);
                }
                tvSaldo.setText("Saldo : S/. " + Tools.formatFloat(object.saldo));
            }
        }
    };

    boolean flag_recargar = true;
    MyRequest MR_RECARGAR = new MyRequest(Urls.ws_recarga_conductor, MyRequest.HttpRequestType.POST) {

        @Override
        public void onSuccesForeground(ResponseWork rw) {

            if (rw.ResponseBody != null) {
                if (rw.ResponseBody.contains("true")) {
                    limpiar();
                    //Toast.makeText(getContext(),"La recarga se aprobará en el plazo determinado.",Toast.LENGTH_SHORT).show();

                } else {
                    //Toast.makeText(getContext(),"Error al recargar",Toast.LENGTH_SHORT).show();
                }
            }
            flag_recargar = true;

        }
    };

    public void limpiar() {
        etMonto.setText("");
        etNumeorVoucher.setText("");
        btnFecha.setText("Ingrese la fecha");
        btnHora.setText("Ingrese la hora");
        btnEntidad.setText("Selecione Entidad");
    }


    public static String[] Entidades = {"BCP", "BBVA", "Scotiabank", "Interbank", "BanBif", "Banco Financiero"};
    DatePickerFragment DPF;
    TimePickerFragment TPF;
    String idsel = "1";
    String fechasel = "";
    String tiemposel = "";

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEnviar:


                if (flag_recargar) {

                    flag_recargar = false;
                    //2016-08-25T20:12:00.000Z

                    Log.i("fecha gen", fechasel + "T" + tiemposel + ".000Z");
                    MR_RECARGAR
                            .putParams("conductor_id", new SPUser(getActivity()).getResponseLogin().conductor_id)
                            .putParams("recarga_monto", etMonto.getText().toString())
                            .putParams("recarga_num_oper", etNumeorVoucher.getText().toString())
                            .putParams("recarga_fecha", fechasel + "T" + tiemposel + ":00.000Z")
                            .putParams("recarga_tipo", idsel)
                            .send();


                }
                break;

            case R.id.btnEntidad:


                new AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                        //new AlertDialog.Builder(getActivity(),android.R.style.Theme_DeviceDefault_Light)
                        .setTitle(getString(R.string.app_empresa))
                        .setItems(Entidades, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                btnEntidad.setText(Entidades[which]);
                                idsel = "" + which + 1;

                            }
                        }).create().show();
                break;


            case R.id.btnFecha:
                if (DPF == null) {
                    DPF = new DatePickerFragment();
                    DPF.IDP = new DatePickerFragment.IDatePicker() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int day) {
                            fechasel = year + "-" + String.format("%02d", (month + 1)) + "-" + String.format("%02d", day);
                            btnFecha.setText(fechasel);

                        }
                    };

                }
                DPF.show(getFragmentManager(), "date");
                break;

            case R.id.btnHora:

                if (TPF == null) {
                    TPF = new TimePickerFragment();
                    TPF.ITP = new TimePickerFragment.ITimePicker() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                            tiemposel = String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute);
                            btnHora.setText(tiemposel);


                        }
                    };

                }

                TPF.show(getFragmentManager(), "time");
                break;
        }
    }
}
