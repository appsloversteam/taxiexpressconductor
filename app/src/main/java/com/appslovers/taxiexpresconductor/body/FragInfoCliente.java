package com.appslovers.taxiexpresconductor.body;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.AppBackground;
import com.appslovers.taxiexpresconductor.AppForeGround;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.RootSplash;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.data.Urls;
import com.appslovers.taxiexpresconductor.dialogs.DialogAlerta;
import com.appslovers.taxiexpresconductor.dialogs.DialogCostosAdicionales;
import com.appslovers.taxiexpresconductor.dialogs.DialogEnviarVale;
import com.appslovers.taxiexpresconductor.dialogs.DialogSms;
import com.appslovers.taxiexpresconductor.util.ConfirmDialog;
import com.appslovers.taxiexpresconductor.util.Constans;
import com.appslovers.taxiexpresconductor.util.Cronometro;
import com.appslovers.taxiexpresconductor.util.MyAppBackground;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.util.SocketIo;
import com.appslovers.taxiexpresconductor.util.SocketIoBridge;
import com.appslovers.taxiexpresconductor.util.Tools;
import com.appslovers.taxiexpresconductor.util.TouchableMapFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;

public class FragInfoCliente extends TouchableMapFragment implements View.OnClickListener {

    View ViewRoot;
    Button btnServicioIni, btnServicioFin, btnAvisarLlegada, btnCancelar, btnSrvicioAdicional;

    Button btnWazeOrigen, btnWazeDestino, btnLlamar, btnSms, btnAlerta;

    TextView tvContador;

    TextView origenTV, destinoTV;
    TextView tvreferenciaorigen, tvdestinoreferencia;


    LinearLayout flInicial;
    TextView tvNombreCli;
    FrameLayout flWazeOri;
    FrameLayout flAlerta;
    FrameLayout flWazeDes;

    LinearLayout llContenedor;

    FloatingActionButton floatingButton;
    /*
    TextView tvNombre;
    TextView tvOrigen, tvDireccionOrigen, tvReferenciaOrigen;
    TextView tvDestino, tvDireccionDestino, tvReferenciaDestino;
    TextView tvTarifa;
    TextView tvTipoPago;

    TextView tvLabelTarifa;*/
    Cronometro CRONO;//=new Cronometro(getActivity());

    LocationManager locationManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        CRONO = new Cronometro(getActivity());
        ViewRoot = inflater.inflate(R.layout.fraginfocliente, null);

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        tvNombreCli = (TextView) ViewRoot.findViewById(R.id.tvLabelnombrecli);

        origenTV = (TextView) ViewRoot.findViewById(R.id.textView33);
        destinoTV = (TextView) ViewRoot.findViewById(R.id.textView32);
        tvreferenciaorigen = (TextView) ViewRoot.findViewById(R.id.tvreferenciaorigen);
        tvdestinoreferencia = (TextView) ViewRoot.findViewById(R.id.tvdestinoreferencia);

        btnSrvicioAdicional = (Button) ViewRoot.findViewById(R.id.btnCostosAdicionales);
        btnSrvicioAdicional.setOnClickListener(this);
        btnServicioFin = (Button) ViewRoot.findViewById(R.id.btnFinalizar);
        btnServicioFin.setOnClickListener(this);
        btnServicioIni = (Button) ViewRoot.findViewById(R.id.btnIniciar);
        btnServicioIni.setOnClickListener(this);
        btnAvisarLlegada = (Button) ViewRoot.findViewById(R.id.btnAvisarLlegada);
        btnAvisarLlegada.setOnClickListener(this);
        btnCancelar = (Button) ViewRoot.findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(this);


        ViewRoot.findViewById(R.id.btnAlerta).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnInfo).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnWazeOri).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnWazeDes).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnLlamar).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnEnviarMensaje).setOnClickListener(this);

        ViewRoot.findViewById(R.id.btnCostosAdicionales).setOnClickListener(this);


        flInicial = (LinearLayout) ViewRoot.findViewById(R.id.flInicial);
        flWazeOri = (FrameLayout) ViewRoot.findViewById(R.id.llWaze);
        flAlerta = (FrameLayout) ViewRoot.findViewById(R.id.flAlerta);
        flWazeDes = (FrameLayout) ViewRoot.findViewById(R.id.flWaze);

        tvContador = (TextView) ViewRoot.findViewById(R.id.tvContador);

        floatingButton = (FloatingActionButton) ViewRoot.findViewById(R.id.floatingActionButton);
        floatingButton.setOnClickListener(this);

        /*
        tvNombre = (TextView) ViewRoot.findViewById(R.id.tvNombre);

        tvOrigen = (TextView) ViewRoot.findViewById(R.id.tvOrigen);
        tvDireccionOrigen = (TextView) ViewRoot.findViewById(R.id.tvDireccionOrigen);
        tvReferenciaOrigen = (TextView) ViewRoot.findViewById(R.id.tvReferenciaOrigen);

        tvDestino = (TextView) ViewRoot.findViewById(R.id.tvDestino);
        tvDireccionDestino = (TextView) ViewRoot.findViewById(R.id.tvDireccionDestino);
        tvReferenciaDestino = (TextView) ViewRoot.findViewById(R.id.tvReferenciaDestino);

        tvTarifa = (TextView) ViewRoot.findViewById(R.id.tvTarifa);
        tvTipoPago = (TextView) ViewRoot.findViewById(R.id.tvTipoPago);

        tvLabelTarifa=(TextView) ViewRoot.findViewById(R.id.tvLabelTarifa);

        if(((AppForeGround)getActivity().getApplication()).flag_tipotarifa==2)
        {
            tvLabelTarifa.setText(getString(R.string.tarifa)+" : ");
        }
        else if(((AppForeGround)getActivity().getApplication()).flag_tipotarifa==1)
        {
            tvLabelTarifa.setText(getString(R.string.tarifa)+" : ");
        }*/

        llContenedor = (LinearLayout) ViewRoot.findViewById(R.id.llMapa);
        llContenedor.addView(super.onCreateView(inflater, container, savedInstanceState), 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        return ViewRoot;
    }

    Cronometro.ICronometro miICron = new Cronometro.ICronometro() {

        @Override
        public void onChange(String texto, boolean color) {
            if (color) {
                tvContador.setTextColor(Color.RED);
            }
            tvContador.setText(texto);
        }

    };

    MyAppBackground MAB;
    GoogleMap MAPA;
    boolean flag_pulsofinalizarser = true;
    boolean flag_pulsoiniciarServicio = true;
    AlertDialog dialogWaitingResponse;


    DialogEnviarVale DEV;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //((RootBody)getActivity()).flSolicitudes.setVisibility(View.GONE);
        ((RootBody) getActivity()).tvTitulo.setText("EN CAMINO");
        //((RootBody)getActivity()).Emergencia.setBackgroundResource(R.drawable.bg_touch_rojo4r);
        //((RootBody)getActivity()).Emergencia.setEnabled(true);

        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                //MAPA.setMyLocationEnabled(false);
                googleMap.getUiSettings().setCompassEnabled(false);
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.getUiSettings().setMapToolbarEnabled(true);
                googleMap.setIndoorEnabled(true);

                MAPA = googleMap;

                LatLng posicionActual = null;

                try {
                    posicionActual = new LatLng(Double.parseDouble(MAB.ABG.getServicioActual().DestinoLat), Double.parseDouble(MAB.ABG.getServicioActual().DestinoLng));
                } catch (Exception e) {
                    e.printStackTrace();
                    posicionActual = null;
                }

                if (posicionActual != null)
                    MAPA.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(posicionActual, AppBackground.ZOOM_GOOGLEMAPS, 0, 0)));

                ViewRoot.findViewById(R.id.progress).setVisibility(View.GONE);
                MAB.doit(getActivity(), 8);
            }
        });

        MAB = new MyAppBackground(getActivity()) {

            @Override
            public void todo(AppBackground app, int idop, Object o) {

                Log.i("todo", "idop>" + idop);
                switch (idop) {
                    case 0:
                        app.EnviarLlegadaRecogo(new SocketIo.CallBack() {
                            @Override
                            public void onCallBack(String s) {
                                ActualizarVistaLuegoDeLlegaste();
                                ABG.getServicioActual().Estado = "2";
                                CRONO.setICrono(miICron, ABG.getServicioActual().FechaRecojo, ABG.getServicioActual().HoraInicio);
                                CRONO.encender(true);
                                if (ABG.getServicioActual().HoraInicio == null)
                                    ABG.getServicioActual().FechaRecojo = CRONO.TInicio;

                            }
                        });
                        break;

                    case 1:
                        app.EnviarIniciarServicio(new SocketIo.CallBack() {
                            @Override
                            public void onCallBack(String s) {
                                flag_pulsoiniciarServicio = false;

                                Log.i("EnviarLlegadaRecogo", ">" + s);
                                CRONO.setICrono(miICron, ABG.getServicioActual().FechaRecojo, ABG.getServicioActual().HoraInicio);
                                CRONO.encender(false);
                                if (ABG.getServicioActual().HoraInicio == null)
                                    ABG.getServicioActual().HoraInicio = CRONO.TFinal;
                                ABG.getServicioActual().Estado = "3";
                                ActualizarVistaLuegodeIniciarServicio();
                                MAB.doit(getActivity(), 8);

                            }
                        });
                        break;

                    case 2:
                        boolean flagfinalizaahora = true;
                        if (MAB.ABG.getServicioActual().TipoPago.equals("2"))//si es tipo vale
                        {
                            guardarVale(new SPUser(getActivity()).getCodigoVale(),app.ServicioActual.IdServicio);
                            if (new SPUser(getActivity()).getCodigoVale() == null) {//si el vale no existe

                                flagfinalizaahora = false;
                            }

                        }else if (MAB.ABG.getServicioActual().TipoPago.equalsIgnoreCase("3"))
                        {
                            flagfinalizaahora=false;
                            final View viewFin = getActivity().getLayoutInflater().inflate(R.layout.waiting_payment_response, null);
                            AlertDialog.Builder builderDialogFinalizar = new AlertDialog.Builder(getActivity());
                            builderDialogFinalizar.setView(viewFin);
                            dialogWaitingResponse = builderDialogFinalizar.show();

                            consultarTarifa();

                        }

                        if (flagfinalizaahora) {

                            final SPUser st = new SPUser(getActivity());
                            JsonPack.CostosAdicionales ca2 = st.getAdicionales();
                            ca2.idServicio = app.getServicioActual().IdServicio;
                            ca2.Espera = CRONO.getCostoEspera();
                            Log.i("costo espera", ">" + ca2.Espera);
                            st.setAdiconales(ca2);
                            CosAdi = ca2;

                            if (MAB.ABG.getServicioActual().TipoPago.equals("2"))//si es tipo vale
                            {
                                guardarVale(new SPUser(getActivity()).getCodigoVale(),app.ServicioActual.IdServicio);
                                return;

                            }


                            MAB.ABG.EnviarFinalizarServicio(CosAdi, new SocketIo.CallBack() {
                                @Override
                                public void onCallBack(String s) {
                                    flag_pulsofinalizarser = false;
                                    if (getActivity() != null) {
                                        //if( ((AppForeGround)getActivity().getApplication()).flag_tipotarifa==2) {
                                        JsonPack.CallbackFinalizar cf = new JsonPack.CallbackFinalizar();
                                        if (s != null) {
                                            try {
                                                cf = new Gson().fromJson(s, JsonPack.CallbackFinalizar.class);
                                            } catch (JsonParseException e) {
                                                Log.e(e.getClass().getSimpleName(), ">" + e.getMessage());
                                            }

                                        }
                                        if (cf != null) {
                                            MAB.ABG.getServicioActual().Tarifa = String.format("%.2f", cf.tarifa);
                                        }

                                        //}

                                        st.setAdiconales(null);

                                        MAB.ABG.getServicioActual().Estado = "" + (MAB.ABG.getServicioActual().getEstado() + 1);

                                        //MOSTRANDO EL RESUMEN DEL SERVIVICIO EXPRESS, COSTO DEL SERVICIO, COSTO ADICIONAL, COSTO DE ESPERA... ETC.
                                        if (getActivity() != null) {
                                            getActivity().getSupportFragmentManager().beginTransaction()
                                                    .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                                                    //.replace(R.id.fl_root_inside, new FragResumen(), FragResumen.class.getSimpleName())
                                                    .commit();

                                            final View viewFin = getActivity().getLayoutInflater().inflate(R.layout.dialog_finalizarserv, null);
                                            AlertDialog.Builder builderDialogFinalizar = new AlertDialog.Builder(getActivity());
                                            builderDialogFinalizar.setView(viewFin);
                                            final AlertDialog dialogFin = builderDialogFinalizar.show();

                                            Float tarifa = Float.parseFloat(Tools.formatFloat(Float.parseFloat(MAB.ABG.getServicioActual().Tarifa)));
                                            Float adicionales = Float.parseFloat(Tools.formatFloat(MAB.ABG.getServicioActual().costosadicionales.Espera + MAB.ABG.getServicioActual().costosadicionales.Paradas + MAB.ABG.getServicioActual().costosadicionales.Parqueos + MAB.ABG.getServicioActual().costosadicionales.Peajes));
                                            Float total = tarifa + adicionales;

                                            ((TextView) viewFin.findViewById(R.id.etTarifa)).setText("S/. " + Tools.formatFloat(tarifa));
                                            ((TextView) viewFin.findViewById(R.id.etAdicionales)).setText("S/. " + Tools.formatFloat(adicionales));
                                            ((TextView) viewFin.findViewById(R.id.etTotal)).setText("S/." + Tools.formatFloat(total));

                                            viewFin.findViewById(R.id.btnAceptar).setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialogFin.dismiss();
                                                }
                                            });

                                        }
                                    }
                                }
                            });


                        } //else {

                          //  if (DEV == null) {
                          //      DEV = new DialogEnviarVale();
                          //      DEV.setTargetFragment(FragInfoCliente.this, 5656);
                          //  }

                          //  DEV.idServicio = app.getServicioActual().IdServicio;
                          //  if (!DEV.isAdded()) {
                          //      DEV.show(getFragmentManager(), "dev");
                          //  }
                       // }


                        break;

                    case 3:
                        if (MAB.ABG.getClienteActual() != null) {
                            tvNombreCli.setText(MAB.ABG.getClienteActual().cliente_nombre);

                            destinoTV.setText(MAB.ABG.getServicioActual().DestinoDireccion);
                            origenTV.setText(MAB.ABG.getServicioActual().OrigenDireccion);

                            if (MAB.ABG.getServicioActual().OrigenReferencia != null) {
                                if (MAB.ABG.getServicioActual().OrigenReferencia.trim().length() > 0)
                                    tvreferenciaorigen.setText(MAB.ABG.getServicioActual().OrigenReferencia);
                            }

                            if (MAB.ABG.getServicioActual().DestinoReferencia != null) {
                                if (MAB.ABG.getServicioActual().DestinoReferencia.trim().length() > 0)
                                    tvdestinoreferencia.setText(MAB.ABG.getServicioActual().DestinoReferencia);
                            }

                            int estado_actual_servicio = MAB.ABG.getServicioActual().getEstado();

                            switch (estado_actual_servicio) {
                                case 2:// ya llego

                                    ActualizarVistaLuegoDeLlegaste();
                                    flag_pulsoiniciarServicio = true;

                                    CRONO.setICrono(miICron, ABG.getServicioActual().FechaRecojo, ABG.getServicioActual().HoraInicio);
                                    CRONO.encender(true);

                                    break;

                                case 3://ya inciio servicio
                                    flag_pulsoiniciarServicio = false;
                                    ActualizarVistaLuegodeIniciarServicio();

                                    CRONO.setICrono(miICron, ABG.getServicioActual().FechaRecojo, ABG.getServicioActual().HoraInicio);
                                    CRONO.getDiferenciaTiempo();


                                    break;
                                case 4:

                                    //Toast.makeText(getActivity()," Estado 4",Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        /*
                        tvNombre.setText(MAB.ABG.getClienteActual().cliente_nombre);

                        tvOrigen.setText(MAB.ABG.getServicioActual().OrigenNombre);
                        tvDireccionOrigen.setText(MAB.ABG.getServicioActual().OrigenDireccion);
                        tvReferenciaOrigen.setText(MAB.ABG.getServicioActual().OrigenReferencia);
                        tvDestino.setText(MAB.ABG.getServicioActual().DestinoNombre);
                        tvDireccionDestino.setText(MAB.ABG.getServicioActual().DestinoDireccion);
                        tvReferenciaDestino.setText(MAB.ABG.getServicioActual().DestinoReferencia);



                        if(MAB.ABG.getServicioActual().Tarifa!=null) {
                            //tvTarifa.setText("S/. " + Tools.formatFloat(Tools.getFloat(MAB.ABG.getServicioActual().Tarifa)));
                            tvTarifa.setText("S/. " +(int)Tools.getFloat(MAB.ABG.getServicioActual().Tarifa));
                        }
                        else
                            tvTarifa.setText("S/. 0.00");

                        switch(MAB.ABG.getServicioActual().TipoPago)
                        {
                            case "1":
                                tvTipoPago.setText("Efectivo");
                                break;

                            case "2":
                                tvTipoPago.setText("Cupón/vale");
                                break;
                        }*/

                            break;
                        } else {
                            //app.EnviarCancelarServicio();
                            //app.NodeJsTurnOff();
                            getActivity().startActivity(new Intent(getActivity(), RootSplash.class));
                            getActivity().finish();

                            break;

                        }


                    case 4:
                        if (android.util.Patterns.PHONE.matcher(MAB.ABG.getClienteActual().cliente_celular).matches()) {
                            //String uri = "tel:" +MAB.ABG.getServicioActual().celularr;
                            //String uri = "tel:" + "945928933";
                            String uri = "tel:" + MAB.ABG.getClienteActual().cliente_celular;
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse(uri));
                            startActivity(intent);

                        } else {
                            //Toast.makeText(getActivity(), "Error en formato de numero ", Toast.LENGTH_SHORT).show();
                        }

                        break;

                    case 5:


                        try
                        {
                            String url = "";
                            int estado=Integer.parseInt(app.getServicioActual().Estado);
                            if (estado==3){
                                url="waze://?ll="+app.getServicioActual().DestinoLat+","+app.getServicioActual().DestinoLng+"&navigate=yes";
                            }else{
                                url="waze://?ll="+app.getServicioActual().OrigenLat+","+app.getServicioActual().OrigenLng+"&navigate=yes";
                            }
                            Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
                            startActivity( intent );
                        }
                        catch ( ActivityNotFoundException ex  )
                        {
                            Intent intent =
                                    new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
                            startActivity(intent);
                        }

                        /*
                        String uri ="";
                        if(MAB.ABG.getServicioActual().getEstado()<3)
                        {

                            uri = "geo:"+ MAB.ABG.getServicioActual().OrigenLat+"," +MAB.ABG.getServicioActual().OrigenLng;
                        }
                        else if(MAB.ABG.getServicioActual().getEstado()==3)
                        {

                            uri = "geo:"+ MAB.ABG.getServicioActual().DestinoLat+"," +MAB.ABG.getServicioActual().DestinoLng;

                        }

                        startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));*/

                        break;

                    case 6:

                        Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                        i.putExtra("address", MAB.ABG.getClienteActual().cliente_celular);
                        //i.putExtra("sms_body",DSMS!=null?DSMS.Mensaje:"");
                        i.setType("vnd.android-dir/mms-sms");

                        startActivity(Intent.createChooser(i, "Escoger app :"));
                        break;

                    case 7:

                        app.getServicioActual().vale = ((String) o);
                        MAB.doit(getActivity(), 2);
                        break;

                    case 8:
                        actualizar_mapa();
                        break;

                    case 9:
                        app.EnviarCancelarServicio();
                        //app.NodeJsTurnOff();
                        getActivity().startActivity(new Intent(getActivity(), RootSplash.class));
                        getActivity().finish();

                        break;
                }
            }
        };

    }

    AppBackground.IFinalizarServicio MyIFinalizarServicio;

    MyRequest<JsonPack.ResponseTrarifa> MRT = new MyRequest<JsonPack.ResponseTrarifa>(Urls.ws_consultar_tarifa, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.ResponseTrarifa object) {

            if (object.status) {
                MAB.ABG.ServicioActual.Tarifa=String.valueOf(object.tarifa);
                SPUser st = new SPUser(getActivity());
                JsonPack.CostosAdicionales ca2 = st.getAdicionales();
                ca2.idServicio = MAB.ABG.getServicioActual().IdServicio;
                ca2.Espera = CRONO.getCostoEspera();
                Log.i("costo espera", ">" + ca2.Espera);
                st.setAdiconales(ca2);
                CosAdi = ca2;

                JsonPack.SolicitudPago solicitudPago=new JsonPack.SolicitudPago();
                solicitudPago.costosAdicionales=CosAdi;
                solicitudPago.clienteId=MAB.ABG.getClienteActual().cliente_id;
                solicitudPago.tarifa=String.valueOf(object.tarifa);

                MAB.ABG.enviarSolicitudPago(new Gson().toJson(solicitudPago));
            }

        }
    };

    ArrayList<LatLng> ruta;
    Polyline polyLin;

    /*private void updateRoute(final LatLng origen, final LatLng destino) {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            PolylineOptions rectLine;


            @Override
            protected Void doInBackground(Void... voids) {
                if (ruta != null) {
                    ruta.clear();
                }
                GMapV2Direction md = new GMapV2Direction();
                Document doc = md.getDocument(origen, destino,
                        GMapV2Direction.MODE_DRIVING);


                ruta = md.getDirection(doc);
                //0,255,191,255
                rectLine = new PolylineOptions().width(3).color(Color.argb(255, 0, 255, 191));
                rectLine.addAll(ruta);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (polyLin == null) {
                    polyLin = MAPA.addPolyline(rectLine);
                } else {
                    polyLin.setPoints(ruta);
                }

                LatLngBounds.Builder builder = new LatLngBounds.Builder();

                builder.include(origen);//AGREGANDO EL INICIO
                for (int i = 0; i < ruta.size(); i++) {
                    builder.include(ruta.get(i));
                }
                builder.include(destino);//AGREGANDO EL FIN DE LA RUTA

                LatLngBounds bounds = builder.build();
                int padding = 50; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                MAPA.moveCamera(cu);
            }
        };

        task.execute();
    }*/

    Marker MConductor;
    Marker MCliente;
    Marker MEnCamino;

    LatLng ConPos;

    LatLng CliPos;


    public void ejecutarFinServicioPagoTarjeta(){
        SPUser st = new SPUser(getActivity());
        JsonPack.CostosAdicionales ca2 = st.getAdicionales();
        ca2.idServicio = MAB.ABG.getServicioActual().IdServicio;
        ca2.Espera = CRONO.getCostoEspera();
        Log.i("costo espera", ">" + ca2.Espera);
        st.setAdiconales(ca2);
        CosAdi = ca2;

        MAB.ABG.EnviarFinalizarServicio(CosAdi, new SocketIo.CallBack() {
            @Override
            public void onCallBack(String s) {
                flag_pulsofinalizarser = false;
                if (getActivity() != null) {
                    //if( ((AppForeGround)getActivity().getApplication()).flag_tipotarifa==2) {
                    JsonPack.CallbackFinalizar cf = new JsonPack.CallbackFinalizar();
                    if (s != null) {
                        try {
                            cf = new Gson().fromJson(s, JsonPack.CallbackFinalizar.class);
                        } catch (JsonParseException e) {
                            Log.e(e.getClass().getSimpleName(), ">" + e.getMessage());
                        }

                    }
                    if (cf != null) {
                        MAB.ABG.getServicioActual().Tarifa = String.format("%.2f", cf.tarifa);
                    }

                    //}

                    MAB.ABG.getServicioActual().Estado = "" + (MAB.ABG.getServicioActual().getEstado() + 1);

                    //MOSTRANDO EL RESUMEN DEL SERVIVICIO EXPRESS, COSTO DEL SERVICIO, COSTO ADICIONAL, COSTO DE ESPERA... ETC.
                    if (getActivity() != null) {
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                                //.replace(R.id.fl_root_inside, new FragResumen(), FragResumen.class.getSimpleName())
                                .commit();

                        final View viewFin = getActivity().getLayoutInflater().inflate(R.layout.dialog_finalizarserv, null);
                        AlertDialog.Builder builderDialogFinalizar = new AlertDialog.Builder(getActivity());
                        builderDialogFinalizar.setView(viewFin);
                        final AlertDialog dialogFin = builderDialogFinalizar.show();

                        Float tarifa = Float.parseFloat(Tools.formatFloat(Float.parseFloat(MAB.ABG.getServicioActual().Tarifa)));
                        Float adicionales = Float.parseFloat(Tools.formatFloat(MAB.ABG.getServicioActual().costosadicionales.Espera + MAB.ABG.getServicioActual().costosadicionales.Paradas + MAB.ABG.getServicioActual().costosadicionales.Parqueos + MAB.ABG.getServicioActual().costosadicionales.Peajes));
                        Float total = tarifa + adicionales;

                        ((TextView) viewFin.findViewById(R.id.etTarifa)).setText("S/. " + Tools.formatFloat(tarifa));
                        ((TextView) viewFin.findViewById(R.id.etAdicionales)).setText("S/. " + Tools.formatFloat(adicionales));
                        ((TextView) viewFin.findViewById(R.id.etTotal)).setText("S/." + Tools.formatFloat(total));

                        viewFin.findViewById(R.id.btnAceptar).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogFin.dismiss();
                            }
                        });

                    }
                }
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();

        MAB.doit(getActivity(), 3);


        MAB.addListner(new SocketIoBridge() {
            @Override
            public void ServicioCancelado(JsonPack.CancelarServicio s) {

                Log.d("FragInfoCliente", "ServicioCancelado>idser>" + s.ServicioId);

                if (clienteCancela) {
                    CRONO.setICrono(miICron, MAB.ABG.getServicioActual().FechaRecojo, MAB.ABG.getServicioActual().HoraInicio);
                    if (CRONO != null)
                        CRONO.encender(false);

                    new ConfirmDialog(getActivity(), "Se canceló el servicio.", getString(R.string.app_empresa), "Aceptar", "") {

                        @Override
                        public void onPositive() {

                            getActivity().startActivity(new Intent(getActivity(), RootSplash.class));
                            getActivity().finish();
                        }

                    }.show();
                }

            }

            @Override
            public void finalizarConTarjeta(String json){
                Log.e("FragInfoCliente", "finalizarConTarjeta:"+json);

                JsonPack.VisaNetResponse response=new Gson().fromJson(json, JsonPack.VisaNetResponse.class);

                if(response.info.data.CODACCION.equalsIgnoreCase("000")){
                    dialogWaitingResponse.dismiss();
                    ejecutarFinServicioPagoTarjeta();
                }else{
                    dialogWaitingResponse.findViewById(R.id.button3).setVisibility(View.VISIBLE);
                    ((TextView)dialogWaitingResponse.findViewById(R.id.textView37)).setText(R.string.error_tarjeta);
                    dialogWaitingResponse.findViewById(R.id.progressBar2).setVisibility(View.GONE);
                    dialogWaitingResponse.findViewById(R.id.button3).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ejecutarFinServicioPagoTarjeta();
                            dialogWaitingResponse.dismiss();
                        }
                    });

                }


            }

            @Override
            public void refreshCoordinates(double lat, double lng) {
                ConPos = new LatLng(lat, lng);
                CliPos = new LatLng(Tools.getDouble(MAB.ABG.getServicioActual().OrigenLat), Tools.getDouble(MAB.ABG.getServicioActual().OrigenLng));
                //updateDriverMarker(lat, lng);

                ConPos = new LatLng(lat, lng);

                actualizar_mapa();
            }
        });
    }

    @Override
    public void onStop() {
        //checkContador("", false);
        if (CRONO != null) {
            if (MAB.ABG.getServicioActual() != null) {
                CRONO.setICrono(miICron, MAB.ABG.getServicioActual().FechaRecojo, MAB.ABG.getServicioActual().HoraInicio);
                CRONO.encender(false);
            }
        }
        MAB.removeListener();//Danny
        super.onStop();
    }

    boolean clienteCancela = true;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.floatingActionButton: {
                ViewRoot.findViewById(R.id.layoutOpciones).setVisibility(
                        ViewRoot.findViewById(R.id.layoutOpciones).getVisibility() == View.GONE ?
                                View.VISIBLE :
                                View.GONE
                );
                break;
            }

            case R.id.btnAlerta:
                DialogAlerta da = new DialogAlerta();
                da.setTargetFragment(getActivity().getSupportFragmentManager().findFragmentById(R.id.fl_root_inside), 145);
                da.show(getActivity().getSupportFragmentManager(), "alert");

                break;
            /*case R.id.btnInfo:

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fl_root_inside,new FragInformacionServicio(), FragInformacionServicio.class.getSimpleName())
                        .addToBackStack(FragInformacionServicio.class.getSimpleName())
                        .commit();

                break;*/

            case R.id.btnCancelar:
                final LayoutInflater inflater = getLayoutInflater(getArguments());
                final View dialoglayout = inflater.inflate(R.layout.dialog_confirmarcancelarservicio, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(dialoglayout);
                final AlertDialog dialog = builder.show();
                Button btnSi = (Button) dialoglayout.findViewById(R.id.btnConfirmar);
                Button btnNo = (Button) dialoglayout.findViewById(R.id.btnCancelar);
                btnSi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        clienteCancela = false;
                        MAB.doit(getActivity(), 9);
                    }
                });
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
                break;

            case R.id.btnCostosAdicionales:
                DialogCostosAdicionales md3 = new DialogCostosAdicionales();
                md3.costo_espera = CRONO.getCostoEspera();
                md3.setTargetFragment(FragInfoCliente.this, 111);
                md3.show(getFragmentManager(), "cost");
                break;
            case R.id.btnFinalizar:

                if (flag_pulsofinalizarser) {
                    final LayoutInflater inflaterfinalizar = getLayoutInflater(getArguments());

                    final View dialoglayoutfinalizar = inflaterfinalizar.inflate(R.layout.dialog_confirmarfinalizarservicio, null);
                    AlertDialog.Builder builderfinalizar = new AlertDialog.Builder(getActivity());
                    builderfinalizar.setView(dialoglayoutfinalizar);
                    final AlertDialog dialogfinalizar = builderfinalizar.show();
                    Button btnSifinalizar = (Button) dialoglayoutfinalizar.findViewById(R.id.btnConfirmar);
                    Button btnNofinalizar = (Button) dialoglayoutfinalizar.findViewById(R.id.btnCancelar);
                    btnSifinalizar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            //FINALIZANDO SERVICIO
                            MAB.doit(getActivity(), 2);
                            dialogfinalizar.dismiss();
                        }
                    });

                    dialogfinalizar.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            dialogfinalizar.dismiss();
                        }
                    });

                    btnNofinalizar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogfinalizar.dismiss();
                        }
                    });

                    dialogfinalizar.show();
                }

                break;
            case R.id.btnIniciar:

                if (flag_pulsoiniciarServicio) {
                    // //Toast.makeText(getContext(),"flag en true",Toast.LENGTH_SHORT).show();
                    /*new ConfirmDialog(getActivity().getWindow().getContext(), "¿Seguro de inciar el servicio?", " Taxi Express", "Iniciar") {
                        @Override
                        public void onPositive() {
                            flag_pulsoiniciarServicio=false;
                            MAB.doit(getActivity(), 1);
                            dismisss();
                        }
                    }.show();*/

                    MAB.doit(getActivity(), 1);

                }

                break;
            case R.id.btnAvisarLlegada:
                MAB.doit(getActivity(), 0);

                /*
                //DialogAlerta md =new DialogAlerta();
                DialogEnviarNotificacion md = new DialogEnviarNotificacion();
                //DialogRegistroEnProceso md =new DialogRegistroEnProceso();

                md.setTargetFragment(FragInfoCliente.this, 100);
                md.show(getFragmentManager(), "terminos");*/

                break;
            case R.id.btnLlamar:

                MAB.doit(getActivity(), 4);


                break;

            case R.id.btnWazeOri:

                /*
                String uri ="";
                uri = "geo:"+PosDestino.latitude+","+PosDestino.longitude;
                startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));*/

                MAB.doit(getActivity(), 5);
                break;

            case R.id.btnWazeDes:

                MAB.doit(getActivity(), 5);
                break;

            case R.id.btnEnviarMensaje:

                //DialogAlerta md =new DialogAlerta();
                if (DSMS == null)
                    DSMS = new DialogSms();

                //DialogRegistroEnProceso md =new DialogRegistroEnProceso();
                DSMS.setTargetFragment(FragInfoCliente.this, 112);
                DSMS.show(getFragmentManager(), "sms");
                break;

        }
    }

    private void actualizar_mapa() {
        if (MAPA == null) {
            return;
        }

        int estado_actual_servicio = MAB.ABG.getServicioActual().getEstado();

        switch (estado_actual_servicio) {
            case Constans.SERVICIO_SOLICITUD://SOLO SE MUESTRA LA UBICACION DEL TAXISTA
                if (MEnCamino != null) {
                    MEnCamino.remove();
                    MEnCamino = null;
                }

                if (MCliente != null) {
                    MCliente.remove();
                    MCliente = null;
                }

                if (MConductor != null) {
                    MConductor.remove();
                }

                if (ConPos == null)
                    return;

                MConductor = MAPA.addMarker
                        (new MarkerOptions()
                                .position(ConPos)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_conductor))
                                .title(getActivity().getResources().getString(R.string.usted_esta_aqui))
                        );

                break;
            case Constans.SERVICIO_YENDO_RECOJER://SE MUESTRA LA UBICACION DEL TAXISTA Y DEL PASAJERO
            case Constans.SERVICIO_EN_ESPERA:
                if (MEnCamino != null) {
                    MEnCamino.remove();
                    MEnCamino = null;
                }

                if (MConductor != null) {
                    MConductor.remove();
                    MConductor = null;
                }

                if (ConPos != null && CliPos != null) {
                    MCliente = MAPA.addMarker
                            (new MarkerOptions()
                                    .position(CliPos)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cliente))
                                    .title(getActivity().getResources().getString(R.string.cliente))
                            );

                    MConductor = MAPA.addMarker
                            (new MarkerOptions()
                                    .position(ConPos)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_conductor))
                                    .title(getActivity().getResources().getString(R.string.usted_esta_aqui))
                            );

                    LatLngBounds.Builder builder = new LatLngBounds.Builder();

                    builder.include(CliPos);//AGREGANDO EL INICIO
                    builder.include(ConPos);//AGREGANDO EL FIN DE LA RUTA

                    LatLngBounds bounds = builder.build();
                    int padding = 50; // offset from edges of the map in pixels
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                    MAPA.moveCamera(cu);

                    //updateRoute(ConPos, CliPos);//LatLng Origen, LatLng Destino
                }

                break;

            case Constans.SERVICIO_INICIADO:
            case Constans.SERVICIO_FINALIZADO:
            case Constans.SERVICIO_CANCELADO:
                if (MAPA != null) {
                    MAPA.clear();
                }


                if (MConductor != null) {
                    MConductor.remove();
                    MConductor = null;
                }

                if (MCliente != null) {
                    MCliente.remove();
                    MCliente = null;
                }

                if (MEnCamino != null) {
                    MEnCamino.remove();
                    MEnCamino = null;
                }

                if (ConPos != null) {
                    MEnCamino = MAPA.addMarker
                            (new MarkerOptions()
                                    .position(ConPos)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cliente_conductor))
                                    .title(getActivity().getResources().getString(R.string.en_camino))
                            );

                    MAPA.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(ConPos, AppBackground.ZOOM_GOOGLEMAPS, 0, 0)));
                    //LatLng destinoLatLng = new LatLng(Double.parseDouble(MAB.ABG.getServicioActual().DestinoLat), Double.parseDouble(MAB.ABG.getServicioActual().DestinoLng));
                    //updateRoute(CliPos, destinoLatLng); //LatLng Origen, LatLng Destino
                }
                break;
        }
    }


    DialogSms DSMS;

    JsonPack.CostosAdicionales CosAdi = new JsonPack.CostosAdicionales();

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        Log.i("onActivityResult", "req : " + requestCode + ", res : " + resultCode);
        switch (requestCode) {
            case 100:

                if (resultCode == 200) {
                    //jqg descomentar

                    MAB.doit(getActivity(), 0);
                }

                break;

            case 111:

                if (resultCode == 200) {
                    CosAdi = (JsonPack.CostosAdicionales) data.getSerializableExtra("adicionales");
                }

                break;

            case 112:

                MAB.doit(getActivity(), 6);

                break;

            case 5656:

                if (resultCode == 200) {
                    MAB.doit(getActivity(), 7, data.getStringExtra("codigo"));
                }
                break;
        }
    }

    private void ActualizarVistaLuegoDeLlegaste() {
        ((RootBody) getActivity()).tvTitulo.setText("ESPERANDO CLIENTE");
        //flWazeOri.setVisibility(View.GONE);
        tvContador.setVisibility(View.VISIBLE);
        btnAvisarLlegada.setVisibility(View.GONE);
        btnServicioIni.setVisibility(View.VISIBLE);
    }

    private void ActualizarVistaLuegodeIniciarServicio() {

        ((RootBody) getActivity()).tvTitulo.setText("SERVICIO EN PROCESO");
        tvContador.setVisibility(View.GONE);
        flWazeDes.setVisibility(View.VISIBLE);
        flAlerta.setVisibility(View.VISIBLE);
        ViewRoot.findViewById(R.id.layout_contact_options).setVisibility(View.GONE);

        btnServicioIni.setVisibility(View.GONE);
        btnAvisarLlegada.setVisibility(View.GONE);
        btnCancelar.setVisibility(View.GONE);
        btnServicioFin.setVisibility(View.VISIBLE);
        btnSrvicioAdicional.setVisibility(View.VISIBLE);
    }

    boolean valeSaved=false;

    MyRequest<JsonPack.ResponseGeneric> saveValeRequest=new MyRequest<JsonPack.ResponseGeneric>(Urls.save_vale, MyRequest.HttpRequestType.POST) {

        @Override
        public void onParseSuccesForeground(ResponseWork rw, JsonPack.ResponseGeneric object) {

            object=new Gson().fromJson(rw.ResponseBody, JsonPack.ResponseGeneric.class);
            valeSaved=object.status;
            if(valeSaved){
                MAB.ABG.EnviarFinalizarServicio(CosAdi, new SocketIo.CallBack() {
                    @Override
                    public void onCallBack(String s) {
                        flag_pulsofinalizarser = false;
                        if (getActivity() != null) {
                            //if( ((AppForeGround)getActivity().getApplication()).flag_tipotarifa==2) {
                            JsonPack.CallbackFinalizar cf = new JsonPack.CallbackFinalizar();
                            if (s != null) {
                                try {
                                    cf = new Gson().fromJson(s, JsonPack.CallbackFinalizar.class);
                                } catch (JsonParseException e) {
                                    Log.e(e.getClass().getSimpleName(), ">" + e.getMessage());
                                }

                            }
                            if (cf != null) {
                                MAB.ABG.getServicioActual().Tarifa = String.format("%.2f", cf.tarifa);
                            }

                            //}

                            MAB.ABG.getServicioActual().Estado = "" + (MAB.ABG.getServicioActual().getEstado() + 1);

                            //MOSTRANDO EL RESUMEN DEL SERVIVICIO EXPRESS, COSTO DEL SERVICIO, COSTO ADICIONAL, COSTO DE ESPERA... ETC.
                            if (getActivity() != null) {
                                getActivity().getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                                        //.replace(R.id.fl_root_inside, new FragResumen(), FragResumen.class.getSimpleName())
                                        .commit();

                                final View viewFin = getActivity().getLayoutInflater().inflate(R.layout.dialog_finalizarserv, null);
                                AlertDialog.Builder builderDialogFinalizar = new AlertDialog.Builder(getActivity());
                                builderDialogFinalizar.setView(viewFin);
                                final AlertDialog dialogFin = builderDialogFinalizar.show();

                                Float tarifa = Float.parseFloat(Tools.formatFloat(Float.parseFloat(MAB.ABG.getServicioActual().Tarifa)));
                                Float adicionales = Float.parseFloat(Tools.formatFloat(MAB.ABG.getServicioActual().costosadicionales.Espera + MAB.ABG.getServicioActual().costosadicionales.Paradas + MAB.ABG.getServicioActual().costosadicionales.Parqueos + MAB.ABG.getServicioActual().costosadicionales.Peajes));
                                Float total = tarifa + adicionales;

                                ((TextView) viewFin.findViewById(R.id.etTarifa)).setText("S/. " + Tools.formatFloat(tarifa));
                                ((TextView) viewFin.findViewById(R.id.etAdicionales)).setText("S/. " + Tools.formatFloat(adicionales));
                                ((TextView) viewFin.findViewById(R.id.etTotal)).setText("S/." + Tools.formatFloat(total));
                                TextView notaVale=(TextView) viewFin.findViewById(R.id.textView39);
                                if (cf.TipoPago.equalsIgnoreCase("Vale")){
                                    notaVale.setVisibility(View.VISIBLE);
                                    notaVale.setText("* La tarifa del servicio fue de S/. "+ cf.tarifaServicio + " pero el cliente ha cancelado con un vale de descuento electrónico.");
                                }

                                viewFin.findViewById(R.id.btnAceptar).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogFin.dismiss();
                                    }
                                });

                            }
                        }
                    }
                });
            }
        }

    };

    public void guardarVale(String vale,String idServicio)
    {
        saveValeRequest
                .putParams("vale",vale)
                .putParams("idservicio",idServicio)
                .send();
    }

    String idZonaDestino;

    getIdDestino getIdDes;

    private class getIdDestino extends AsyncTask<Void, Void, Void> {
        private LatLng point;
        private ArrayList<JsonPack.Zona> ZZ;

        public getIdDestino(LatLng point, ArrayList<JsonPack.Zona> ZZ) {
            this.point = point;
            this.ZZ = ZZ;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Boolean hasId = false;

            for (JsonPack.Zona zona : ZZ) {
                ArrayList<LatLng> LatLngs = new ArrayList<>();
                ArrayList<JsonPack.Coordenada> coordenadas = zona.coordenadas;

                if (coordenadas.size() > 0) {
                    for (JsonPack.Coordenada coord : coordenadas) {
                        LatLngs.add(new LatLng(coord.latitud, coord.longitud));
                    }

                    if (LatLngs.size() > 0) {
                        hasId = new JsonPack.Coordenada().isInsidePolygon(LatLngs, point);

                        if (hasId) {
                            idZonaDestino = zona.id;
                            Log.e("IdDestino", "" + idZonaDestino + ", Zona: " + zona.nombre.toUpperCase());
                            break;
                        }
                    }
                }
                if (hasId) {
                    break;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            SPUser Sesion=new SPUser(getActivity());
            if (MAB.ABG.getServicioActual().OrigenId != null && idZonaDestino != null) {
                if(MAB.ABG.getServicioActual().OrigenId.trim() != "" && idZonaDestino.trim() != null){
                    MRT
                            .putParams("tiposervicio_id", "" + Sesion.getResponseLogin().tipo_id)
                            .putParams("empresa_id", "0")
                            .putParams("origen_id", "" + MAB.ABG.getServicioActual().OrigenId)
                            .putParams("destino_id", "" + idZonaDestino)
                            .send();
                }
            }
        }
    }


    public String consultarTarifa(){


        if (getIdDes != null) {
            if (getIdDes.getStatus() == AsyncTask.Status.RUNNING) {
                getIdDes.cancel(true);
            }
        }

        getIdDes = new getIdDestino(new LatLng(MAB.ABG.lastLatitude, MAB.ABG.lastLongitude), ((AppForeGround) MAB.ABG.getApplication()).getDb().getZonas());
        getIdDes.execute();



        return null;
    }


    //public static LatLng conductorPosicion, clientePosicion;

    /*public void actualizarPosicion(double lat, double lng)
    {

        System.out.println("ENTERING UPDATE POSITION!!!");
        LatLng conductor=new LatLng(lat, lng);
        if(MEnCamino!=null)
        {

                MEnCamino.setPosition(conductor);
                MAPA.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(conductor, getResources().getDimension(R.dimen.zoom_google_maps), 0, 0)));


        }
        else
        {
            if(conductor!=null)
                MConductor.setPosition(conductor);

                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(conductor);
                MAPA.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics())));

        }


    }*/
}
