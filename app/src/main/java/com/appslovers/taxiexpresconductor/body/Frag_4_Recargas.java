package com.appslovers.taxiexpresconductor.body;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appslovers.taxiexpresconductor.R;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class Frag_4_Recargas extends Fragment {



    View ViewRoot;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot =inflater.inflate(R.layout.fragrecargas,null);

        //PagerTabStrip pagerTabStrip = (PagerTabStrip) ViewRoot.findViewById(R.id.pager_tab_strip);
        //pagerTabStrip.setDrawFullUnderline(true);
        //pagerTabStrip.setTabIndicatorColor(getResources().getColor(R.color.colortexto));

        return ViewRoot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody)getActivity()).mostarTopBarSolicitud();
        ((RootBody)getActivity()).tvTitulo.setText("Recargas");




        ViewPager viewPager = (ViewPager) ViewRoot.findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(3);
        ArrayList<Fragment> fragments=new ArrayList<>();

        fragments.add(new FragRecargasHistorial());
        fragments.add(new FragRecargasRegistro());
        fragments.add(new FragRecargasMovimientos());


        viewPager.setAdapter(new ScreenSlidePagerAdapter(getChildFragmentManager(),fragments));
        viewPager.setCurrentItem(1);
    }

    public static class ScreenSlidePagerAdapter extends FragmentPagerAdapter//FragmentStatePagerAdapter3
    {

        ArrayList<Fragment> fragments;

        public ScreenSlidePagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            if(position==0)
            {
                return "Historial";
            }
            else if(position==1)
            {
                return "Registro";
            }
            else if(position==2)
            {
                return "Movimientos";
            }else
            {
                return "Pagina "+position ;
            }


        }


        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
