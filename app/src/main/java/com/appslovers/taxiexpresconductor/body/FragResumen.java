package com.appslovers.taxiexpresconductor.body;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.appslovers.taxiexpresconductor.AppForeGround;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.data.Urls;
import com.appslovers.taxiexpresconductor.util.ConfirmDialog;
import com.appslovers.taxiexpresconductor.util.MyAppBackground;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.AppBackground;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.util.Tools;

/**
 * Created by javierquiroz on 31/05/16.
 */
public class FragResumen extends Fragment implements View.OnClickListener{

    TextView tvTarifa;
    TextView tvAdicionales;
    TextView tvTotal;
    TextView tvLabelVale;
    //EditText etVale;
    TextView tvVale;
    View ViewRoot;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot=inflater.inflate(R.layout.fragresumen,null);

        tvTotal=(TextView)ViewRoot.findViewById(R.id.tvTotal);
        tvTarifa=(TextView)ViewRoot.findViewById(R.id.tvTarifa);
        tvAdicionales=(TextView)ViewRoot.findViewById(R.id.tvAdicionales);
        tvLabelVale=(TextView)ViewRoot.findViewById(R.id.tvLabelVale);
        tvVale=(TextView) ViewRoot.findViewById(R.id.tvVale);

        ViewRoot.findViewById(R.id.btnAprobacion).setOnClickListener(this);

        return ViewRoot;

    }

    MyAppBackground MAB;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody)getActivity()).tvTitulo.setText("Resumen de servicio");

        MAB=new MyAppBackground() {

            @Override
            public void todo(AppBackground app, int idop, Object obj) {

                switch (idop)
                {
                    case 0:

                        if(MAB.ABG.getServicioActual().TipoPago.equals("2"))
                        {
                            tvLabelVale.setVisibility(View.VISIBLE);
                            tvVale.setVisibility(View.VISIBLE);
                            tvVale.setText(app.getServicioActual().vale);

                        }

                        tvTarifa.setText("S/. "+ Tools.formatFloat(Tools.getFloat(app.getServicioActual().Tarifa )));
                        tvAdicionales.setText("S/. "+Tools.formatFloat( app.getServicioActual().costosadicionales.getTotal()));
                        tvTotal.setText("S/. "+ Tools.formatFloat((Tools.getFloat(app.getServicioActual().Tarifa )+ app.getServicioActual().costosadicionales.getTotal())));

                    break;

                    case 1:
                        //((AppForeGround)getActivity().getApplication()).guardarVale(etVale.getText().toString(),MAB.ABG.getServicioActual().IdServicio);



                        break;

                }
            }


        };
        MAB.doit(getActivity(),0);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnAprobacion:

                /*
                if(etVale.getVisibility()==View.VISIBLE)
                {
                    if(etVale.getText().toString().length()>0)
                    {

                        //MAB.doit(getActivity(),1);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                                .addToBackStack("vb")
                                .commit();
                    }
                    else
                    {

                        new ConfirmDialog(getActivity().getWindow().getContext(), "¿No ha escrito el vale, continuar de todos modos?", getString(R.string.app_empresa), "Continuar") {
                            @Override
                            public void onPositive() {

                                getActivity().getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                                        .addToBackStack("vb")
                                        .commit();
                            }
                        }.show();

                    }
                }
                else
                {
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                            .addToBackStack("vb")
                            .commit();
                }*/

                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                        .addToBackStack("vb")
                        .commit();



                break;
        }
    }
}
