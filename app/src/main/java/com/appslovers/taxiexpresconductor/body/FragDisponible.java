package com.appslovers.taxiexpresconductor.body;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.util.MapStateListener;
import com.appslovers.taxiexpresconductor.util.MyAppBackground;
import com.appslovers.taxiexpresconductor.AppBackground;
import com.appslovers.taxiexpresconductor.util.ConfirmDialog;
import com.appslovers.taxiexpresconductor.data.IMyAdapter;
import com.appslovers.taxiexpresconductor.data.IMyViewHolder;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.util.MyAdapter;
import com.appslovers.taxiexpresconductor.util.MyGps;
import com.appslovers.taxiexpresconductor.util.SocketIoBridge;
import com.appslovers.taxiexpresconductor.util.Tools;
import com.appslovers.taxiexpresconductor.util.TouchableMapFragment;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 30/05/16.
 */
public class FragDisponible extends TouchableMapFragment implements IMyViewHolder, View.OnClickListener {

    View ViewRoot;
    RecyclerView rvLista;
    ArrayList<JsonPack.NuevaSolicitud> solicitudes;
    LinearLayoutManager llm;
    ImageView ivImagen;
    MyAdapter<FragDisponible.SolicitudVh, JsonPack.NuevaSolicitud> mAdapter;
    LinearLayout contenedor;
    Marker MTaxista;
    MapStateListener MSL;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot = inflater.inflate(R.layout.fragdisponible, null);

        ViewRoot.findViewById(R.id.btnSalir).setOnClickListener(this);
        ivImagen = (ImageView) ViewRoot.findViewById(R.id.ivImagen);

        rvLista = (RecyclerView) ViewRoot.findViewById(R.id.lista);
        rvLista.setHasFixedSize(true);
        rvLista.setVisibility(View.GONE);
        llm = new LinearLayoutManager(getActivity());
        rvLista.setLayoutManager(llm);

        solicitudes = new ArrayList<>();
        mAdapter = new MyAdapter<>(solicitudes, this, R.layout.fragdisponible_item);
        rvLista.setAdapter(mAdapter);

        contenedor = (LinearLayout) ViewRoot.findViewById(R.id.contenedor);
        contenedor.addView(super.onCreateView(inflater, container, savedInstanceState), 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {

                Log.d("onSingleTapUp", ">");
                ((RootBody) getActivity()).GPS.turnOn(true);
                return true;
            }


            @Override
            public boolean onDoubleTap(MotionEvent e) {
                /*float zoom = FSOL.MAPA.getCameraPosition().zoom;
                if (flagMyZoom) {
                    if (zoom < FSOL.MAPA.getMaxZoomLevel()) {
                        zoom += 0.5;
                        FSOL.MAPA.moveCamera(CameraUpdateFactory.zoomTo(zoom));
                    }
                } else {
                    if (zoom > FSOL.MAPA.getMinZoomLevel()) {
                        zoom -= 0.5;
                        FSOL.MAPA.moveCamera(CameraUpdateFactory.zoomTo(zoom));
                    }
                }*/
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                flagMyZoom = !flagMyZoom;
                super.onLongPress(e);
            }

        });

        ViewRoot.findViewById(R.id.btnMyLocation).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mDetector.onTouchEvent(event);
            }
        });


        return ViewRoot;
    }

    public MyAppBackground MAB;
    GoogleMap MAPA;
    boolean flagMyZoom = false;
    Integer Drivers;
    private GestureDetector mDetector;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody) getActivity()).flSolicitudes.setVisibility(View.GONE);
        ((RootBody) getActivity()).tvTitulo.setText("Disponible");
        ((RootBody) getActivity()).Emergencia.setBackgroundResource(R.drawable.bg_touch_rojo4r);
        ((RootBody) getActivity()).Emergencia.setEnabled(true);

        ((RootBody) getActivity()).GPS.setIMyGps(new MyGps.IMyGps() {
            @Override
            public void onLocation(Location l) {
                MAPA.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(l.getLatitude(), l.getLongitude()), AppBackground.ZOOM_GOOGLEMAPS, 0, 0)));//LatLng target, float zoom, float tilt, float bearing

                if (getActivity() != null) {
                    if (((RootBody) getActivity()).GPS != null) {
                        ((RootBody) getActivity()).GPS.turnOff();
                    }
                }

                if (MAPA != null) {
                    if (MTaxista == null) {
                        MTaxista = MAPA.addMarker
                                (
                                        new MarkerOptions()
                                                .position(new LatLng(l.getLatitude(), l.getLongitude()))
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_conductor))
                                                .title("Usted esta aqui")
                                );
                    } else {
                        MTaxista.setPosition(new LatLng(l.getLatitude(), l.getLongitude()));
                    }
                }
            }

            @Override
            public void onOldLocation(Location l) {
                try {
                    MAPA.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(l.getLatitude(), l.getLongitude()), AppBackground.ZOOM_GOOGLEMAPS, 0, 0)));//LatLng target, float zoom, float tilt, float bearing
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onEstaApagado() {
            }

            @Override
            public void onFueApagado() {
            }

            @Override
            public void onGpsTurnOff(Status status) {

            }

            @Override
            public void onGpsTurnOn() {

            }
        });

        mDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {

                Log.d("onSingleTapUp", ">");
                ((RootBody) getActivity()).GPS.turnOn(true);
                return true;
            }


            @Override
            public boolean onDoubleTap(MotionEvent e) {
                /*float zoom=FSOL.MAPA.getCameraPosition().zoom;
                if(flagMyZoom)
                {
                    if(zoom<FSOL.MAPA.getMaxZoomLevel())
                    {zoom+=0.5;FSOL.MAPA.moveCamera(CameraUpdateFactory.zoomTo(zoom));}
                }
                else
                {
                    if(zoom>FSOL.MAPA.getMinZoomLevel())
                    {zoom-=0.5;FSOL.MAPA.moveCamera(CameraUpdateFactory.zoomTo(zoom));}
                }*/
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                flagMyZoom = !flagMyZoom;
                super.onLongPress(e);
            }

        });
        //cdt.start();

        MAB = new MyAppBackground(getActivity()) {

            @Override
            public void todo(final AppBackground app, int idop, Object o) {

                switch (idop) {
                    case 0:


                        new ConfirmDialog(getActivity().getWindow().getContext(), "¿Está seguro desconectarse?", getString(R.string.app_empresa), "Salir") {
                            @Override
                            public void onPositive() {
                                app.NodeJsTurnOff();
                                dismisss();
                                //saliendo=true;
                                //getActivity().onBackPressed();
                            }

                        }.show();

                        break;

                    case 1:
                        MAB.ABG.AceptarServicioConductor((JsonPack.NuevaSolicitud) o);
                        break;

                }


            }

        };

        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                MAPA = googleMap;
                MAPA.getUiSettings().setMyLocationButtonEnabled(false);
                //MAPA.setMyLocationEnabled(false);
                MAPA.getUiSettings().setCompassEnabled(false);
                MAPA.getUiSettings().setZoomControlsEnabled(false);
                MAPA.getUiSettings().setMapToolbarEnabled(false);
                MAPA.getUiSettings().setScrollGesturesEnabled(true);

                /*
                PolygonOptions rectOptions = new PolygonOptions()
                        .fillColor(Color.YELLOW)
                        .addAll(Tools.generarArray());
                Polygon polygon = googleMap.addPolygon(rectOptions);*/


                ((RootBody) getActivity()).GPS.turnOn(true);
                asignandoListener();
            }
        });
    }

    public void asignandoListener() {
        if (MAPA != null) {
            MSL = new MapStateListener(MAPA, this, getActivity()) {
                @Override
                public void onMapTouched() {


                }

                @Override
                public void onMapReleased() {

                }

                @Override
                public void onMapUnsettled() {

                }

                @Override
                public void onMapSettled() {

                }
            };
        } else {
            Log.e("mapa", "esta null");
        }

    }

    //public boolean saliendo=false;
    @Override
    public void onStart() {
        super.onStart();

        MAB.addListner(new SocketIoBridge() {

            @Override
            public void ReservaPendienteConductor(JsonPack.ReservaPendiente r) {

                Toast.makeText(getActivity(), "No puede aceptar servicio, tiene un reserva pendiente.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void nuevaSolicitudConductor(JsonPack.NuevaSolicitud ns) {
                rvLista.setVisibility(View.VISIBLE);
                solicitudes.add(ns);
                mAdapter.notifyDataSetChanged();
                //mAdapter.notifyItemInserted(solicitudes.size()-1);

                ivImagen.setVisibility(View.GONE);
            }

            @Override
            public void ServicioCaducado(JsonPack.ServicioCad ser) {
                //super.ServicioCaducado(ser);

                if (solicitudes != null) {
                    JsonPack.NuevaSolicitud ns = null;
                    for (JsonPack.NuevaSolicitud n : solicitudes) {
                        if (n.ServicioId.equals(ser.IDServicio)) {
                            ns = n;
                            break;
                        }
                    }
                    if (ns != null) {
                        solicitudes.remove(ns);
                        mAdapter.notifyDataSetChanged();
                    }
                    if (solicitudes.size() == 0) {
                        ivImagen.setVisibility(View.VISIBLE);
                    }

                }
            }

            @Override
            public void refreshCoordinates(double lat, double lng) {
                super.refreshCoordinates(lat, lng);

                Log.e("CoordinatesUpdate", "Actualizando Coordenadas: " + lat + ", " + lng);

                if (MAPA == null)
                    return;

                MAPA.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(lat, lng), AppBackground.ZOOM_GOOGLEMAPS, 0, 0)));//LatLng target, float zoom, float tilt, float bearing

                if (MTaxista == null) {
                    MTaxista = MAPA.addMarker
                            (
                                    new MarkerOptions()
                                            .position(new LatLng(lat, lng))
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_conductor))
                                            .title("Usted esta aqui")
                            );
                } else {
                    MTaxista.setPosition(new LatLng(lat, lng));
                }
            }

            @Override
            public void ConductorSeleccionado(JsonPack.RespEstadoCondcutor s) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new FragInfoCliente(), FragInfoCliente.class.getSimpleName())
                        //.addToBackStack("b")
                        .commit();

            }

            @Override
            public void disconnect(String s) {

                //getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new FragBienvenido(), FragBienvenido.class.getSimpleName() + System.currentTimeMillis())
                        .commit();

            }
        });
    }

    @Override
    public void onStop() {

        Log.e("MyAppBackground", "removeListener Frag Disponible " + this.hashCode());
        MAB.removeListener();
        super.onStop();
        //cdt.cancel();
    }


    @Override
    public RecyclerView.ViewHolder getInstancia(View v) {
        return new SolicitudVh(v);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnSalir:

                MAB.doit(getActivity(), 0);

                break;
        }

    }

    public class SolicitudVh extends RecyclerView.ViewHolder implements IMyAdapter<SolicitudVh>, View.OnClickListener {

        public TextView tvOrigen, tvDestino, tvDirOrigen, tvDirDestino, tvViajes, tvTarifas, tvRefOrigen, tvRefDestino;
        public Button btnAceptar, btnRechazar;
        public ImageView ivPago;
        public RatingBar rbCalificacion;


        public SolicitudVh(View v) {
            super(v);

            tvOrigen = (TextView) v.findViewById(R.id.tvOrigen);
            tvDirOrigen = (TextView) v.findViewById(R.id.tvDirOrigen);
            tvRefOrigen = (TextView) v.findViewById(R.id.tvRefOrigen);
            tvDestino = (TextView) v.findViewById(R.id.tvDestino);
            tvDirDestino = (TextView) v.findViewById(R.id.tvDirDestino);
            tvRefDestino = (TextView) v.findViewById(R.id.tvRefDestino);
            tvTarifas = (TextView) v.findViewById(R.id.tvTarifas);
            tvViajes = (TextView) v.findViewById(R.id.tvViajes);

            btnAceptar = (Button) v.findViewById(R.id.btnAceptar);
            //btnAceptar.setOnClickListener(this);
            btnRechazar = (Button) v.findViewById(R.id.btnRechazar);
            //btnRechazar.setOnClickListener(this);
            ivPago = (ImageView) v.findViewById(R.id.ivPago);
            rbCalificacion = (RatingBar) v.findViewById(R.id.rbCalificacion);
        }

        @Override
        public void bindView(SolicitudVh holder, int position) {
            holder.btnAceptar.setOnClickListener(this);
            holder.btnRechazar.setOnClickListener(this);

            holder.tvOrigen.setText(solicitudes.get(position).OrigenNombre);
            holder.tvDirOrigen.setText(solicitudes.get(position).dirOrigen);
            holder.tvDestino.setText(solicitudes.get(position).DestinoNombre);
            holder.tvDirDestino.setText(solicitudes.get(position).dirDestino);
            holder.tvViajes.setText("" + solicitudes.get(position).Viajes);
            holder.rbCalificacion.setRating(solicitudes.get(position).Calificacion);
            holder.tvRefOrigen.setText(solicitudes.get(position).referenciaorigen);
            holder.tvRefDestino.setText(solicitudes.get(position).referenciadestino);

            if (solicitudes.get(position).tipopago != null) {
                switch (solicitudes.get(position).tipopago) {
                    case "1":
                        holder.ivPago.setImageResource(R.drawable.vehiculo_e);
                        break;
                    case "2":
                        holder.ivPago.setImageResource(R.drawable.vehiculo_v);
                        break;
                    case "3":
                        holder.ivPago.setImageResource(R.drawable.vehiculo_t);
                        break;

                }
            }

            holder.tvTarifas.setText("S/. " + Tools.formatFloat(solicitudes.get(position).tarifa));
            holder.getAdapterPosition();
        }

        @Override
        public void onClick(View v) {

            v.setOnClickListener(null);
            switch (v.getId()) {
                case R.id.btnAceptar:
                    MAB.doit(getActivity(), 1, solicitudes.get(getLayoutPosition()));
                    break;
                case R.id.btnRechazar:
                    break;
            }

            Log.d("onclick", ">" + getLayoutPosition());
            solicitudes.remove(getLayoutPosition());
            mAdapter.notifyItemRemoved(getLayoutPosition());

            if (solicitudes.size() <= 0) {
                ivImagen.setVisibility(View.VISIBLE);
            }

            if (solicitudes.size() == 0) {
                rvLista.setVisibility(View.GONE);
            }
        }
    }
}
