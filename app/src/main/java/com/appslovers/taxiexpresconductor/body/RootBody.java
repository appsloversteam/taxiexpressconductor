package com.appslovers.taxiexpresconductor.body;

import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.AppBackground;
import com.appslovers.taxiexpresconductor.AppBinder;
import com.appslovers.taxiexpresconductor.dialogs.DialogAlerta;
import com.appslovers.taxiexpresconductor.dialogs.DialogNuevaReserva;
import com.appslovers.taxiexpresconductor.dialogs.DialogRegistroEnProceso;
import com.appslovers.taxiexpresconductor.util.ConfirmDialog;
import com.appslovers.taxiexpresconductor.data.ISetActivityResult;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.util.MyGps;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.RootSplash;
import com.appslovers.taxiexpresconductor.util.SocketIoBridge;
import com.google.firebase.crash.FirebaseCrash;

public class RootBody extends AppCompatActivity implements View.OnClickListener, ISetActivityResult {


    public AppBinder AppConexion;
    public MyGps GPS;

    boolean flag_fragbienvenido = true;
    ServiceConnection SC = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            AppConexion = ((AppBinder) iBinder);
            AppConexion.setRoot(RootBody.this);
            Log.d("Service Conection", " onServiceConnected---------------------->");
            actualizarVista();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

            //solo se llama cuando el servicio se cae :(
            //Log.d("Service Conection", " onServiceDisconnected---------------------->");
            AppConexion.setApp(null);
            AppConexion.setRoot(null);

        }
    };


    public DrawerLayout drawer;
    public TextView tvTitulo;
    public FrameLayout flSolicitudes;
    public TextView tvNroSolicitudes;
    public Button Emergencia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rootbody);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        flSolicitudes = (FrameLayout) findViewById(R.id.flSolicitudes);
        tvNroSolicitudes = (TextView) findViewById(R.id.tvNroSolicitudes);
        findViewById(R.id.btnMenu).setOnClickListener(this);
        findViewById(R.id.btnSolicitudes).setOnClickListener(this);

        Emergencia = (Button) findViewById(R.id.btnEmergencia);
        Emergencia.setOnClickListener(this);
        findViewById(R.id.btnReservas).setOnClickListener(this);
        findViewById(R.id.btnTarifario).setOnClickListener(this);
        findViewById(R.id.btnHistorial).setOnClickListener(this);
        findViewById(R.id.btnServicio).setOnClickListener(this);
        findViewById(R.id.btnBeneficio).setOnClickListener(this);
        findViewById(R.id.btnCerrar).setOnClickListener(this);
        findViewById(R.id.btnRecargas).setOnClickListener(this);
        findViewById(R.id.btnContactenos).setOnClickListener(this);
        findViewById(R.id.btnProtocolo).setOnClickListener(this);

        tvTitulo = (TextView) findViewById(R.id.tvTitulo);
        tvTitulo.setText(getString(R.string.app_empresa));

    }

    public void mostarTopBarSolicitud() {
        if (AppConexion != null) {
            if (AppConexion.getApp() != null) {
                if (AppConexion.getApp().isNodeJsConnected()) {
                    if (AppConexion.getApp().getServicioActual() == null) {
                        flSolicitudes.setVisibility(View.VISIBLE);
                    } else if (AppConexion.getApp().getServicioActual().getEstado() > 3) {
                        flSolicitudes.setVisibility(View.VISIBLE);
                    }

                } else {
                    flSolicitudes.setVisibility(View.GONE);
                }
            } else {
                flSolicitudes.setVisibility(View.GONE);
            }
        } else {
            flSolicitudes.setVisibility(View.GONE);
        }
    }



    /*
    new MyAppBackground(getActivity())
    {@Override public void todo(AppBackground app, int idop, Object obj) {}}
    .addListner();*/

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    DialogNuevaReserva DNR;

    public SocketIoBridge.AuxiliarBridge SIOBAB = new SocketIoBridge.AuxiliarBridge() {

        @Override
        public void onNuevasSolicitudes(int cantidad) {

            tvNroSolicitudes.setText(String.format("%02d", cantidad));
            Log.i(">nuevas solicitudes", ">" + cantidad);
        }

        @Override
        public void onNodeNuevaReserva(JsonPack.NuevaReserva nr) {

            if (DNR == null) {
                DNR = new DialogNuevaReserva();
            }
            DNR.NR = nr;
            if (!DNR.isAdded())
                DNR.show(getSupportFragmentManager(), "d");

        }
    };

    public void actualizarVista() {
        if (AppConexion.getApp() != null)
            AppConexion.getApp().setAuxiliarBridge(SIOBAB);

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fl_root_inside);
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            if (AppConexion.getApp().isNodeJsConnected()) {
                if (AppConexion.getApp().getServicioActual() != null && AppConexion.getApp().getServicioActual().getEstado() == 1) {
                    if (f == null || !(f instanceof FragInfoCliente)) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_root_inside, new FragInfoCliente(), FragInfoCliente.class.getSimpleName() + System.currentTimeMillis())
                                .commit();
                    }
                } else if (AppConexion.getApp().getServicioActual() != null && AppConexion.getApp().getServicioActual().getEstado() == 2) {
                    if (f == null || !(f instanceof FragInfoCliente)) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_root_inside, new FragInfoCliente(), FragInfoCliente.class.getSimpleName() + System.currentTimeMillis())
                                .commit();
                    }
                } else if (AppConexion.getApp().getServicioActual() != null && AppConexion.getApp().getServicioActual().getEstado() == 3) {
                    if (f == null || !(f instanceof FragInfoCliente)) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_root_inside, new FragInfoCliente(), FragInfoCliente.class.getSimpleName() + System.currentTimeMillis())
                                .commit();
                    }
                } else {
                    if (f == null || !(f instanceof FragDisponible)) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_root_inside, new FragDisponible(), FragDisponible.class.getSimpleName() + System.currentTimeMillis())
                                .commit();
                    }
                }

            } else {
                if (flag_fragbienvenido) {
                    flag_fragbienvenido = false;
                    if (f == null || !(f instanceof FragBienvenido)) {
                        getSupportFragmentManager().beginTransaction()
                                .add(R.id.fl_root_inside, new FragBienvenido(), FragBienvenido.class.getSimpleName())
                                .commit();
                    }
                } else {
                    if (f == null || !(f instanceof FragBienvenido)) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_root_inside, new FragBienvenido(), FragBienvenido.class.getSimpleName())
                                .commit();
                    }
                }
            }

        } else {
            Log.e("servconected", " entry backstack != 0 solo se usa con los fragments de menu");
            Log.e("servconected", " entry backstack != 0 solo se usa con los fragments de menu");
            if (AppConexion.getApp().isNodeJsConnected()) {
                if (AppConexion.getApp().getServicioActual() != null && AppConexion.getApp().getServicioActual().getEstado() == 1) {
                    if (f == null || !(f instanceof FragInfoCliente)) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_root_inside, new FragInfoCliente(), FragInfoCliente.class.getSimpleName() + System.currentTimeMillis())
                                .commit();
                    }
                } else if (AppConexion.getApp().getServicioActual() != null && AppConexion.getApp().getServicioActual().getEstado() == 2) {
                    if (f == null || !(f instanceof FragInfoCliente)) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_root_inside, new FragInfoCliente(), FragInfoCliente.class.getSimpleName() + System.currentTimeMillis())
                                .commit();
                    }
                } else if (AppConexion.getApp().getServicioActual() != null && AppConexion.getApp().getServicioActual().getEstado() == 3) {
                    if (f == null || !(f instanceof FragInfoCliente)) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_root_inside, new FragInfoCliente(), FragInfoCliente.class.getSimpleName() + System.currentTimeMillis())
                                .commit();
                    }
                } else {
                    if (f == null || !(f instanceof FragDisponible)) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_root_inside, new FragDisponible(), FragDisponible.class.getSimpleName() + System.currentTimeMillis())
                                .commit();
                    }
                }

            }
        }


        /*
        new MyAppBackground(this) {
            @Override
            public void todo(AppBackground app, int idop, Object obj) {

                if(app.isNodeJsConnected())
                {
                    //app.EnviarAlerta();
                    Emergencia.setEnabled(true);
                    Emergencia.setBackgroundResource(R.drawable.bg_touch_rojo4r);
                }
                else
                {
                    Emergencia.setEnabled(false);
                    Emergencia.setBackgroundResource(R.drawable.bg_touch_grisconborde4r);
                }
            }

        }.doit(this, 0);*/
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        GPS = new MyGps(this);

        //bindService(new Intent(this, AppBackground.class), SC, Context.BIND_AUTO_CREATE);
        startService(new Intent(getApplicationContext(), AppBackground.class));
    }


    @Override
    protected void onStart() {
        super.onStart();
        bindService(new Intent(this, AppBackground.class), SC, 0);

    }

    @Override
    protected void onStop() {
        unbindService(SC);
        super.onStop();
    }

    @Override
    public void onBackPressed() {


        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fl_root_inside);
        if (f instanceof DialogRegistroEnProceso) {
            finish();
        } else if (f instanceof FragDisponible) {

            //if(!((FragDisponible)f).saliendo) {
            new ConfirmDialog(getWindow().getContext(), "¿Está seguro de salir de la aplicación?", getString(R.string.app_empresa), "Salir") {
                @Override
                public void onPositive() {

                    dismisss();
                    retroceder();
                }


            }.show();
            //}

        } else if (
                (f instanceof Frag_5_Beneficios)
                        || (f instanceof Frag_1_Reservas)
                        || (f instanceof Frag_2_Tarifario)
                        || (f instanceof Frag_3_ServiciosHistorial)
                        || (f instanceof Frag_7_ServicioEmergencia)
                        || (f instanceof Frag_4_Recargas)
                ||(f instanceof FragProtocoloAtencion)
                ) {
            actualizarVista();
        } else {
            super.onBackPressed();
        }

        //super.onBackPressed();

    }

    public void retroceder() {
        super.onBackPressed();
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {

            case R.id.btnProtocolo:

                drawer.closeDrawers();

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new FragProtocoloAtencion(), FragProtocoloAtencion.class.getSimpleName())
                        //.addToBackStack("dsfsdfsdadf")
                        .commit();


                break;


            case R.id.btnRecargas:
                drawer.closeDrawers();

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new Frag_4_Recargas(), Frag_4_Recargas.class.getSimpleName())
                        //.addToBackStack("dsfsdfsdadf")
                        .commit();
                break;

            case R.id.btnMenu:

                //FirebaseCrash.report(new Exception("My first Android non-fatal error 3"));
                // SI.Desconectar();

                /*
                Alerta A=new Alerta();
                A.idConductor=new SPUser(getApplicationContext()).getReponseLogin().conductor_id;
                A.latitud=-12.04666545d;
                A.longitud=-77.04261303d;*/
                //SI.emitData("alertaTaxi", new Gson().toJson(A));


                drawer.openDrawer(GravityCompat.START);


                break;

            case R.id.btnSolicitudes:

                tvNroSolicitudes.setText("0");
                drawer.closeDrawers();
                //getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new FragDisponible(), FragDisponible.class.getSimpleName() + System.currentTimeMillis())
                        .commit();

                break;

            case R.id.btnBeneficio:


                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new Frag_5_Beneficios(), FragBienvenido.class.getSimpleName())
                        //.addToBackStack("dsfsdfsdadf")
                        .commit();
                drawer.closeDrawers();
                break;

            case R.id.btnServicio:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new Frag_7_ServicioEmergencia(), Frag_7_ServicioEmergencia.class.getSimpleName())
                        //.addToBackStack("dsfsdfddf")
                        .commit();
                drawer.closeDrawers();

                break;
            case R.id.btnHistorial:

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new Frag_3_ServiciosHistorial(), Frag_3_ServiciosHistorial.class.getSimpleName())
                        //.addToBackStack("dsfsdfdf")
                        .commit();
                drawer.closeDrawers();
                break;

            case R.id.btnTarifario:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new Frag_2_Tarifario(), Frag_2_Tarifario.class.getSimpleName())
                        //.addToBackStack("dsfsdf")
                        .commit();
                drawer.closeDrawers();

                break;
            case R.id.btnReservas:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new Frag_1_Reservas(), Frag_1_Reservas.class.getSimpleName())
                        //.addToBackStack("dsfsdf")
                        .commit();
                drawer.closeDrawers();

                break;


            case R.id.btnEmergencia:

                DialogAlerta md = new DialogAlerta();
                md.setTargetFragment(getSupportFragmentManager().findFragmentById(R.id.fl_root_inside), 145);
                md.show(getSupportFragmentManager(), "alert");
                break;

            case R.id.btnIniciarSesion:


                break;

            case R.id.btnContactenos:

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fl_root_inside, new Frag_6_Contactenos(), Frag_6_Contactenos.class.getSimpleName())
                        .addToBackStack("contactenos")
                        .commit();

                drawer.closeDrawers();

                break;
            case R.id.btnCerrar:


                new ConfirmDialog(getWindow().getContext(), "¿Desea cerrar sesión?", getString(R.string.app_empresa), "Cerrar Sesión") {
                    @Override
                    public void onPositive() {

                        if (AppConexion != null) {
                            AppConexion.getApp().NodeJsTurnOff();
                            AppConexion.getApp().detenerServicio();
                        }

                        dismisss();

                        SPUser.logout(getApplicationContext());
                        startActivity(new Intent(getApplicationContext(), RootSplash.class));
                        finish();
                    }
                }.show();

                break;
        }
    }


    MyGps mygps;

    @Override
    public void setMyInterface(MyGps mgps) {
        mygps = mgps;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (mygps != null) {
            mygps.onGetResult(requestCode, resultCode, data);
        }

    }

    NotificationManager NM;

    @Override
    protected void onResume() {
        super.onResume();
        NM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NM.cancelAll();
    }


}
