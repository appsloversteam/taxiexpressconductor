package com.appslovers.taxiexpresconductor.body;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.AppForeGround;
import com.appslovers.taxiexpresconductor.data.IMyAdapter;
import com.appslovers.taxiexpresconductor.data.IMyViewHolder;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.dialogs.DialogCanjeRealizado;
import com.appslovers.taxiexpresconductor.dialogs.DialogConfirmarCanje;
import com.appslovers.taxiexpresconductor.util.MyAdapter;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.data.Urls;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class FragCanjes  extends Fragment implements IMyViewHolder{


    public static FragCanjes nuevo(int puntos)
    {
        FragCanjes fc=new FragCanjes();
        fc.Puntos=puntos;
        return fc;
    }
    View ViewRoot;
    RecyclerView rvLista;
    ArrayList<JsonPack.Promocion> promociones;
    LinearLayoutManager llm;
    MyAdapter<FragCanjes.PromocionViewHolder,JsonPack.Promocion> mAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot=inflater.inflate(R.layout.fragcanjes,null);
        rvLista=(RecyclerView)ViewRoot.findViewById(R.id.lista);

        llm=new LinearLayoutManager(getActivity());
        rvLista.setLayoutManager(llm);

        return ViewRoot;
    }
    public int Puntos;
    JsonPack.ResponseLogin rl;
    DialogConfirmarCanje DCC;
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody)getActivity()).tvTitulo.setText("Canjes");

        rl=new SPUser(getActivity()).getResponseLogin();
        MHR.putParams("conductor_id", rl.conductor_id);
        MHR.send();

        DCC=new DialogConfirmarCanje();
        DCC.setTargetFragment(FragCanjes.this,333);
    }
    ProgressDialog pdProcesando;
    JsonPack.Promocion SelPro;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //Toast.makeText(getContext(),requestCode+" - "+resultCode,Toast.LENGTH_SHORT).show();
        switch (requestCode)
        {
            case 333:

                if(resultCode==200)
                {
                    if(SelPro!=null)
                    {

                        if(pdProcesando==null)
                            pdProcesando= ProgressDialog.show(getActivity(),getString(R.string.app_empresa), "Procesando canje...", true, false);
                        else {
                            pdProcesando.setMessage("Procesando canje...");
                            pdProcesando.show();
                        }

                        MHR_canjear.putParams("conductor_id",rl.conductor_id);
                        MHR_canjear.putParams("promo_id",SelPro.promo_id);
                        MHR_canjear.send();
                    }

                }
                break;
        }
    }



    MyRequest MHR=new MyRequest(Urls.ws_lista_beneficio, MyRequest.HttpRequestType.GET)
    {
        @Override
        public void onSuccesForeground(ResponseWork rw) {
            if(pdProcesando!=null)
                pdProcesando.cancel();
            promociones=null;
            try {


                //promociones=new Gson().fromJson(rw.ResponseBody,  new TypeToken<ArrayList<JsonPack.Promocion>>() {}.getType());
                JsonPack.ResponsePromociones rp=new Gson().fromJson(rw.ResponseBody, JsonPack.ResponsePromociones.class);
                if(rp!=null && rp.result!=null)
                {
                    promociones= rp.result;
                    mAdapter=new MyAdapter<>(promociones,FragCanjes.this,R.layout.fragcanjes_item);
                    rvLista.setAdapter(mAdapter);
                }
            }
            catch (JsonParseException e)
            {
                Log.e("json ex", ">" + e.getMessage());
            }

        }

        @Override
        public void onFailedRequest(ResponseWork rw, boolean isActive) {
            if(pdProcesando!=null)
                pdProcesando.cancel();

            SelPro=null;
            Toast.makeText(getContext(),"Error en el canje Realizado",Toast.LENGTH_SHORT).show();
        }
    };


    MyRequest MHR_canjear=new MyRequest(Urls.ws_canjear_beneficios, MyRequest.HttpRequestType.GET)
    {
        @Override
        public void onSuccesForeground(ResponseWork rw) {


            JsonPack.ResponseGeneric rg;
            try {

                rg=new Gson().fromJson(rw.ResponseBody, JsonPack.ResponseGeneric.class);
                if(rg.status)
                {
                    MHR_puntos.putParams("conductor_id",rl.conductor_id).send();
                    new DialogCanjeRealizado().show(getFragmentManager(),"realizado");
                    //Toast.makeText(getActivity(),"Canje Realizado",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getActivity(),"Error en el canje Realizado",Toast.LENGTH_SHORT).show();
                }
            }
            catch (JsonParseException e)
            {
                Log.e("json ex", ">" + e.getMessage());
                Toast.makeText(getActivity(),"Error en el canje Realizado",Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onFailedRequest(ResponseWork rw, boolean isActive) {
            SelPro=null;
            Toast.makeText(getContext(),"Error en el canje Realizado",Toast.LENGTH_SHORT).show();
        }
    };

    MyRequest<JsonPack.RespPuntos> MHR_puntos=new MyRequest<JsonPack.RespPuntos>(Urls.ws_obtener_puntos, MyRequest.HttpRequestType.GET)
    {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.RespPuntos object) {
            if(object!=null) {

                Puntos=object.puntos;
               // mAdapter.notifyDataSetChanged();
                MHR.putParams("conductor_id", rl.conductor_id);
                MHR.send();
            }
        }
    };

    @Override
    public RecyclerView.ViewHolder getInstancia(View v) {
        return new PromocionViewHolder(v);
    }

    public class PromocionViewHolder  extends RecyclerView.ViewHolder implements IMyAdapter<PromocionViewHolder>,View.OnClickListener
    {


        ImageView ivImagen;
        TextView tvDescripcion,tvPuntos;
        Button btnCajear;
        Integer cPosition;

        public PromocionViewHolder(View v)
        {
            super(v);
            ivImagen=(ImageView)v.findViewById(R.id.ivImagen);
            tvDescripcion=(TextView)v.findViewById(R.id.tvDesc);
            tvPuntos=(TextView)v.findViewById(R.id.tvPuntos);
            btnCajear=(Button)v.findViewById(R.id.btnCanjear);
            btnCajear.setOnClickListener(this);

        }



        @Override
        public void bindView(PromocionViewHolder holder, int position) {

            cPosition = position;

            ((AppForeGround)getActivity().getApplication()).getPicasso().load(Urls.servidorarchivos+promociones.get(position).promo_imagen).into(holder.ivImagen);
            holder.tvPuntos.setText(promociones.get(position).promo_puntos+"["+Puntos+"] || "+promociones.get(position).promo_fvigencia+" || Cant : "+promociones.get(position).promo_stock);

            holder.tvDescripcion.setText(promociones.get(position).promo_nombre);
            holder.btnCajear.setEnabled(true);
        }

        @Override
        public void onClick(View v) {
            if( promociones.get(cPosition).promo_puntos  <=  Puntos ){
                SelPro=promociones.get(getLayoutPosition());
                DCC.show(getFragmentManager(),"confirmar");
            }else{
                Toast.makeText(getActivity(), "Por el momento no tiene puntos suficientes", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
