package com.appslovers.taxiexpresconductor.body;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.data.IMyAdapter;
import com.appslovers.taxiexpresconductor.data.IMyViewHolder;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.data.Urls;
import com.appslovers.taxiexpresconductor.util.DatePickerFragment;
import com.appslovers.taxiexpresconductor.util.MyAdapter;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.util.Tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class FragRecargasMovimientos extends Fragment implements IMyViewHolder,View.OnClickListener{



    View ViewRoot;
    private RecyclerView rvLista;
    ArrayList<JsonPack.MovimientoHist> movs;
    LinearLayoutManager llm;
    Button btnFechaIni,btnFechaFin;
    private  MyAdapter<FragRecargasMovimientos.MoviVh,JsonPack.MovimientoHist> mAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot =inflater.inflate(R.layout.fragrecargas_movimientos,null);
        btnFechaIni=(Button)ViewRoot.findViewById(R.id.btnFechaIni);
        btnFechaFin=(Button)ViewRoot.findViewById(R.id.btnFechaFin);
        btnFechaIni.setOnClickListener(this);
        btnFechaFin.setOnClickListener(this);
        rvLista=(RecyclerView) ViewRoot.findViewById(R.id.lista);
        //rvLista.setHasFixedSize(true);
        llm=new LinearLayoutManager(getActivity());
        rvLista.setLayoutManager(llm);


        return ViewRoot;
    }

    MyRequest<JsonPack.RespRecargaMovimientos> MR=new MyRequest<JsonPack.RespRecargaMovimientos>(Urls.ws_historial_movimientos, MyRequest.HttpRequestType.GET)
    {

        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.RespRecargaMovimientos object) {


            if(object!=null)
            {
                movs=object.result;
                if(movs!=null && movs.size()>0)
                {

                    mAdapter=new MyAdapter<>(movs,FragRecargasMovimientos.this,R.layout.fragrecargamov_item);
                    rvLista.setAdapter(mAdapter);
                }
            }
            else {


            }
        }
    };

    Calendar calini=Calendar.getInstance();
    Calendar calfin=Calendar.getInstance();
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody)getActivity()).mostarTopBarSolicitud();
        ((RootBody)getActivity()).tvTitulo.setText("Recargas");


        calini.add(Calendar.DAY_OF_YEAR,-7);
        btnFechaIni.setText(yyyyMMdd.format(calini.getTime()));
        btnFechaFin.setText(yyyyMMdd.format(calfin.getTime()));


        Pedir();

    }


    public void Pedir()
    {
        if(movs!=null)
        movs.clear();
        if(mAdapter!=null)
        mAdapter.notifyDataSetChanged();

        MR.putParams("conductor_id",new SPUser(getActivity()).getResponseLogin().conductor_id)
                .putParams("fecha_inicio",yyyyMMdd.format(calini.getTime()))
                .putParams("fecha_final",yyyyMMdd.format(calfin.getTime()))
                .send();
    }

    @Override
    public RecyclerView.ViewHolder getInstancia(View v) {
        return new MoviVh(v);
    }


    SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");

    DatePickerFragment DPFINI,DPFFIN;
    @Override
    public void onClick(View v) {

        switch (v.getId())
        {

            case R.id.btnFechaIni:
                if(DPFINI==null)
                {
                    DPFINI=new DatePickerFragment();
                    DPFINI.IDP=new DatePickerFragment.IDatePicker() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int day) {
                            //fechasel=year+"-"+String.format("%02d",(month+1))+"-"+String.format("%02d",day);
                            //btnFecha.setText(fechasel);

                            calini.set(Calendar.YEAR,year);
                            calini.set(Calendar.MONTH,month);
                            calini.set(Calendar.DAY_OF_MONTH,day);
                            btnFechaIni.setText(yyyyMMdd.format(calini.getTime()));
                            Pedir();
                        }
                    };

                }
                DPFINI.show(getFragmentManager(),"date");
                break;
            case R.id.btnFechaFin:
                if(DPFFIN==null)
                {
                    DPFFIN=new DatePickerFragment();
                    DPFFIN.IDP=new DatePickerFragment.IDatePicker() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int day) {
                            //fechasel=year+"-"+String.format("%02d",(month+1))+"-"+String.format("%02d",day);
                            //btnFecha.setText(fechasel);

                            calfin.set(Calendar.YEAR,year);
                            calfin.set(Calendar.MONTH,month);
                            calfin.set(Calendar.DAY_OF_MONTH,day);
                            btnFechaFin.setText(yyyyMMdd.format(calfin.getTime()));
                            Pedir();

                        }
                    };


                }
                DPFFIN.show(getFragmentManager(),"date");
                break;
        }

    }

    public class MoviVh extends RecyclerView.ViewHolder implements IMyAdapter<MoviVh>{

        public TextView tvMonto,tvFecha,tvTipo;


        public MoviVh(View v) {
            super(v);

            tvTipo=(TextView)v.findViewById(R.id.tvTipo);
            tvMonto=(TextView)v.findViewById(R.id.tvMonto);
            tvFecha=(TextView)v.findViewById(R.id.tvFecha);

        }

        @Override
        public void bindView(MoviVh holder, int position) {

            holder.tvTipo.setText(movs.get(position).cta_tipo);
            holder.tvFecha.setText(movs.get(position).cta_fecha.split(" ")[0]);


            holder.tvMonto.setText(Tools.formatFloat(movs.get(position).cta_monto));

            if(movs.get(position).cta_monto<0) {
                holder.tvMonto.setBackgroundColor(getResources().getColor(R.color.bgrojo));
                holder.tvMonto.setTextColor(Color.WHITE);
            }
            else {
                holder.tvMonto.setBackgroundColor(getResources().getColor(R.color.bgverde));
                holder.tvMonto.setTextColor(Color.BLACK);
            }

            /*
            holder.tvNroOp.setText(recargas.get(position).recarga_num_oper);
            holder.tvMonto.setText(recargas.get(position).recarga_monto);
            holder.tvFecha.setText(recargas.get(position).fecha);
            holder.tvEntidad.setText(recargas.get(position).banco_descripcion);
            holder.tvTransaccion.setText(recargas.get(position).recarga_tipo);*/

        }

    }

}
