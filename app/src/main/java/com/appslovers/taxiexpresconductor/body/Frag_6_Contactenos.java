package com.appslovers.taxiexpresconductor.body;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.R;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class Frag_6_Contactenos extends Fragment implements View.OnClickListener{


    View ViewRoot;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot=inflater.inflate(R.layout.fragcontactenos,null);
        ViewRoot.findViewById(R.id.btnEmail).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnLlamar).setOnClickListener(this);

        return ViewRoot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody)getActivity()).mostarTopBarSolicitud();


        ((RootBody)getActivity()).tvTitulo.setText("Contáctenos");

        //int t=5/0;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btnEmail:



                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.contacto_email)});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                try {
                    startActivity(Intent.createChooser(i, "Enviar correo electrónico"));
                } catch (android.content.ActivityNotFoundException ex) {
                    //Toast.makeText(getActivity(),"Error en el envio de correo electronico.", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.btnLlamar:
                if (android.util.Patterns.PHONE.matcher(getString(R.string.contacto_telcell)).matches()) {
                    String uri= "tel:"+getString(R.string.contacto_telcell);
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse(uri));
                    getActivity().startActivity(intent);

                } else {
                    //Toast.makeText(getActivity(), "Error en formato de numero ", Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }
}
