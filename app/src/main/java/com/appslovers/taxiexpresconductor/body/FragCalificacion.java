package com.appslovers.taxiexpresconductor.body;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;

import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.dialogs.DialogFinalizarServicio;
import com.appslovers.taxiexpresconductor.dialogs.DialogPorquecalifico;
import com.appslovers.taxiexpresconductor.util.MyAppBackground;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.AppBackground;
import com.appslovers.taxiexpresconductor.RootSplash;
import com.appslovers.taxiexpresconductor.util.SocketIoBridge;

import static com.appslovers.taxiexpresconductor.R.id.btnCancelar;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class FragCalificacion extends Fragment implements View.OnClickListener {

    View ViewRoot;
    Button btnTerminar;
    RatingBar rbCalificacion;
    DialogPorquecalifico DPC;
    DialogFinalizarServicio DFS;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot = inflater.inflate(R.layout.fragcalfiicacion, null);
        rbCalificacion = (RatingBar) ViewRoot.findViewById(R.id.rbCalificacion);
        rbCalificacion.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if (rating <= 2.0) {
                    if (DPC == null)
                        DPC = new DialogPorquecalifico();


                    if (!DPC.isAdded())
                        DPC.show(getFragmentManager(), "dfdf");

                }
                btnTerminar.setEnabled(true);
                btnTerminar.setTextColor(Color.WHITE);
            }
        });
        btnTerminar = (Button) ViewRoot.findViewById(R.id.btnTerminar);
        btnTerminar.setOnClickListener(this);

        return ViewRoot;

    }

    MyAppBackground MAB;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody) getActivity()).flSolicitudes.setVisibility(View.GONE);
        ((RootBody) getActivity()).tvTitulo.setText("Calificacíon");

        MAB = new MyAppBackground(getActivity()) {
            @Override
            public void todo(AppBackground app, int idop, Object o) {

                String mensaje = "";
                if (DPC != null)
                    mensaje = DPC.MENSAJE;

                app.CalificarCliente(rbCalificacion.getRating(), mensaje);
                app.NodeJsTurnOff();
                getActivity().startActivity(new Intent(getActivity(), RootSplash.class));
                getActivity().finish();
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();

        MAB.addListner(new SocketIoBridge() {

            @Override
            public void FinalizarServicioTaxista(JsonPack.RespFinalizarServ s) {
                super.FinalizarServicioTaxista(s);

                //llEnCamino.setVisibility(View.GONE);
                //btnCancelar.setVisibility(View.GONE);

                if (FragCalificacion.this != null) {
                    if (DFS == null) {
                        DFS = new DialogFinalizarServicio();
                        DFS.FS = s;
                        DFS.setTargetFragment(FragCalificacion.this, 12345);
                    }
                    if (!DFS.isAdded())
                        DFS.show(getFragmentManager(), "dfdsf");
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnTerminar:
                MAB.doit(getActivity(), 0);
                break;
        }


    }
}
