package com.appslovers.taxiexpresconductor;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.data.MyDataBase;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.util.Tools;
import com.appslovers.taxiexpresconductor.data.Urls;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 20/05/16.
 */
public class AppForeGround extends MultiDexApplication {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    //7public MyHttpRequest MHTTPR;
    Picasso Pic;
    private MyDataBase db;
    @Override
    public void onCreate() {
        super.onCreate();
        Log.v("FINGER_POINT", Tools.getCertificateSHA1Fingerprint(getApplicationContext()) + ";" + getApplicationContext().getPackageName());
        Log.v("HASH_FACEBOOK", Tools.keyhashfacebook(getApplicationContext()));


        // MHTTPR=new MyHttpRequest();
        // registerActivityLifecycleCallbacks(MHTTPR);
        /*
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException (Thread thread, Throwable e)
            {

                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        });*/
    }


    public int Alerta;
    public int flag_califico=0;
    public int flag_tipotarifa;
    public MyDataBase getDb()
    {
        if(db==null)
            db=new MyDataBase(getApplicationContext());

        return db;
    }

    private ArrayList<JsonPack.Zona> Zonas;
    public ArrayList<JsonPack.Zona> getZonas()
    {
        if(Zonas==null)
            Zonas=getDb().getZonas();

        return Zonas;
    }
    public Picasso getPicasso()
    {
        if(Pic==null)
            Pic=Picasso.with(getApplicationContext());
        return Pic;
    }

    public void recuperarContrasena(String email)
    {
        new MyRequest<JsonPack.ResponseGeneric>( Urls.ws_recuperar_password, MyRequest.HttpRequestType.POST) {

            @Override
            public void onParseSuccesForeground(ResponseWork rw, JsonPack.ResponseGeneric object) {


                /*
                if(object!=null) {

                    if(object.status) {


                        //Toast.makeText(getActivity(),object.mensaje,Toast.LENGTH_SHORT).show();

                    }
                    else
                    {
                        //Toast.makeText(getActivity(),object.mensaje,Toast.LENGTH_SHORT).show();
                    }
                }*/

            }


        }
                .putParams("correo",email)
                .send();
    }


    public void guardarVale(String vale,String idServicio)
    {

        new MyRequest<JsonPack.ResponseGeneric>(Urls.save_vale, MyRequest.HttpRequestType.POST) {

            @Override
            public void onParseSuccesForeground(ResponseWork rw, JsonPack.ResponseGeneric object) {
                /*
                if(object!=null) {

                    if(object.status) {
                        //Toast.makeText(getActivity(),object.mensaje,Toast.LENGTH_SHORT).show();
                    }
                    else{
                        //Toast.makeText(getActivity(),object.mensaje,Toast.LENGTH_SHORT).show();
                    }
                }*/
            }

        }
        .putParams("vale",vale)
        .putParams("idservicio",idServicio)
        .send();
    }

}
