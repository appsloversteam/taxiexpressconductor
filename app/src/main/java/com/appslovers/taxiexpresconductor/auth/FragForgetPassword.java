package com.appslovers.taxiexpresconductor.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.util.ConfirmDialog;
import com.appslovers.taxiexpresconductor.util.MyFragment;

/**
 * Created by javierquiroz on 27/05/16.
 */
public class FragForgetPassword extends MyFragment implements View.OnClickListener{


    EditText etCorreo;
    View ViewRoot;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot=inflater.inflate(R.layout.fragforgetpassword,null);
        ViewRoot.findViewById(R.id.btnEnviar).setOnClickListener(this);
        etCorreo=(EditText)ViewRoot.findViewById(R.id.etEmail);
        return ViewRoot;
    }

    @Override
    public void onClick(View v) {


        if( etCorreo.getText().toString()!=null &&  etCorreo.getText().toString().length()>5)
        {

            new ConfirmDialog(getActivity().getWindow().getContext(), "En breve se le enviara la contraseña a su correo electrónico.", getString(R.string.app_empresa), "Enviar","Cancelar") {
                @Override
                public void onPositive() {


                    getApp().recuperarContrasena(etCorreo.getText().toString());
                    getActivity().onBackPressed();
                }

            }.show();


        }
        else
        {
            Toast.makeText(getActivity(),"Ingresar el correo electronico correctamente",Toast.LENGTH_SHORT).show();
        }
    }
}
