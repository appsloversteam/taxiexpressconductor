package com.appslovers.taxiexpresconductor.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.appslovers.taxiexpresconductor.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.google.gson.Gson;

/**
 * Created by javierquiroz on 20/05/16.
 */
public class RootLoginReg extends FragmentActivity {

    public Gson mGson;
    public CallbackManager mCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.rootlogin);

        mCallbackManager = CallbackManager.Factory.create();
        mGson=new Gson();


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fl_container_login, new FragAuth(), FragAuth.class.getSimpleName())
                .commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);//facebook
    }
}
