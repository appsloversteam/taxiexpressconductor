package com.appslovers.taxiexpresconductor.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.R;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

/**
 * Created by javierquiroz on 22/05/16.
 */
public class FragAuth extends Fragment implements View.OnClickListener{
    List<String> permissionNeeds= Arrays.asList("email", "public_profile");

    View ViewRoot;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        ViewRoot=inflater.inflate(R.layout.fragauth,null);

        ViewRoot.findViewById(R.id.btnIniciarSesion).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnIniciarSesionFacebook).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnRegistrarse).setOnClickListener(this);
        return ViewRoot;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LoginManager.getInstance().registerCallback(((RootLoginReg) getActivity()).mCallbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {


                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object,GraphResponse response) {

                                        Log.v("LoginActivity", response.getRawResponse());

                                        JsonPack.FbResponse fb = new Gson().fromJson(response.getRawResponse(), JsonPack.FbResponse.class);
                                        //Toast.makeText(getActivity(), fb.name, Toast.LENGTH_SHORT).show();


                                        Bundle B = new Bundle();
                                        B.putSerializable("facebook", fb);
                                        //((RootLoginReg) getActivity()).FB = fb;

                                        getActivity().getSupportFragmentManager().popBackStackImmediate();

                                        getActivity().
                                                getSupportFragmentManager()
                                                .beginTransaction()
                                                .replace(R.id.fl_container_login, FragRegister.onNewInstance(B), FragRegister.class.getSimpleName())
                                                .addToBackStack(FragRegister.class.getSimpleName())
                                                .commit();

                                    }
                                });

                        Bundle parameters = new Bundle();
                        //https://developers.facebook.com/docs/graph-api/reference/user
                        parameters.putString("fields", "id,name,email,gender, birthday,last_name");
                        request.setParameters(parameters);
                        request.executeAsync();
                        //Toast.makeText(getActivity(),"succes send request",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {

                        //Toast.makeText(getActivity(),"cancel",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {

                        //Toast.makeText(getActivity(),"error",Toast.LENGTH_SHORT).show();
                    }
                });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnIniciarSesion:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_container_login, new FragStartSession(), FragStartSession.class.getSimpleName())
                        .addToBackStack("b")
                        .commit();

                break;

            case R.id.btnIniciarSesionFacebook:

                LoginManager.getInstance().logInWithReadPermissions(getActivity(),permissionNeeds);

                break;

            case R.id.btnRegistrarse:

                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_container_login, new FragRegister(), FragRegister.class.getSimpleName())
                        .addToBackStack("b")
                        .commit();

                break;
        }
    }
}
