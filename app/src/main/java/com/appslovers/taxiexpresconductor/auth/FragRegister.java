package com.appslovers.taxiexpresconductor.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.util.MyRequestValidaror;
import com.appslovers.taxiexpresconductor.data.Urls;
import com.appslovers.taxiexpresconductor.dialogs.DialogRegistroEnProceso;
import com.appslovers.taxiexpresconductor.dialogs.DialogTerminos;
import com.appslovers.taxiexpresconductor.util.MyFragment;

/**
 * Created by javierquiroz on 23/05/16.
 */
public class FragRegister extends Fragment implements View.OnClickListener {


    public static FragRegister onNewInstance(Bundle b) {
        FragRegister f = new FragRegister();
        f.setArguments(b);
        return f;
    }

    CheckBox cbAcepto;
    EditText etNombres, etApellidos, etCorreo, etNumeroCel, etPass, etRePass;
    View ViewRoot;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot = inflater.inflate(R.layout.fragregister, null);
        ViewRoot.findViewById(R.id.tvTerminos).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnRegistrar).setOnClickListener(this);
        cbAcepto = (CheckBox) ViewRoot.findViewById(R.id.cbAcepto);

        etNombres = (EditText) ViewRoot.findViewById(R.id.etNombres);
        etApellidos = (EditText) ViewRoot.findViewById(R.id.etApellidos);
        etCorreo = (EditText) ViewRoot.findViewById(R.id.etEmail);
        etNumeroCel = (EditText) ViewRoot.findViewById(R.id.etNumeroCel);
        etPass = (EditText) ViewRoot.findViewById(R.id.etPass);
        etRePass = (EditText) ViewRoot.findViewById(R.id.etRePass);


        return ViewRoot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            JsonPack.FbResponse fb = (JsonPack.FbResponse) getArguments().getSerializable("facebook");
            if (fb != null) {
                etNombres.setText(fb.name);
                etApellidos.setText(fb.last_name);
                etCorreo.setText(fb.email);

            }
        }


    }


    MyRequestValidaror.Validator mrvnull = new MyRequestValidaror.Validator() {
        @Override
        public boolean validate(String value) {
            return value != null && value.length() > 0;
        }
    };
    MyRequestValidaror.Validator mrvemail = new MyRequestValidaror.Validator() {
        @Override
        public boolean validate(String value) {
            return value != null && value.length() > 0 && Patterns.EMAIL_ADDRESS.matcher(value).matches();
        }
    };
    MyRequestValidaror.Validator mrvphone = new MyRequestValidaror.Validator() {
        @Override
        public boolean validate(String value) {
            return value != null && value.length() > 0 && Patterns.PHONE.matcher(value).matches();
        }
    };

    MyRequestValidaror.Validator mrvpass = new MyRequestValidaror.Validator() {
        @Override
        public boolean validate(String value) {
            if (value != null && value.length() > 0) {
                if (value.length() >= 6)
                    return true;
                else
                    return false;

            }

            return false;
        }
    };


    Toast tMensaje;

    @Override
    public void onClick(View v) {

        if (tMensaje == null)
            tMensaje = Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT);

        switch (v.getId()) {
            case R.id.btnRegistrar:
                if (cbAcepto.isChecked()) {
                    new MyRequest<JsonPack.ResponseRegister>(Urls.ws_registro, MyRequest.HttpRequestType.POST) {

                        @Override
                        public void onParseSuccesForeground(ResponseWork rw, JsonPack.ResponseRegister object) {
                            if (object != null) {
                                if (object.success) {
                                    Log.d("registro", "registro con exito");
                                    new DialogRegistroEnProceso().show(getFragmentManager(), "regproc");
                                } else {
                                    Toast.makeText(getActivity(), object.mensaje, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), ">Error", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onValidateEx(String v, int index) {

                            Log.d("index", ">" + index);
                            switch (index) {
                                case 10:
                                    tMensaje.setText("Coloque el nombre por favor.");
                                    tMensaje.show();
                                    etNombres.requestFocus();

                                    break;

                                case 20:

                                    tMensaje.setText("Coloque los apellidos.");
                                    tMensaje.show();
                                    etApellidos.requestFocus();
                                    break;

                                case 30:
                                    tMensaje.setText("Coloque su correo.");
                                    tMensaje.show();
                                    etCorreo.requestFocus();
                                    break;

                                case 40:

                                    tMensaje.setText("Coloque su numero celular.");
                                    tMensaje.show();
                                    etNumeroCel.requestFocus();
                                    break;

                                case 50:

                                    tMensaje.setText("Las contraseñas no coinciden o son menores a 6 caracteres");
                                    tMensaje.show();
                                    etPass.requestFocus();
                                    break;
                            }

                        }
                    }
                            .putParams("nombre", etNombres.getText().toString(), mrvnull, 10)
                            .putParams("apellido", etApellidos.getText().toString(), mrvnull, 20)
                            .putParams("correo", etCorreo.getText().toString(), mrvemail, 30)
                            .putParams("celular", etNumeroCel.getText().toString(), mrvphone, 40)
                            .putParams("password", etPass.getText().toString().equals(etRePass.getText().toString()) ? etPass.getText().toString() : "", mrvpass, 50)
                            .putParams("dni", "0")
                            .putParams("ciudad", "")
                            .putParams("distrito", "")
                            .putParams("licencia", "")
                            .putParams("catlicencia", "")
                            .putParams("fechabrevete", "")
                            .send();

                } else {
                    Toast.makeText(getActivity(), "Debe Aceptar los terminos y condiciones", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.tvTerminos:

                //DialogAlerta md =new DialogAlerta();
                DialogTerminos md = new DialogTerminos();
                //DialogRegistroEnProceso md =new DialogRegistroEnProceso();
                // md.setTargetFragment(FragRegister.this,1);
                md.show(getFragmentManager(), "terminos");

                break;

        }
    }
}
