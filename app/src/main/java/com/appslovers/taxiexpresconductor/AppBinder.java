package com.appslovers.taxiexpresconductor;

import android.os.Binder;
import android.util.Log;

import com.appslovers.taxiexpresconductor.body.RootBody;

/**
 * Created by javierquiroz on 30/06/16.
 */
public class AppBinder extends Binder {

    private AppBackground app;
    private RootBody root;

    public AppBackground getApp() {
        return app;
    }

    public void setApp(AppBackground app) {
        Log.d("AppBinder","set App"+(app!=null?" OK":" NULL"));
        this.app = app;
    }

    public RootBody getRoot() {
        return root;
    }

    public void setRoot(RootBody root) {
        this.root = root;
    }
}
