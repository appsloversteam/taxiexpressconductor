package com.appslovers.taxiexpresconductor;

import android.*;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.data.InnerActions;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.util.MyRequest;
import com.appslovers.taxiexpresconductor.data.SqlitePack;
import com.appslovers.taxiexpresconductor.data.Urls;
import com.appslovers.taxiexpresconductor.auth.RootLoginReg;
import com.appslovers.taxiexpresconductor.body.RootBody;
import com.appslovers.taxiexpresconductor.util.Tools;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by javierquiroz on 20/05/16.
 */
public class RootSplash extends FragmentActivity implements View.OnClickListener {

    TextView tvReintentar;
    ProgressBar pbBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rootsplash);
        pbBar = (ProgressBar) findViewById(R.id.progressBar);
        tvReintentar = (TextView) findViewById(R.id.tvReintentar);
        tvReintentar.setOnClickListener(this);

    }

    SPUser sp;

    CountDownTimer CDT = new CountDownTimer(1000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {

            if (logueado)
                startActivity(new Intent(getApplicationContext(), RootBody.class));
            else
                startActivity(new Intent(getApplicationContext(), RootLoginReg.class));

            finish();
        }

    };


    boolean logueado;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        //new MyDataBase(getApplicationContext()).getReadableDatabase();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if ((ContextCompat.checkSelfPermission(RootSplash.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(RootSplash.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(RootSplash.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.CALL_PHONE}, 100);
        } else {
            initComonProces();
        }


    }

    public void initComonProces() {
        sp = new SPUser(getApplicationContext());
        //createOrUpdateShortcut();
        if (sp.getResponseLogin() != null) {
            logueado = true;
            ReqData();
        } else {
            CDT.start();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 1) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        initComonProces();
                    } else {
                        Toast.makeText(getApplicationContext(), "Por favor brinde los permisos requeridos.", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(RootSplash.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.CALL_PHONE}, 100);
                    }
                } else {

                    Toast.makeText(getApplicationContext(), "Por favor brinde los permisos requeridos.", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(RootSplash.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.CALL_PHONE}, 100);
                }
                return;
            }
        }
    }

    public void ReqData() {

        JsonPack.ModCount mc = ((AppForeGround) getApplication()).getDb().getCountMod();
        //syncZonas.putParams("mod", mc.mMod).putParams("tot", mc.mTot).send();
        syncZonas.putParams("fecha",sp.getDate()).putParams("id",sp.getLastId()).send();
    }

    MyRequest syncZonas = new MyRequest(null, Urls.ws_lista_zonas, MyRequest.HttpRequestType.GET) {
        @Override
        public void onSuccesForeground(ResponseWork rw) {

            //ArrayList<SqlitePack.Zona> zonas = null;
            ArrayList<JsonPack.ZonaSQL> zonas = null;
            JsonPack.SynchronizeZonesResponse respuesta=new Gson().fromJson(rw.ResponseBody, JsonPack.SynchronizeZonesResponse.class);
            try {
                Log.i("response", ">" + rw.ResponseBody);
                //zonas = new Gson().fromJson(rw.ResponseBody, new TypeToken<ArrayList<SqlitePack.Zona>>() {}.getType());
                zonas = respuesta.list;
            } catch (JsonParseException e) {
                Log.e("jsonparse", ">" + e.getMessage());
                System.out.println("ERROR JsonParseException  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ");
            }

            if (zonas != null) {
                sp.setDate(respuesta.fecha);
                sp.setLastId(respuesta.end_id);
                ((AppForeGround) getApplication()).getDb().fillZonas(zonas);
                Log.i("after fillzone", "------------->>>>>>>>>>>>>>>>>");
                ArrayList<JsonPack.Zona> ZZ = ((AppForeGround) getApplication()).getDb().getZonas();
                Log.e("zonas", ZZ.size() + " Zonas registradas en SQLite");
            }
            //syncTipoServicio.putParams("d", "d").send();

            getEstadoConductor.putParams("codigo", sp.getResponseLogin().conductor_id).putParams("flag", "1").send();
        }

        @Override
        public void onFailedRequestForeground(ResponseWork rw) {
            getEstadoConductor.putParams("codigo", sp.getResponseLogin().conductor_id).putParams("flag", "1").send();
        }
    };

    MyRequest<JsonPack.RespEstadoCondcutor> getEstadoConductor = new MyRequest<JsonPack.RespEstadoCondcutor>(null, Urls.estado, MyRequest.HttpRequestType.POST) {

        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.RespEstadoCondcutor object) {

            Log.i("Calf cond", ">" + object.CalificacionConductor);
            Log.i(" cliente objeto", " " + object.Cliente);
            Log.i(" servicio objeto", " " + object.Servicio);

            ((AppForeGround) getApplication()).flag_tipotarifa = object.TipoTarifa;
            //((AppForeGround) getApplication()).flag_califico = object.CalificacionCliente;
            ((AppForeGround) getApplication()).Alerta = object.Alerta;
            CDT.start();

            Intent servici = new Intent(getApplicationContext(), AppBackground.class);
            servici.setAction(InnerActions.notact_updateestado.toString());
            servici.putExtra("rec", object);
            startService(servici);

            /*RECUPERANDO API KEYS*/
            Urls.FOURSQUARE_CLIENT_ID = object.foursquare_key;
            Urls.FOURSQUARE_CLIENT_SECRET = object.foursquare_secret;
            Urls.GOOGLE_KEY = object.google_android;
            Urls.HERE_APP_ID = object.here_key;
            Urls.HERE_APP_CODE = object.here_secret;
        }

        @Override
        public void onFailedRequest(ResponseWork rw, boolean isActive) {
            flagTouchReintentar = true;
            pbBar.setVisibility(View.GONE);
            tvReintentar.setVisibility(View.VISIBLE);
        }
    };

    @Override
    protected void onStop() {

        if (CDT != null)
            CDT.cancel();

        super.onStop();

    }

    public void createOrUpdateShortcut() {
        if (!sp.checkInstalationShotCut()) {

            Intent HomeScreenShortCut = new Intent(getApplicationContext(), RootSplash.class);
            HomeScreenShortCut.setAction(Intent.ACTION_MAIN);
            HomeScreenShortCut.putExtra("duplicate", false);

            /*
            Intent removeIntent = new Intent();
            removeIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, HomeScreenShortCut);
            removeIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
            removeIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
            getApplicationContext().sendBroadcast(removeIntent);*/

            Intent addIntent = new Intent();
            addIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            addIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, HomeScreenShortCut);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.launcher));
            addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            getApplicationContext().sendBroadcast(addIntent);
        }
    }

    boolean flagTouchReintentar = false;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvReintentar:
                if (flagTouchReintentar) {
                    flagTouchReintentar = false;
                    tvReintentar.setVisibility(View.GONE);
                    pbBar.setVisibility(View.VISIBLE);
                    ReqData();
                }
                break;
        }
    }
}
