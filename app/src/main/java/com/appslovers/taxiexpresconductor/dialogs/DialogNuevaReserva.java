package com.appslovers.taxiexpresconductor.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.body.FragDisponible;
import com.appslovers.taxiexpresconductor.util.MyAppBackground;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.AppBackground;
import com.appslovers.taxiexpresconductor.data.JsonPack;

import java.text.DecimalFormat;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogNuevaReserva extends MyDialog implements View.OnClickListener {


    View ViewRoot;

    public JsonPack.NuevaReserva NR;

    TextView tvOrigen;
    TextView tvOrigenDir, tvOrigenRef;
    TextView tvDestino;
    TextView tvDestinoDir, tvDestinoRef;
    TextView tvTarifa;
    TextView tvHora;
    DecimalFormat formater;


    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot = inflater.inflate(R.layout.dialog_nuevareserva, null);
        ViewRoot.findViewById(R.id.btnAceptar).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnRechazar).setOnClickListener(this);

        tvOrigen = (TextView) ViewRoot.findViewById(R.id.tvOrigen);
        tvOrigenDir = (TextView) ViewRoot.findViewById(R.id.tvOrigenDir);
        tvOrigenRef = (TextView) ViewRoot.findViewById(R.id.tvOrigenRef);

        tvDestino = (TextView) ViewRoot.findViewById(R.id.tvDestino);
        tvDestinoDir = (TextView) ViewRoot.findViewById(R.id.tvDestinoDir);
        tvDestinoRef = (TextView) ViewRoot.findViewById(R.id.tvDestinoRef);

        tvTarifa = (TextView) ViewRoot.findViewById(R.id.tvTarifa);
        tvHora = (TextView) ViewRoot.findViewById(R.id.tvHora);
        formater = new DecimalFormat("#0.00");


        return ViewRoot;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (NR != null) {

            //tvOrigenDir.setText(NR.origen);
            //tvOrigenRef.setText(NR.ori);
            //tvDestinoDir.setText(NR.destino);
            //tvDestinoRef.setText(NR.referencia_destino);

            tvOrigen.setText("Origen : " + NR.getDirecOri());
            tvDestino.setText("Dirección : " + NR.getDirecDes());

            tvHora.setText("Fecha : " + NR.getFechaRes());
            tvTarifa.setText(getString(R.string.tarifa) + " : S/. " + formater.format(Double.parseDouble(NR.getTarifa())));
        }

    }

    @Override
    public boolean isMatchParent() {
        return false;
    }


    @Override
    public int getMargins() {
        return 15;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnAceptar:

                new MyAppBackground() {
                    @Override
                    public void todo(AppBackground app, int idop, Object obj) {
                        if (app.isNodeJsConnected()) {
                            NR.aceptado = 1;
                            app.AceptarNuevaReserva(NR);
                        }
                    }
                }.doit(getActivity(), 0);
                dismiss();
                break;

            case R.id.btnRechazar:
                new MyAppBackground() {
                    @Override
                    public void todo(AppBackground app, int idop, Object obj) {
                        if (app.isNodeJsConnected()) {
                            NR.aceptado = 2;
                            app.AceptarNuevaReserva(NR);
                        }
                    }
                }.doit(getActivity(), 0);
                dismiss();
                break;
        }

        //getTargetFragment().onActivityResult(getTargetRequestCode(), 200, new Intent());

    }
}
