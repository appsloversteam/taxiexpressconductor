package com.appslovers.taxiexpresconductor.dialogs;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.appslovers.taxiexpresconductor.AppBackground;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.util.MyAppBackground;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogPorquecalifico extends MyDialog implements View.OnClickListener{


    View ViewRoot;
    EditText etMensaje;
    public String MENSAJE="";

    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot =inflater.inflate(R.layout.dialog_porquepocasesteellas,null);
        ViewRoot.findViewById(R.id.btnEnviarComentario).setOnClickListener(this);
        etMensaje=(EditText)ViewRoot.findViewById(R.id.etMensaje);

        return ViewRoot;
    }



    @Override
    public boolean isMatchParent() {
        return false;
    }


    @Override
    public int getMargins() {
        return 15;
    }



    @Override
    public void onClick(View v) {

        MENSAJE=etMensaje.getText().toString();
        dismiss();
        //getTargetFragment().onActivityResult(getTargetRequestCode(), 200, new Intent());

    }
}
