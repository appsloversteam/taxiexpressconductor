package com.appslovers.taxiexpresconductor.dialogs;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.data.Urls;
import com.appslovers.taxiexpresconductor.util.MyRequest;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogEnviarVale extends MyDialog implements View.OnClickListener{

    public String idServicio;
    View ViewRoot;
    EditText etVale;
    ProgressBar pbLoading;
    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot =inflater.inflate(R.layout.dialog_enviarvale,null);
        ViewRoot.findViewById(R.id.btnEnviarVale).setOnClickListener(this);
        etVale=(EditText)ViewRoot.findViewById(R.id.etVale);
        pbLoading=(ProgressBar)ViewRoot.findViewById(R.id.pbLoading);

        return ViewRoot;
    }



    @Override
    public boolean isMatchParent() {
        return false;
    }


    @Override
    public int getMargins() {
        return 15;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId())
        {

            case R.id.btnEnviarVale:

                pbLoading.setVisibility(View.VISIBLE);
                MR_SAVEVALE
                        .putParams("vale",etVale.getText().toString())
                        .putParams("idservicio",idServicio)
                        .send();

                break;
        }

        //getTargetFragment().onActivityResult(getTargetRequestCode(), 200, new Intent());
    }

    MyRequest MR_SAVEVALE =new MyRequest(Urls.save_vale, MyRequest.HttpRequestType.POST) {

        @Override
        public void onSucces(ResponseWork rw, boolean isActive) {

            if(rw.ResponseBody!=null)
            {
                if(rw.ResponseBody.contains("true"))
                {
                    Intent ii=new Intent();
                    ii.putExtra("codigo",etVale.getText().toString());
                    getTargetFragment().onActivityResult(getTargetRequestCode(), 200, ii);
                    dismiss();
                    Toast.makeText(getActivity(),"Codigo de vale validado.",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getActivity(),"Este codigo ya ha sido usado.",Toast.LENGTH_SHORT).show();
                    pbLoading.setVisibility(View.GONE);
                }
            }
            else
            {
                Toast.makeText(getActivity(),"Este codigo ya ha sido usado.",Toast.LENGTH_SHORT).show();
                pbLoading.setVisibility(View.GONE);
            }

        }

        @Override
        public void onFailedRequest(ResponseWork rw, boolean isActive) {

            Toast.makeText(getActivity(),"Error en al conexion.",Toast.LENGTH_SHORT).show();
            pbLoading.setVisibility(View.GONE);

        }


    };
}
