package com.appslovers.taxiexpresconductor.dialogs;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.appslovers.taxiexpresconductor.util.IMethodFragmentStates;
import com.appslovers.taxiexpresconductor.data.IFrgamentStates;
import com.appslovers.taxiexpresconductor.R;

/**
 * Created by javierquiroz on 24/05/16.
 */
public class MyDialog extends DialogFragment implements IMethodFragmentStates {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MIDIALOG);
    }

    IFrgamentStates IFS;

    @Override
    public void setIfs(IFrgamentStates ifs) {
        IFS = ifs;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (IFS != null)
            IFS.onFragResume();
    }

    @Override
    public void onStop() {
        if (IFS != null)
            IFS.onFragStop();
        super.onStop();
    }

    ImageView ivClose;

    View ViewRoot;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(150, 0, 0, 0)));

        if (releaseOnBack()) {
            //setCancelable(false);

            getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(android.content.DialogInterface dialog, int keyCode, android.view.KeyEvent event) {

                    if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                        //This is the filter
                        if (event.getAction() != KeyEvent.ACTION_DOWN) {

                            Log.d("onKeyDialog", "true");
                            return true;
                        } else {
                            getActivity().onBackPressed();
                            Log.d("onKeyDialog ", "true UP");
                            //Hide your keyboard here!!!!!!
                            return true; // pretend we've processed it
                        }
                    } else {
                        Log.d("onKeyDialog", "false");
                        return false; // pass on to be processed as normal
                    }


                }
            });
        }


        ViewRoot = inflater.inflate(R.layout.mydialog, null);

        ivClose = (ImageView) ViewRoot.findViewById(R.id.ivClose);

        ViewRoot.setOnClickListener(OCL);
        ViewRoot.findViewById(R.id.ivClose).setOnClickListener(OCL);


        LinearLayout.LayoutParams ll_lp;
        RelativeLayout.LayoutParams rl_lp;

        LinearLayout.LayoutParams ll_lp_inside;

        ll_lp_inside = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);


        if (isMatchParent()) {
            rl_lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            ll_lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        } else {
            rl_lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            ll_lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        }

        rl_lp.setMargins(
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getMarginsClose(), getResources().getDisplayMetrics()),
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getMarginsClose(), getResources().getDisplayMetrics()),
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getMarginsClose(), getResources().getDisplayMetrics()),
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getMarginsClose(), getResources().getDisplayMetrics())
        );

        ll_lp.setMargins
                (
                        (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getMargins(), getResources().getDisplayMetrics()),
                        (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getMargins(), getResources().getDisplayMetrics()),
                        (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getMargins(), getResources().getDisplayMetrics()),
                        (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getMargins(), getResources().getDisplayMetrics())
                );


        ViewRoot.findViewById(R.id.rlContainer).setLayoutParams(ll_lp);
        ViewRoot.findViewById(R.id.llContainer).setLayoutParams(rl_lp);

        if (clearBackground())
            ViewRoot.findViewById(R.id.llContainer).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        View vi = onGetCreateView(inflater);
        if (vi != null) {

            Log.d("MyDialog", ">view inside OK");

            /*
            vi.setPadding(
                    (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getPaddingInside(), getResources().getDisplayMetrics()),
                    (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getPaddingInside(), getResources().getDisplayMetrics()),
                    (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getPaddingInside(), getResources().getDisplayMetrics()),
                    (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getPaddingInside(), getResources().getDisplayMetrics())
            );*/
            //vi.requestLayout();

            ((LinearLayout) ViewRoot.findViewById(R.id.llContainer)).addView(vi, ll_lp_inside);


        } else {
            Log.d("MyDialog", ">view inside null");
        }


        if (isCloseVisible()) {
            ivClose.setVisibility(View.VISIBLE);
        } else {
            ivClose.setVisibility(View.GONE);
        }

        return ViewRoot;
    }


    public boolean clearBackground() {
        return false;
    }

    public boolean isCloseVisible() {
        return true;
    }

    public boolean isMatchParent() {
        return true;
    }

    public int getMargins() {
        return 30;
    }

    public int getMarginsClose() {
        return 5;
    }


    public boolean dismissOnTouchOut() {
        return true;
    }


    public boolean releaseOnBack() {
        return false;
    }


    public int getPaddingInside() {
        return 4;
    }

    public View onGetCreateView(LayoutInflater inflater) {
        return null;
    }

    View.OnClickListener OCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.ivClose:
                    dismiss();
                    break;

                default:
                    if (dismissOnTouchOut())
                        dismiss();
            }
        }
    };


}
