package com.appslovers.taxiexpresconductor.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.appslovers.taxiexpresconductor.util.MyAppBackground;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.AppBackground;
import com.appslovers.taxiexpresconductor.data.JsonPack;
import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.util.Tools;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogCostosAdicionales extends MyDialog implements View.OnClickListener{


    View ViewRoot;
    EditText etPeajes;
    EditText etParqueos;
    EditText etParadas;
    TextView etEspera;

    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot =inflater.inflate(R.layout.dialog_costosadicionales,null);
        ViewRoot.findViewById(R.id.btnEnviarAlerta).setOnClickListener(this);
        etPeajes = (EditText)ViewRoot.findViewById(R.id.etPeajes);
        etParqueos =(EditText)ViewRoot.findViewById(R.id.etParqueos);
        etParadas=(EditText)ViewRoot.findViewById(R.id.etParadas);
        etEspera=(TextView)ViewRoot.findViewById(R.id.etEspera);

        return ViewRoot;
    }

    public float costo_espera=0;
    SPUser st;
    MyAppBackground MAB;
    Intent resultado=new Intent();
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        st=new SPUser(getActivity());
        MAB=new MyAppBackground() {

            @Override
            public void todo(AppBackground app, int idop, Object obj) {

                switch (idop)
                {
                    case 0:

                        //tvTarifa.setText("S/. "+String.format( "%.2f", app.getServicioActual().Tarifa ));


                        JsonPack.CostosAdicionales ca=st.getAdicionales();
                        if(ca!=null && ca.idServicio!=null)
                        {
                            if(!ca.idServicio.equals(app.getServicioActual().IdServicio))
                            {
                                st.setAdiconales(null);
                            }
                            else
                            {

                                etPeajes.setText(Tools.formatFloat(ca.Peajes));
                                etParqueos.setText(Tools.formatDouble(ca.Parqueos));
                                etParadas.setText(Tools.formatDouble(ca.Paradas ));
                                etEspera.setText(Tools.formatDouble(ca.Espera));
                            }
                        }


                        break;

                    case 1:

                        JsonPack.CostosAdicionales ca2=new JsonPack.CostosAdicionales();
                        ca2.idServicio = app.getServicioActual().IdServicio;
                        ca2.Peajes=(etPeajes.getText().toString().length()>0?Float.parseFloat(etPeajes.getText().toString()):0f);
                        ca2.Parqueos=(etParqueos.getText().toString().length()>0?Float.parseFloat(etParqueos.getText().toString()):0f);
                        ca2.Paradas=(etParadas.getText().toString().length()>0?Float.parseFloat(etParadas.getText().toString()):0f);
                        ca2.Espera=(etEspera.getText().toString().length()>0?Float.parseFloat(etEspera.getText().toString()):0f);
                        st.setAdiconales(ca2);
                        resultado.putExtra("adicionales",ca2);

                        break;

                }
            }


        };


    }


    @Override
    public void onResume() {
        super.onResume();

        MAB.doit(getActivity(), 0);
        if(costo_espera>0)
            etEspera.setText(Tools.formatFloat(costo_espera));

    }

    @Override
    public boolean isMatchParent() {
        return false;
    }


    @Override
    public int getMargins() {
        return 15;
    }

    @Override
    public void onClick(View v) {

        MAB.doit(getActivity(), 1);

        dismiss();
        getTargetFragment().onActivityResult(getTargetRequestCode(), 200, resultado);

    }
}
