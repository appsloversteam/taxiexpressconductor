package com.appslovers.taxiexpresconductor.dialogs;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.appslovers.taxiexpresconductor.data.SPUser;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.auth.RootLoginReg;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogRegistroEnProceso extends MyDialog {

    View ViewRoot;

    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot = inflater.inflate(R.layout.dialog_registroenproceso, null);
        ViewRoot.findViewById(R.id.tvCerrar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
                getActivity().startActivity(new Intent(getActivity(), RootLoginReg.class));
            }
        });
        ViewRoot.findViewById(R.id.tvEnlace).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.enlace_registro)));
                    startActivity(myIntent);
                } catch (ActivityNotFoundException e) {
                    //Toast.makeText(getContext(), "Instalar un navegador web",Toast.LENGTH_SHORT).show();

                }
            }
        });


        return ViewRoot;
    }


    @Override
    public boolean releaseOnBack() {
        return true;
    }

    @Override
    public boolean dismissOnTouchOut() {

        SPUser.logout(getActivity());
        getActivity().finish();
        getActivity().startActivity(new Intent(getActivity(), RootLoginReg.class));
        return true;
    }

    @Override
    public boolean isCloseVisible() {
        return false;
    }

    @Override
    public boolean isMatchParent() {
        return false;
    }


    @Override
    public int getMargins() {
        return 15;
    }
}
