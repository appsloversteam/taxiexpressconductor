package com.appslovers.taxiexpresconductor.dialogs;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;

import com.appslovers.taxiexpresconductor.R;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogConfirmarCanje extends MyDialog implements View.OnClickListener{


    View ViewRoot;

    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot =inflater.inflate(R.layout.dialog_confirmarcanje,null);
        ViewRoot.findViewById(R.id.btnConfirmar).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnCancelar).setOnClickListener(this);

        return ViewRoot;
    }

    @Override
    public boolean isMatchParent() {
        return false;
    }

    @Override
    public int getMargins() {
        return 15;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btnConfirmar:

                /*
                    new MyAppBackground() {
                        @Override
                        public void todo(AppBackground app, int idop, Object obj) {

                            if(app.isNodeJsConnected())
                            {
                                app.EnviarAlerta();
                            }
                            else
                            {
                                //Toast.makeText(getActivity(), "Esta funcionalidad esta disponible al estar Conectado.", Toast.LENGTH_SHORT).show();
                            }

                        }

                    }.doit(getActivity(),0);*/

                dismiss();

                getTargetFragment().onActivityResult(getTargetRequestCode(), 200, new Intent());
                break;
            case R.id.btnCancelar:

                dismiss();
                break;
        }



    }
}
