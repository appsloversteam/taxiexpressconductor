package com.appslovers.taxiexpresconductor.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.appslovers.taxiexpresconductor.R;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogTerminos extends MyDialog {


    View ViewRoot;
    public WebView WV;
    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot =inflater.inflate(R.layout.dialog_terminos,null);
        WV=(WebView)ViewRoot.findViewById(R.id.wbContenido);
        WV.getSettings().setSupportZoom(true);//habilita el zoom
        WV.getSettings().setBuiltInZoomControls(true);//necesario para habilitar zoom
        WV.getSettings().setLoadWithOverviewMode(true);//centra el contenido
        WV.getSettings().setDisplayZoomControls(false);//hide buttons zoom
        WV.getSettings().setUseWideViewPort(true);//double tap zoom

        WV.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
        WV.loadUrl(getString(R.string.url_terminos));
        return ViewRoot;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public boolean isMatchParent() {
        return true;
    }


    @Override
    public int getMargins() {
        return 15;
    }
}
