package com.appslovers.taxiexpresconductor.dialogs;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.appslovers.taxiexpresconductor.R;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogSms extends MyDialog implements View.OnClickListener{


    View ViewRoot;
    RadioGroup rbGrupo;
    public String Mensaje="";
    public RadioButton rb1,rb2,rb3,rb4;
    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot =inflater.inflate(R.layout.dialog_enviarsms,null);
        ViewRoot.findViewById(R.id.btnEnviarAlerta).setOnClickListener(this);
        rbGrupo=(RadioGroup)ViewRoot.findViewById(R.id.rbGrupo);

        rb1=(RadioButton)ViewRoot.findViewById(R.id.rb1);
        rb2=(RadioButton)ViewRoot.findViewById(R.id.rb2);
        rb3=(RadioButton)ViewRoot.findViewById(R.id.rb3);
        rb4=(RadioButton)ViewRoot.findViewById(R.id.rb4);

        Mensaje=rb1.getText().toString();


        rbGrupo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId)
                {

                    case R.id.rb1:
                        Mensaje=rb1.getText().toString();
                        break;

                    case  R.id.rb2:
                        Mensaje=rb2.getText().toString();
                        break;

                    case R.id.rb3:
                        Mensaje=rb3.getText().toString();
                        break;

                    case R.id.rb4:
                        Mensaje=rb4.getText().toString();
                        break;


                }
            }
        });
        return ViewRoot;
    }

    @Override
    public boolean isMatchParent() {
        return false;
    }


    @Override
    public int getMargins() {
        return 15;
    }

    @Override
    public void onClick(View v) {

        dismiss();
        getTargetFragment().onActivityResult(getTargetRequestCode(), 200, new Intent());
    }
}
