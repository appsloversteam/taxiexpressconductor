package com.appslovers.taxiexpresconductor.data;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by javierquiroz on 1/06/16.
 */
public interface IMyViewHolder {

    RecyclerView.ViewHolder getInstancia(View v);


}
