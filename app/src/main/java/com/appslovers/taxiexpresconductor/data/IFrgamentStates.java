package com.appslovers.taxiexpresconductor.data;

/**
 * Created by javierquiroz on 2/06/16.
 */
public interface IFrgamentStates {

    void onFragResume();

    void onFragStop();
}
