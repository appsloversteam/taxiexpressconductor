package com.appslovers.taxiexpresconductor.data;

/**
 * Created by javierquiroz on 23/06/16.
 */
public enum InnerActions {

    notact_updateestado
    ,notact_salirservicio
    ,notact_encendernode
    ,notact_detenernode
    ,notact_cancelar_detenernode
    ,notact_cancelar_encendernode
    ,notact_detenerservicio
    ,notact_aceptarSolicitud
    ,notact_rechazarSolicitud
    ,notact_showSolicitud
    ,notact_iniciarReserva
    ,notact_rechazarReserva

    ,noact_aceptarReserva
    ,noact_noaceptarReserva
    ,notact_cancelarservicio



}
