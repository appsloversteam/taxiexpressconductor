package com.appslovers.taxiexpresconductor.data;

/**
 * Created by javierquiroz on 6/06/16.
 */

import com.github.nkzawa.emitter.Emitter;

public class CustomEmitterListener implements Emitter.Listener
{
    public String myEvent ="";
    public  CustomEmitterListener(String me)
    {
        myEvent=me;
    }

    @Override
    public void call(Object... args) {

    }
}