package com.appslovers.taxiexpresconductor.data;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.appslovers.taxiexpresconductor.AppBackground;
import com.appslovers.taxiexpresconductor.R;
import com.appslovers.taxiexpresconductor.RootSplash;
import com.appslovers.taxiexpresconductor.body.RootBody;
import com.appslovers.taxiexpresconductor.util.Tools;

import java.util.Date;

/**
 * Created by javierquiroz on 23/06/16.
 */
public class Notificaciones {

    public static int LedOnMs = 1000;
    public static int LedOffMs = 300;
    public static long vibrador[] = {0, 500, 1000};

    public static Notification getNotificacionEncender(Context servicio) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(servicio)
                .setAutoCancel(false)
                .setSmallIcon(R.drawable.notificacion)
                .setColor(servicio.getResources().getColor(R.color.notificacion))
                .setLargeIcon(BitmapFactory.decodeResource(servicio.getResources(), R.drawable.launcher))
                //.setContentTitle(servicio.getString(R.string.noti_tit_enservicio))
                // .setContentText(servicio.getString(R.string.noti_text_enservicio))
                .setContentTitle(servicio.getString(R.string.nottitulo));

        Intent resultIntent = new Intent(servicio, RootBody.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(servicio, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);


        Intent Opcion1I = new Intent(servicio, AppBackground.class);
        Opcion1I.setAction(InnerActions.notact_encendernode.toString());
        PendingIntent Opcion1PI = PendingIntent.getService(servicio, 100, Opcion1I, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.addAction(R.drawable.bg_transpartente, "encender", Opcion1PI);


        Intent Opcion2I = new Intent(servicio, AppBackground.class);
        Opcion2I.setAction(InnerActions.notact_salirservicio.toString());
        PendingIntent Opcion2PI = PendingIntent.getService(servicio, 200, Opcion2I, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.addAction(R.drawable.bg_transpartente, "Salir", Opcion2PI);


        return mBuilder.build();
    }

    public static Notification getNotificacionEncendiendoDeteniendo(Context servicio, boolean b) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(servicio)
                .setAutoCancel(false)
                .setSmallIcon(R.drawable.notificacion)
                .setColor(servicio.getResources().getColor(R.color.notificacion))
                .setLargeIcon(BitmapFactory.decodeResource(servicio.getResources(), R.drawable.launcher))
                .setContentTitle(servicio.getString(R.string.nottitulo))
                .setContentText(b ? "mi app encendiendo" : "mi app deteniendo");

        Intent resultIntent = new Intent(servicio, RootBody.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(servicio, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        //String idconductor =new SPUser(servicio).getIdConductor();

        if (b) {
            Intent Opcion1I = new Intent(servicio, AppBackground.class);//Opcion1I.putExtra("tipo","2");
            Opcion1I.setAction(InnerActions.notact_cancelar_encendernode.toString());
            PendingIntent Opcion1PI = PendingIntent.getService(servicio, 201, Opcion1I, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.addAction(R.drawable.bg_transpartente, "Cancelar", Opcion1PI);
        } else {
            Intent Opcion1I = new Intent(servicio, AppBackground.class);//Opcion1I.putExtra("tipo","2");
            Opcion1I.setAction(InnerActions.notact_cancelar_detenernode.toString());
            PendingIntent Opcion1PI = PendingIntent.getService(servicio, 202, Opcion1I, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.addAction(R.drawable.bg_transpartente, "Cancelar", Opcion1PI);
        }

        Intent Opcion2I = new Intent(servicio, AppBackground.class);
        Opcion2I.setAction(InnerActions.notact_salirservicio.toString());
        PendingIntent Opcion2PI = PendingIntent.getService(servicio, 205, Opcion2I, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.addAction(R.drawable.bg_transpartente, "Salir", Opcion2PI);


        return mBuilder.build();
    }

    public static Notification getNotificacionDetener(Context servicio) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(servicio)
                .setAutoCancel(false)
                .setSmallIcon(R.drawable.notificacion)
                .setColor(servicio.getResources().getColor(R.color.notificacion))
                .setLargeIcon(BitmapFactory.decodeResource(servicio.getResources(), R.drawable.launcher))
                .setContentTitle(servicio.getString(R.string.nottitulo));

        Intent resultIntent = new Intent(servicio, RootBody.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(servicio, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);


        //String idconductor =new SPUser(servicio).getIdConductor();

        Intent Opcion1I = new Intent(servicio, AppBackground.class);
        //Opcion1I.putExtra("tipo","2");
        Opcion1I.setAction(InnerActions.notact_detenernode.toString());
        PendingIntent Opcion1PI = PendingIntent.getService(servicio, 200, Opcion1I, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.addAction(R.drawable.bg_transpartente, "detener node", Opcion1PI);
        //mBuilder.addAction(R.drawable.launcher, servicio.getString(R.string.noti_button_finalizarr), Opcion1PI);


        return mBuilder.build();
    }

    public static  Notification  getNotificationCancelarServicio(Context servicio){

        NotificationCompat.Builder mBuilder =  new NotificationCompat.Builder(servicio)
                .setPriority(Notification.PRIORITY_MAX)
                .setOngoing(true)
                .setSmallIcon(R.drawable.notificacion)
                .setColor(servicio.getResources().getColor(R.color.notificacion))
                .setContentTitle(servicio.getString(R.string.nottitulo))
                //.setLargeIcon(BitmapFactory.decodeResource(servicio.getResources(), R.drawable.ic_launcher))
                .setContentText("El cliente canceló el Servicio")
                //.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                //.setSound(Uri.parse("file:///android_asset/audio1.mp3"))
                //.setSound(Uri.parse("android.resource://" + servicio.getPackageName() + "/" + R.raw.audio1))
                .setVibrate(vibrador)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setStyle( new NotificationCompat.InboxStyle()
                        .addLine("Alerta: Se Cancelo El Servicio")
                        .setSummaryText(" " + new Date(System.currentTimeMillis()).toLocaleString()) );

        Intent resultIntent = new Intent(servicio, RootSplash.class);
        resultIntent.setAction(InnerActions.notact_cancelarservicio.toString());
        PendingIntent resultPendingIntent = PendingIntent.getActivity(servicio, 123, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        Notification N=mBuilder.build();


        N.flags |= Notification.FLAG_SHOW_LIGHTS;

        N.ledARGB= Color.YELLOW;//0xFFde281d;
        N.ledOnMS=LedOnMs;
        N.ledOffMS=LedOffMs;
        //N.vibrate=vibrador;

        return N;


    }


    public static Notification getNotificacionSolicitud(Context servicio, int cantidad) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(servicio)
                .setPriority(Notification.PRIORITY_MAX)

                //.setAutoCancel(false)//antes false
                .setOngoing(true)
                .setSmallIcon(R.drawable.notificacion)
                .setColor(servicio.getResources().getColor(R.color.notificacion))
                .setContentTitle(servicio.getString(R.string.nottitulo))
                //.setLargeIcon(BitmapFactory.decodeResource(servicio.getResources(), R.drawable.ic_launcher))
                .setContentText("Hay " + cantidad + (cantidad == 1 ? " nueva solicitud" : " nuevas solicitudes ") + " de servicio")
                //.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                //.setSound(Uri.parse("file:///android_asset/audio1.mp3"))
                //.setSound(Uri.parse("android.resource://" + servicio.getPackageName() + "/" + R.raw.audio1))
                .setVibrate(vibrador)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setStyle(
                        new NotificationCompat.InboxStyle()
                                .addLine("Ultima Solicitud recibida :")
                                .setSummaryText(" " + new Date(System.currentTimeMillis()).toLocaleString())
                );


        Intent resultIntent = new Intent(servicio, RootBody.class);
        resultIntent.setAction(InnerActions.notact_showSolicitud.toString());
        PendingIntent resultPendingIntent = PendingIntent.getActivity(servicio, 123, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        Notification N = mBuilder.build();

        //N.flags=Notification.FLAG_ON;
        //N.sound= Uri.parse("android.resource://" + servicio.getPackageName() + "/" + R.raw.claxon);

        N.flags |= Notification.FLAG_SHOW_LIGHTS;
        //N.sound=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //N.sound=Uri.parse("file:///android_asset/audio1.mp3");
        N.ledARGB = Color.YELLOW;//0xFFde281d;
        N.ledOnMS = LedOnMs;
        N.ledOffMS = LedOffMs;
        //N.vibrate=vibrador;


        return N;

    }

    public static Notification getNotificacionSolicitud(Context servicio, JsonPack.NuevaSolicitud ns, int nid) {

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(servicio)
                .setPriority(Notification.PRIORITY_MAX)
                .setAutoCancel(true)//antes false
                .setSmallIcon(R.drawable.notificacion)
                .setColor(servicio.getResources().getColor(R.color.notificacion))
                //.setLargeIcon(BitmapFactory.decodeResource(servicio.getResources(), R.drawable.ic_launcher))
                .setContentTitle(servicio.getString(R.string.nottitulo))
                .setContentText("Nuevo Servicio")
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setVibrate(vibrador)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setStyle(
                        new NotificationCompat.InboxStyle()
                                .addLine("RECOGER EN :")
                                .addLine(ns.Origen)
                                .addLine(ns.dirOrigen)
                                .addLine("LLEVAR A :")
                                .addLine(ns.Destino)
                                .addLine(ns.dirDestino)
                                .addLine(servicio.getString(R.string.uctarifa) + " : S/. " + ns.tarifa)
                                .setSummaryText("10 segundos para contestar")
                );


        Intent resultIntent = new Intent(servicio, RootBody.class);
        resultIntent.putExtra("solicitud", ns);
        resultIntent.setAction(InnerActions.notact_showSolicitud.toString());
        PendingIntent resultPendingIntent = PendingIntent.getActivity(servicio, 123, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);


        Intent Opcion1I = new Intent(servicio, AppBackground.class);

        Opcion1I.putExtra("solicitud", ns);
        Opcion1I.putExtra("notiid", nid);
        Opcion1I.setAction(InnerActions.notact_aceptarSolicitud.toString());
        PendingIntent Opcion1PI = PendingIntent.getService(servicio, 1000000 + nid, Opcion1I, PendingIntent.FLAG_UPDATE_CURRENT);


        Intent Opcion2I = new Intent(servicio, AppBackground.class);

        Opcion2I.setAction(InnerActions.notact_rechazarSolicitud.toString());
        Opcion2I.putExtra("solicitud", ns);
        Opcion2I.putExtra("notiid", nid);
        PendingIntent Opcion2PI = PendingIntent.getService(servicio, nid, Opcion2I, PendingIntent.FLAG_UPDATE_CURRENT);


        mBuilder.addAction(R.drawable.bg_transpartente, "Aceptar", Opcion1PI);
        mBuilder.addAction(R.drawable.bg_transpartente, "Rechazar", Opcion2PI);


        Notification N = mBuilder.build();

        //N.flags=Notification.FLAG_ON;
        //N.sound= Uri.parse("android.resource://" + servicio.getPackageName() + "/" + R.raw.claxon);

        N.flags |= Notification.FLAG_SHOW_LIGHTS;
        //N.sound=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        N.ledARGB = Color.YELLOW;//0xFFde281d;
        N.ledOnMS = LedOnMs;
        N.ledOffMS = LedOffMs;
        //N.vibrate=vibrador;


        return N;
    }

    public static Notification getNotificacionReserva(Context servicio, JsonPack.NuevaReserva ns, int nid) {

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(servicio)
                .setPriority(Notification.PRIORITY_MAX)
                .setAutoCancel(true)//antes false
                .setSmallIcon(R.drawable.notificacion)
                .setColor(servicio.getResources().getColor(R.color.notificacion))
                //.setLargeIcon(BitmapFactory.decodeResource(servicio.getResources(), R.drawable.ic_launcher))
                .setContentTitle(servicio.getString(R.string.nottitulo))
                .setContentText("Nueva Reserva")
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setVibrate(vibrador)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setStyle(
                        new NotificationCompat.InboxStyle()
                                .addLine("RECOGER EN :")
                                .addLine(ns.getDirecOri())
                                .addLine("LLEVAR A :")
                                .addLine(ns.getDirecDes())
                                .addLine("FECAH : " + ns.getFechaRes())
                                .addLine(servicio.getString(R.string.uctarifa) + " : " + ns.getTarifa())//trafia de reserva
                        //.setSummaryText("10 segundos para contestar")
                );



        /*Intent resultIntent = new Intent(servicio, RootBody.class);
        resultIntent.putExtra("solicitud",ns);
        resultIntent.setAction(InnerActions.notact_showSolicitud.toString());
        PendingIntent resultPendingIntent = PendingIntent.getActivity(servicio, 123, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);*/


        Intent Opcion1I = new Intent(servicio, AppBackground.class);

        Opcion1I.putExtra("reserva", ns);
        Opcion1I.putExtra("notiid", nid);
        Opcion1I.setAction(InnerActions.notact_iniciarReserva.toString());
        PendingIntent Opcion1PI = PendingIntent.getService(servicio, 1000000 + nid, Opcion1I, PendingIntent.FLAG_UPDATE_CURRENT);


        Intent Opcion2I = new Intent(servicio, AppBackground.class);

        Opcion2I.setAction(InnerActions.notact_rechazarReserva.toString());
        Opcion2I.putExtra("reserva", ns);
        Opcion2I.putExtra("notiid", nid);
        PendingIntent Opcion2PI = PendingIntent.getService(servicio, nid, Opcion2I, PendingIntent.FLAG_UPDATE_CURRENT);


        mBuilder.addAction(R.drawable.bg_transpartente, "Aceptar", Opcion1PI);
        mBuilder.addAction(R.drawable.bg_transpartente, "Rechazar", Opcion2PI);


        Notification N = mBuilder.build();

        //N.flags=Notification.FLAG_ON;
        //N.sound= Uri.parse("android.resource://" + servicio.getPackageName() + "/" + R.raw.claxon);

        N.flags |= Notification.FLAG_SHOW_LIGHTS;
        //N.sound=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        N.ledARGB = Color.YELLOW;//0xFFde281d;
        N.ledOnMS = LedOnMs;
        N.ledOffMS = LedOffMs;
        //N.vibrate=vibrador;


        return N;
    }

    public static Notification getNotificacionSolicitudReserva(JsonPack.NuevaReserva ns, Context servicio) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(servicio)
                .setPriority(Notification.PRIORITY_MAX)
                .setAutoCancel(false)//antes false
                //.setOngoing(true)
                .setSmallIcon(R.drawable.notificacion)
                .setColor(servicio.getResources().getColor(R.color.notificacion))
                .setContentTitle(servicio.getString(R.string.nottitulo))
                //.setLargeIcon(BitmapFactory.decodeResource(servicio.getResources(), R.drawable.ic_launcher))
                .setContentText("Hay  una nueva RESERVA")
                .setSound(Uri.parse("android.resource://" + servicio.getPackageName() + "/" + R.raw.audio1))
                .setVibrate(vibrador)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setStyle(
                        new NotificationCompat.InboxStyle()
                                .addLine("RECOGER EN :")
                                .addLine(ns.getDirecOri())
                                .addLine("LLEVAR A :")
                                .addLine(ns.getDirecDes())
                                .addLine(servicio.getString(R.string.uctarifa) + " : S/. " + ((int) Tools.getFloat(ns.getTarifa())))
                                .setSummaryText("recibido a las " + new Date(System.currentTimeMillis()).toLocaleString())
                );



        /*
        Intent resultIntent = new Intent(servicio, RootBody.class);
        resultIntent.setAction(InnerActions.noact_aceptarReserva.toString());
        PendingIntent resultPendingIntent = PendingIntent.getActivity(servicio, 789, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);*/


        Intent Opcion1I = new Intent(servicio, AppBackground.class);

        Opcion1I.putExtra("reserva", ns);
        //Opcion1I.putExtra("notiid",2000000);
        Opcion1I.setAction(InnerActions.noact_aceptarReserva.toString());
        PendingIntent Opcion1PI = PendingIntent.getService(servicio, 2000000, Opcion1I, PendingIntent.FLAG_UPDATE_CURRENT);


        Intent Opcion2I = new Intent(servicio, AppBackground.class);

        Opcion2I.setAction(InnerActions.noact_noaceptarReserva.toString());
        Opcion2I.putExtra("reserva", ns);
        //Opcion2I.putExtra("notiid",3000000);
        PendingIntent Opcion2PI = PendingIntent.getService(servicio, 3000000, Opcion2I, PendingIntent.FLAG_UPDATE_CURRENT);


        mBuilder.addAction(R.drawable.bg_transpartente, "Aceptar", Opcion1PI);
        mBuilder.addAction(R.drawable.bg_transpartente, "Rechazar", Opcion2PI);


        Notification N = mBuilder.build();

        //N.flags=Notification.FLAG_ON;
        //N.sound= Uri.parse("android.resource://" + servicio.getPackageName() + "/" + R.raw.claxon);

        N.flags |= Notification.FLAG_SHOW_LIGHTS;
        //N.sound=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //N.sound=Uri.parse("file:///android_asset/audio1.mp3");
        N.ledARGB = Color.YELLOW;//0xFFde281d;
        N.ledOnMS = LedOnMs;
        N.ledOffMS = LedOffMs;
        //N.vibrate=vibrador;


        return N;

    }
}
