package com.appslovers.taxiexpresconductor.data;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by javierquiroz on 30/05/16.
 */
public class JsonPack {

    public static class ServicioCad {
        public String IDServicio;
    }

    public static class ModCount {
        public String mMod;
        public String mTot;

    }

    public static class RespRecargaMovimientos {
        public boolean status;
        public ArrayList<MovimientoHist> result;
    }

    public static class MovimientoHist {
        public String cta_id;
        public float cta_monto;
        public String cta_fecha;
        public String cta_tipo;
    }

    public static class RespRecargaHistorial {
        public boolean status;
        public float saldo;
        public ArrayList<RecargaHist> historial;
    }

    public static class RecargaHist {
        public String recarga_id;
        public String recarga_monto;
        public String recarga_num_oper;
        public String fecha;
        public String banco_descripcion;
        public String recarga_tipo;
    }

    public static class FbResponse implements Serializable {

        public String id;
        public String name;
        public String last_name;
        public String email;
        public String gender;
        public String birthday;
    }


    public static class ServicioHist {

        public String origen;
        public String direccion_origen;
        public String destino;
        public String direccion_destino;
        public String fecha_inicio;
        public String fecha_fin;
        public String tarifa;
        public String hora;
        public String fecha;
        public int tipo_pago;
    }


    public static class ResponseHistorial {
        public boolean status;
        public ArrayList<ServicioHist> result;
    }


    public static class InicarReserva {
        public String reserva_id;
        public String conductor_id;
    }

    public static class ReservaAsignada {
        public NuevaReserva Reserva;
        public int Tiempo;
    }

    public static class ReservaPendiente {
        public String ServicioId;
    }

    public static class NodeNuevaReserva {
        public NuevaReserva Reserva;
    }

    public static class RespCancelarServicio {
        public String servicio_id;
        public String conductor_id;
        public String usuario;
        public String tipo;
    }

    public static class NuevaReserva implements Serializable {
        /*

        public String cliente_id;
        public String fecha;
        public String id_origen;
        public String origen;
        public double latitud_origen;
        public double longitud_origen;
        public String direccion_origen;
        public String referencia_origen;
        public String id_destino;
        public String destino;
        public double latitud_destino;
        public double longitud_destino;
        public String direccion_destino;
        public String referencia_destino;
        public String tiposervicio_id;
        public String tipopago_id;
        public String tarifa;*/


        public String cliente_id;
        public String cliente_tipo;
        public String cliente_fecha;//fecha de reserva
        public String cliente_celular;
        public String cliente_nombre;
        public String cliente_correo;
        public String cliente_costo;
        public String tiposervicio;
        public String tipopago;
        public String origen_direccion;
        public double origen_lat;
        public double origen_lng;
        public String origen_id;
        public String destino_direccion;
        public double destino_lat;
        public double destino_lng;
        public String destino_id;
        public String conductor_id;
        public int aceptado;
        public int tipo;

        public String reserva_id;

        /**/
        public String cliente;
        //public String reserva_id;
        //public String cliente_id;
        //public String conductor_id;
        public String reserva_fecha;
        //public String origen_id;
        public String lat_origen;
        public String lon_origen;
        public String dir_origen;
        //public String destino_id;
        public String lat_destino;
        public String lon_destino;
        public String dir_destino;
        public String tiposervicio_id;
        public String tipo_pago;
        public String tarifa;
        public String estado;
        public String fecha;
        public String hora;

        public String getDirecOri() {
            if (origen_direccion != null)
                return origen_direccion;
            else if (dir_origen != null)
                return dir_origen;
            else
                return "";
        }

        public String getDirecDes() {
            if (destino_direccion != null)
                return destino_direccion;
            else if (dir_destino != null)
                return dir_destino;
            else
                return "";
        }

        public String getFechaRes() {
            if (cliente_fecha != null)
                return cliente_fecha;
            else if (reserva_fecha != null)
                return reserva_fecha;
            else
                return "";
        }

        public String getTarifa() {
            if (cliente_costo != null)
                return cliente_costo;
            else if (tarifa != null)
                return tarifa;
            else
                return "0";
        }

    }

    /*
     "cliente_id": "140",
    "cliente_tipo": "0",
    "cliente_fecha": "08\/05\/2016 9:04 PM",
    "cliente_celular": "989819809",
    "cliente_nombre": "Boris(Usuario) Asenjo",
    "cliente_correo": "boris.asenjo@hotmail.com",
    "cliente_costo": "4",
    "tiposervicio": "1",
    "tipopago": "1",
    "origen_direccion": "Jirón Francisco Pizarro 722, Trujillo 13001, Perú",
    "origen_lat": "-8.109324",
    "origen_lng": "-79.02516249999996",
    "origen_id": "1",
    "destino_direccion": "Salaverry 354, Trujillo, Perú",
    "destino_lat": "-8.107975399999999",
    "destino_lng": "-79.03347610000003",
    "destino_id": "2",
    "conductor_id": "2"
    * */
    public static class RespReservas {

        public boolean status;
        public ArrayList<Reserva> result;
    }

    public static class Reserva {
        public String cliente;
        public String reserva_id;
        public String cliente_id;
        public String conductor_id;
        public String reserva_fecha;
        public String origen_id;
        public String lat_origen;
        public String lon_origen;
        public String dir_origen;
        public String destino_id;
        public String lat_destino;
        public String lon_destino;
        public String dir_destino;
        public String tiposervicio_id;
        public String tipo_pago;
        public String tarifa;
        public String estado;
        public String fecha;
        public String hora;
        public String Fecha;
        public String Hora;
        public String cliente_nombre;
    }

    public static class Direccion {

    }

    public static class ResponsePromociones {
        public boolean status;
        public ArrayList<Promocion> result;
    }

    public static class Promocion {
        public String promo_id;
        public String promo_imagen;
        public int promo_puntos;
        public String promo_nombre;
        public String promo_finicio;
        public String promo_fvigencia;
        public String promo_cantidad;
        public String promo_estado;
        public String promo_tipo;
        public int promo_stock;

    }

    public static class Avion extends Vehiculo {

    }

    public static class DistanciaRecorrida implements Serializable {
        public double distancia;
        public String idServicio;
    }

    public static class SolicitudPago{
        public CostosAdicionales costosAdicionales;
        public String clienteId;
        public String tarifa;
    }

    public static class CostosAdicionales implements Serializable {
        public float Paradas;
        public float Parqueos;
        public float Peajes;
        public float Espera;
        public String idServicio;


        public float getTotal() {
            return Paradas + Parqueos + Peajes + Espera;
        }
    }

    public static class ResponseLogin extends ResponseGeneric {

        public String nombre;
        public String apellido;
        public float calificacion;
        public String fecha_brevete;
        public String conductor_id;
        public String unidad_id;
        public String celular;
        public int tipo_id;
        public String tipo_nombre;
        public int conductor_status;

        /*API KEYS*/
        public String foursquare_key;
        public String foursquare_secret;
        public String google_android;
        public String here_key;
        public String here_secret;
    }


    public static class RespPuntos {
        public boolean status;
        public int puntos;

    }

    public static class RespFinalizarServ implements Serializable {
        public String FechaFin;
        public float Paradas;
        public float Parqueos;
        public float Peajes;
        public float Espera;
        public float Tarifa;
        public float tarifaServicio;
        public String TipoPago;
    }

    public static class ResponseRegister {
        public boolean success;
        public String mensaje;
    }

    public static class ResponseGeneric {
        public boolean status;
        public String mensaje;
    }

    public static class Vehiculo {
        public int tipo;
    }

    public static class Testnode {
        public String nombre;
    }

    public static class Zona implements Serializable {
        public String id;
        public String nombre;
        public Coordenada centro;
        public ArrayList<Coordenada> coordenadas;

        public LatLng ltlncentro;
        public ArrayList<LatLng> ltlns;

        public ArrayList<LatLng> getLtns() {
            if (ltlns == null)
                ltlns = new ArrayList<>();
            return ltlns;
        }
    }

    public static class Coordenada implements Serializable {
        public double latitud;
        public double longitud;

        public LatLng generar() {
            return new LatLng(latitud, longitud);
        }

        public boolean isInsidePolygon(ArrayList<LatLng> bounds, LatLng point) {
            return PolyUtil.containsLocation(point, bounds, true);
        }

        public Coordenada parseCoordinates(String coordString){
            coordString=coordString.replace('(',' ').replace(')',' ').trim();
            String[] latLng=coordString.split(",");
            this.latitud=Double.parseDouble(latLng[0]);
            this.longitud=Double.parseDouble(latLng[1]);
            return this;
        }
    }


    public static class ResponseTrarifa {
        public float tarifa;
        public boolean status;
    }

    public interface IDirLug {
        public ArrayList<DirLug> getDirLug();
    }

    public static class DirLug implements Serializable {
        public int from;
        public double lat;
        public double lon;
        public String name;
        public String direccion;
        public String iconURL;
    }

    //---- json Google distance matrix

    public static class GoogleDistanceMatrixResult {
        public ArrayList<String> destination_addresses;
        public ArrayList<String> origin_addresses;
        public ArrayList<GoogleDmRow> rows;
        public String status;

    }

    public static class GoogleDmRow {
        public ArrayList<GoogleDmData> elements;
    }

    public static class GoogleDmData {
        public GoogleDitance distance;
        public GoogleDuration duration;
    }

    public static class GoogleDitance {
        public String text;
        public long value;
    }

    public static class GoogleDuration {
        public String text;
        public long value;
    }


    //----------------------------- json direcciones here

    public static class Here implements Serializable, IDirLug {
        public HereResponse Response;
        ArrayList<DirLug> dirslugs;

        @Override
        public ArrayList<DirLug> getDirLug() {

            if (dirslugs == null)
                dirslugs = new ArrayList<>();

            if (dirslugs.size() == 0) {

                if (Response.View.size() > 0) {
                    for (HereResult hr : Response.View.get(0).Result) {
                        DirLug dl = new DirLug();
                        dl.from = 1;
                        //dl.name = hr.Location.Address.Label;
                        dl.name = hr.Location.Address.Street;
                        dl.lat = hr.Location.NavigationPosition.get(0).Latitude;
                        dl.lon = hr.Location.NavigationPosition.get(0).Longitude;
                        dirslugs.add(dl);

                    }
                }
            }
            return dirslugs;
        }
    }

    public static class HereResponse implements Serializable {
        public HereMetaInfo MetaInfo;
        public ArrayList<HereView> View;

    }

    public static class HereMetaInfo implements Serializable {
        public String Timestamp;
    }

    //item
    public static class HereView implements Serializable {
        public String _type;
        public int ViewId;
        public ArrayList<HereResult> Result;
    }

    public static class HereResult implements Serializable {
        public float Relevance;
        public float Distance;
        public String MatchLevel;
        public HereLocation Location;

    }

    public static class HereLocation implements Serializable {
        public String LocationId;
        public String LocationType;
        public ArrayList<HereNavigationPosition> NavigationPosition;
        public HereAddress Address;

    }

    public static class HereNavigationPosition {
        public double Latitude;
        public double Longitude;
    }

    public static class HereAddress {
        public String Label;
        public String Country;
        public String State;
        public String County;
        public String City;
        public String Street;
        public String HouseNumber;
        public String PostalCode;

    }


    //---------------- json google reverse geocode

    /*
    public static class GoogleRevGeocode implements Serializable
    {

        public ArrayList<GoogleRevGeocodeResult> results;
        public String status;
    }

    public static class GoogleRevGeocodeResult
    {
        public ArrayList<GoogleRevGeocodeAddressComponent> address_components;
        public String formatted_address;
        public GoogleRevGeocodeGeometry geometry;
        public ArrayList<String> types;
    }

    public static class GoogleRevGeocodeAddressComponent
    {
        public static String long_name;
        public static String short_name;
        public static  ArrayList<String> types;
    }

    public static class GoogleRevGeocodeGeometry
    {
        public GoogleRevGeocodeLocation location;
    }
    public static class GoogleRevGeocodeLocation
    {
        public double lat;
        public double lon;
    }*/

    //----------------------------- json direcciones Google


    public static class GoogleGeoCode implements Serializable, IDirLug {

        public String status;
        public ArrayList<GoogleGeoCodeResult> results;
        ArrayList<DirLug> dirslugs;

        @Override
        public ArrayList<DirLug> getDirLug() {
            if (dirslugs == null)
                dirslugs = new ArrayList<>();

            if (dirslugs.size() == 0) {
                for (GoogleGeoCodeResult g : results) {
                    DirLug dl = new DirLug();
                    dl.from = 2;

                    if (g.address_components != null) {

                        String snum = "";
                        String snom = "";
                        String urbdis = "";
                        for (JsonPack.GoogleGeoCodeAddressComponent ac : g.address_components) {
                            if (ac.types != null) {
                                if (ac.types.size() > 0) {
                                    for (String tipo : ac.types) {

                                        if (tipo.contains("street_number")) {
                                            snum = ac.short_name;
                                        }


                                        if (ac.types.get(0).contains("route")) {
                                            snom = ac.long_name;
                                        }

                                        if (ac.types.get(0).contains("locality")) {
                                            urbdis = ac.short_name;
                                        }

                                    }

                                }
                            }

                        }

                        dl.name = (snom + " " + snum + " " + urbdis).trim();
                    } else {
                        dl.name = g.formatted_address;
                    }


                    dl.lat = g.geometry.location.lat;
                    dl.lon = g.geometry.location.lng;
                    dirslugs.add(dl);
                }
            }
            return dirslugs;
        }
    }

    public static class GoogleGeoCodeResult implements Serializable {
        public ArrayList<GoogleGeoCodeAddressComponent> address_components;
        public String formatted_address;
        public GoogleGeoCodeGeometry geometry;

    }

    public static class GoogleGeoCodeComponents {
        public String long_name;
        public String short_name;
        public ArrayList<String> types;
    }

    public static class GoogleGeoCodeGeometry implements Serializable {
        public GoogleGeoCodeLocation location;
    }

    public static class GoogleGeoCodeLocation implements Serializable {
        public double lat;
        public double lng;
        public ArrayList<GoogleGeoCodeAddressComponent> address_components;
    }

    public static class GoogleGeoCodeAddressComponent {
        public String long_name;
        public String short_name;
        public ArrayList<String> types;

    }

    //------------------ json foursaquere


    public static class Foursquare implements Serializable, IDirLug {

        public FoursquareMeta meta;
        public FoursquareResponse response;
        ArrayList<DirLug> dirslugs;

        @Override
        public ArrayList<DirLug> getDirLug() {

            if (dirslugs == null)
                dirslugs = new ArrayList<>();

            if (dirslugs.size() == 0) {
                for (FoursquareVenue v : response.venues) {
                    if (v.name != null && v.location.address != null) {
                        DirLug dl = new DirLug();
                        dl.from = 3;
                        dl.name = v.name;
                        dl.direccion = v.location.formattedAddress.get(0);//v.location.formattedAddress.get(0);
                        dl.lat = v.location.lat;
                        dl.lon = v.location.lng;

                        if (!v.categories.isEmpty())
                            dl.iconURL = v.categories.get(0).icon.prefix + "32" + v.categories.get(0).icon.suffix;

                        dirslugs.add(dl);
                    }
                }
            }
            return dirslugs;
        }
    }

    public static class FoursquareResponse implements Serializable {
        public ArrayList<FoursquareVenue> venues;
    }

    public static class FoursquareMeta implements Serializable {
        public int code;
        public String requestId;
    }

    public static class FoursquareVenue implements Serializable {
        public String id;
        public String name;

        public FoursquareLocation location;
        public String url;
        public ArrayList<FoursquareCategory> categories;
        public boolean verified;
    }

    public static class FoursquareLocation implements Serializable {
        public String address;
        public String crossStreet;
        public double lat;
        public double lng;
        public String postalCode;
        public String cc;
        public String city;
        public String state;
        public String country;
        ArrayList<String> formattedAddress;

    }

    public static class FoursquareCategory {
        public String id;
        public String name;
        public FoursquareIcon icon;
    }

    public static class FoursquareIcon {
        public String prefix;
        public String suffix;
    }

    //------------------ Node JS

    public static class Alerta {
        public String idConductor;
        public double latitud;
        public double longitud;
        public String nombre;
        public String apellido;
    }


    public static class SolicitudRespuesta {
        public String servicio_id;
        public String conductor_id;
        public String socketCli;
    }

    public static class NuevaSolicitud implements Serializable {

        public String IdCliente;
        public double latOrig;
        public double latDest;
        public double lngOrig;
        public double lngDest;
        public String Origen;
        public String OrigenNombre;
        public String DestinoNombre;
        public String Destino;
        public String dirOrigen;
        public String dirDestino;
        public String refOrigen;
        public String referenciaorigen;
        public String refDestino;
        public String referenciadestino;
        public String tipoServicio;
        public String tipopago;
        public String ServicioId;
        public float tarifa;

        public String socketCliente;
        public int Envio;

        public float Calificacion;
        public int Viajes;
        public String vale;

    }

    public static class AlertaWeb {
        public String tipo;
        public String codigo;
        public double latitud;
        public double longitud;
        public int flag;
    }

    public static class RespEstadoCondcutor implements Serializable {
        public JsonPack.Servicio Servicio;
        public Cliente Cliente;
        public int CalificacionConductor;
        public int TipoTarifa;//1 es zona 2 es kilometraje
        public int Alerta;

        /*API KEYS*/
        public String foursquare_key;
        public String foursquare_secret;
        public String google_android;
        public String here_key;
        public String here_secret;

        public int getEstado() {
            if (Servicio != null)
                return Servicio.getEstado();
            else
                return 0;
        }

    }

    public static class CallbackFinalizar {
        public String Servicio;
        public float tarifa;
        public float tarifaServicio;
        public String TipoPago;
    }

    public static class Cliente implements Serializable {
        public String cliente_id;
        public String cliente_nombre;
        public String cliente_correo;
        public String cliente_celular;
        public String empresa_id;
        public String tipocliente_id;
        public String cliente_viajes;
    }


    public static class Servicio implements Serializable {

        public String IdServicio;
        public String IdCliente;
        public String IdConductor;
        public String OrigenId;
        public String OrigenNombre;
        public String OrigenDireccion;
        public String OrigenReferencia;
        public String OrigenLat;
        public String OrigenLng;
        public String DestinoId;
        public String DestinoNombre;
        public String DestinoDireccion;
        public String DestinoReferencia;
        public String DestinoLat;
        public String DestinoLng;
        public String TipoPago;
        public String Tarifa;
        public String Estado;
        public String FechaRecojo;
        public String HoraInicio;

        public String vale;

        public String cliente_nombre;

        public JsonPack.CostosAdicionales costosadicionales;


        public int getEstado() {
            if (Estado != null)
                return Integer.parseInt(Estado);
            else
                return 0;
        }
    }

    public static class AceptaTaxi {
        public String nombre;
        public String celular;
        public float calificacion;
        public float tarifa;
        public String origen;
        public String destino;
        public double latitud;
        public double longitud;
        public String socketCliente;


    }

    public static class MisSesiones {
        public ArrayList<String> usuarios;

        public String[] getUsurarios() {
            if (usuarios != null)
                return usuarios.toArray(new String[usuarios.size()]);
            else
                return new String[]{};
        }
    }

    public static class LlegoRecojo {
        public String IdConductor;
        public String IdServicio;
        public int flagexpreso;
        public double distancia;
        //public CostosAdicionales costosad;

        public float Paradas;
        public float Parqueos;
        public float Peajes;
        public float Espera;

        public String tarifa;

        public String DestinoId;
        public String DestinoDireccion;
        public double DestinoLat;
        public double DestinoLng;

        public String activo;//esto es para saber si paso (1) o no(2) por el aeropuerto
    }

    public static class UpdateConductorSocket {
        public String idConductor;
        public String socketConductor;
        public String idCliente;
        public String socketCliente;
    }

    public static class UpdateGeoConductor {
        public String idConductor;
        public double latitud;
        public double longitud;
    }

    public static class RespUpdateGeoConductor {
        public String idConductor;
        public double latitud;
        public double longitud;
        public String socket;
        public int stateConductor;
    }

    //Danny 11-08-2016
    //Incio
    public static class CancelarServicio {
        public String ServicioId;
    }

    public static class ZonaSQL{
        public String lugares_id,lugar_descripcion,lugar_lat,lugar_lon,lugar_coordpolygono,lugar_fecharegistro,lugar_fechaupdate,lugar_estado,lugar_mod,lugar_update;
        public ArrayList<String> coordenadas;

    }

    public static class CoordinatesArray{
        public ArrayList<Coordenada> coordenadas;
    }

    public static class SynchronizeZonesResponse{
        public boolean status;
        public ArrayList<ZonaSQL> list;
        public String end_id, fecha;
    }

    public class VisaNetResponse{
        public String idServicio;
        public PaymentInfo info;
    }

    public class PaymentInfo{
        public PaymentData data;
        public String email,firstName,lastName,paymentDescription,paymentStatus,transactionDate,transactionId;
    }

    public class PaymentData{
        public String NUMORDEN,
        IMPCUOTAAPROX,
        PAN ,
        COD_AUTORIZA ,
        NROCUOTA,
        ETICKET ,
        CSICODIGOPROGRAMA ,
        NUMREFERENCIA,
        ECI ,
        CODACCION ,
        DSC_ECI ,
        RESPUESTA,
        CSIIMPORTECOMERCIO,
        ID_UNICO,
        NOM_EMISOR,
        CSIMENSAJE,
        CSIPORCENTAJEDESCUENTO,
        CODTIENDA ,
        ORI_TARJETA ,
        RES_CVV2 ,
        reviewTransaction ,
        CSITIPOCOBRO ,
        DSC_COD_ACCION,
        IMP_AUTORIZADO,
        DECISIONCS,
        FECHAYHORA_TX;
    }


}
