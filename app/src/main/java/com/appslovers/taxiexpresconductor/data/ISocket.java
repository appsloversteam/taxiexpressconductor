package com.appslovers.taxiexpresconductor.data;

import android.location.Location;

/**
 * Created by javierquiroz on 3/06/16.
 */
public interface  ISocket
{


    void connect(String s);
    void reconnect(String s);
    void disconnect(String s);
    void connect_error(String s);
    void connect_tiemout(String s);

    void nuevaSolicitudConductor(JsonPack.NuevaSolicitud ns);
    void ConductorSeleccionado(JsonPack.RespEstadoCondcutor s);
    void ServicioCaducado(JsonPack.ServicioCad cad);

    void ServicioCancelado(JsonPack.CancelarServicio s);

    void solicitudListaTaxi(JsonPack.AceptaTaxi ace);
    void finalizarConTarjeta(String json);


    void contestar(JsonPack.Testnode t);
    void alertaTaxiWeb(JsonPack.Alerta a);
    void respUpdateSocketConductor(String s);
    void respUpdateSocketCliente(String s);
    void NotificacionReserva(JsonPack.NodeNuevaReserva s);
    void NotificacionReservaAceptada(JsonPack.NodeNuevaReserva s);
    void ReservaProxima(JsonPack.ReservaAsignada s);
    void FinalizarServicioTaxista(JsonPack.RespFinalizarServ s);
    void ReservaPendienteConductor(JsonPack.ReservaPendiente r);

    void refreshCoordinates(double lat, double lng);

}
