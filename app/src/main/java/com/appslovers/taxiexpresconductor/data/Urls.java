package com.appslovers.taxiexpresconductor.data;

/**
 * Created by javierquiroz on 15/07/16.
 */
public class Urls {

    private final static String servidor = "http://taxiexpress.tutaxidispatch.com";

    //private final static String servidor="http://192.168.1.22";

    //KEY APIS
    public  static String HERE_APP_ID = "bMzsKnGwUXPrkNWJEZY6";
    public  static String HERE_APP_CODE = "En8L-Gp7uPd73tJo5p8npw";
    public  static String FOURSQUARE_CLIENT_ID = "0XCQMELQCHQYLO5W15JBO4QLQZG3KDEYYHTZQ1CIJUWM1HFT";
    public  static String FOURSQUARE_CLIENT_SECRET = "XOANLQ2CMP5GR44BSVH1F5Y4X23QC2M5WU1QB4SRLTXWK1QP";
    public  static String GOOGLE_KEY = "XOANLQ2CMP5GR44BSVH1F5Y4X23QC2M5WU1QB4SRLTXWK1QP";


    private final static String puerto = "3000";
    //private final static String puerto="3004";

    private final static String servidornodejs = servidor + ":" + puerto + "/";
    public final static String servidornodejssocket = servidor + ":" + puerto;
    private final static String servidorphp = servidor + "/ws/conductor/";
    //private final static String servidorphp=servidor+":8081/ws/conductor/";


    public final static String servidorarchivos = servidor + "/BL/";

    public final static String estado = servidornodejs + "w_recuperar_estado";
    public final static String save_vale = servidornodejs + "w_save_vale_general";

    public final static String ws_recuperar_password = servidorphp + "ws_recuperar_password.php";
    public final static String ws_lista_beneficio = servidorphp + "ws_lista_beneficio.php";
    public final static String ws_lista_zonas = servidorphp + "ws_lista_zonas.php";
    public final static String ws_canjear_beneficios = servidorphp + "ws_canjear_beneficios.php";
    public final static String ws_registro = servidorphp + "ws_registro.php";
    public final static String ws_lista_reservas = servidorphp + "ws_lista_reservas.php";
    public final static String ws_historial_servicios = servidorphp + "ws_historial_servicios.php";
    public final static String ws_login = servidorphp + "ws_login.php";
    public final static String ws_consultar_tarifa = servidorphp + "ws_consultar_tarifa.php";
    public final static String ws_calificacion = servidorphp + "ws_calificacion.php";
    public final static String ws_obtener_puntos = servidorphp + "ws_obtener_puntos.php";
    public final static String ws_historial_recarga = servidorphp + "ws_historial_recarga.php";
    public final static String ws_recarga_conductor = servidorphp + "ws_recarga_conductor.php";
    public final static String ws_listar_entidad = servidorphp + "ws_listar_entidad.php";
    public final static String ws_historial_movimientos = servidorphp + "ws_historial_movimientos.php";

    //URL APIS
    public final static String here_geocode = "http://geocoder.cit.api.here.com/6.2/geocode.json";
    public final static String foursquare_geocode = "https://api.foursquare.com/v2/venues/search";
    public final static String google_geocode = "http://maps.google.com/maps/api/geocode/json";
    public final static String google_inversegeocode = "https://maps.googleapis.com/maps/api/geocode/json";
    public final static String google_distancematrix = "https://maps.googleapis.com/maps/api/distancematrix/json";


}
