package com.appslovers.taxiexpresconductor.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by javierquiroz on 10/06/16.
 */
public class SqlitePack implements Serializable{



    public static class Zona implements Serializable
    {
        public long _id;
        public String id;
        public String nombre;
        public Coordenada centro;
        public ArrayList<Coordenada> coordenadas;
        public double centrolatitud;
        public double centrolongitud;
        public long mod;
        public long estado;

    }



    public static class Coordenada implements Serializable
    {
        public long _id;
        public String _idzona;
        public double latitud;
        public double longitud;

    }







}
