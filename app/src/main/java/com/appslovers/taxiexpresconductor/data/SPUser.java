package com.appslovers.taxiexpresconductor.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

/**
 * Created by javierquiroz on 9/06/16.
 */
public class SPUser {


    private static final String RESPONSELOGIN = "RESPONSELOGIN";
    private static final String ADICIONALES = "ADICIONALES";
    private static final String DISTREC = "DISTREC";
    private static final String RESERVA_PENDIENTE = "RES_PEN";


    private static final String SESIONES = "SESIONES";
    private static final String ICONO_HOMESCREEN = "ICONO_HOME";


    private static final String DATE="DATE";
    private static final String ULT_ID="ID";
    private static final String CODIGO_VALE="CODIGO_VALE";

    public void setDate(String date){
        SP.edit().putString(DATE,date).commit();
    }

    public String getDate(){
        return SP.getString(DATE,null);
    }


    SharedPreferences SP;

    public SPUser(Context c) {
        SP = c.getSharedPreferences(SPUser.class.getSimpleName(), Context.MODE_MULTI_PROCESS);
    }

    public static void logout(Context c) {
        c.getSharedPreferences(SPUser.class.getSimpleName(), Context.MODE_MULTI_PROCESS).edit()
                .remove(RESPONSELOGIN)
                .remove(DISTREC)
                //.remove(RESERVA)
                .remove(ADICIONALES)
                .remove(RESERVA_PENDIENTE)
                //.clear()
                .commit();
    }
    public void setLastId(String lastId) {
        SP.edit().putString(ULT_ID,lastId).commit();
    }

    public String getLastId(){
        return SP.getString(ULT_ID,null);
    }

    public JsonPack.NuevaReserva getReservaPendiente() {
        JsonPack.NuevaReserva nr = null;

        String s = SP.getString(RESERVA_PENDIENTE, null);
        if (s != null) {
            nr = new Gson().fromJson(s, JsonPack.NuevaReserva.class);
        }

        return nr;
    }

    public void setReservaPendiente(JsonPack.NuevaReserva nr) {
        if (nr == null) {
            SP.edit().remove(RESERVA_PENDIENTE).commit();
        } else {
            SP.edit().putString(RESERVA_PENDIENTE, new Gson().toJson(nr)).commit();
        }
    }

    public void setResponseLogin(JsonPack.ResponseLogin rl) {
        SP.edit().putString(RESPONSELOGIN, new Gson().toJson(rl)).commit();
    }

    public JsonPack.ResponseLogin getResponseLogin() {
        JsonPack.ResponseLogin rl = null;

        String s = SP.getString(RESPONSELOGIN, null);
        //Log.v("getResponseLogin",">"+s);
        if (s != null) {
            rl = new Gson().fromJson(s, JsonPack.ResponseLogin.class);
        }

        return rl;
    }


    public void setDistanciaRecorrida(JsonPack.DistanciaRecorrida ca) {
        if (ca == null)
            SP.edit().remove(DISTREC).commit();
        else
            SP.edit().putString(DISTREC, new Gson().toJson(ca)).commit();
    }

    public JsonPack.DistanciaRecorrida getDistanciaRecorrida() {
        JsonPack.DistanciaRecorrida ca = new JsonPack.DistanciaRecorrida();

        String s = SP.getString(DISTREC, null);
        //Log.v("getResponseLogin",">"+s);
        if (s != null) {
            ca = new Gson().fromJson(s, JsonPack.DistanciaRecorrida.class);
        }


        return ca;
    }

    public SPUser setAdiconales(JsonPack.CostosAdicionales ca) {
        if (ca == null)
            SP.edit().remove(ADICIONALES).commit();
        else
            SP.edit().putString(ADICIONALES, new Gson().toJson(ca)).commit();
        return this;
    }


    public JsonPack.CostosAdicionales getAdicionales() {
        JsonPack.CostosAdicionales ca = new JsonPack.CostosAdicionales();

        String s = SP.getString(ADICIONALES, null);
        //Log.v("getResponseLogin",">"+s);
        if (s != null) {
            ca = new Gson().fromJson(s, JsonPack.CostosAdicionales.class);
        }


        return ca;
    }

    /*
    public void setReservas(JsonPack.NuevaReserva nr) {
        if (nr != null) {
            SP.edit().putString(RESERVA, new Gson().toJson(nr)).commit();
        }
    }


    public JsonPack.NuevaReserva getReserva()
    {
        String s=SP.getString(RESERVA,null);
        if(s!=null)
            return new Gson().fromJson(s, JsonPack.NuevaReserva.class);
        else
            return null;
    }*/

    public JsonPack.MisSesiones getUsuarios() {
        String s = SP.getString(SESIONES, null);
        if (s != null) {
            return new Gson().fromJson(s, JsonPack.MisSesiones.class);
        }
        return new JsonPack.MisSesiones();
    }

    public void setUsuarios(JsonPack.MisSesiones ms) {
        if (ms != null) {
            SP.edit().putString(SESIONES, new Gson().toJson(ms)).commit();
        }
    }


    public boolean checkInstalationShotCut() {
        boolean v = SP.getBoolean(ICONO_HOMESCREEN, false);
        if (!v) {
            SP.edit().putBoolean(ICONO_HOMESCREEN, true).commit();

        }

        return v;
    }

    public void setCodigoVale(String vale){
        SP.edit().putString(CODIGO_VALE,vale).commit();
    }

    public String getCodigoVale(){
        return SP.getString(CODIGO_VALE,null);
    }
}
